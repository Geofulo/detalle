﻿using System;
using Xamarin.Forms;
using Detalle;
using Detalle.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly:ExportRenderer(typeof(SegmentedControlCustom), typeof(SegmentedControlRender))]
namespace Detalle.iOS
{
	public class SegmentedControlRender : ViewRenderer<SegmentedControlCustom, UISegmentedControl>
	{
		public SegmentedControlRender ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SegmentedControlCustom> e)
		{
			base.OnElementChanged (e);

			var segmentedControl = new UISegmentedControl {
				TintColor = UIColor.FromRGB(1, 0.2f, 0.4f),
				ControlStyle = UISegmentedControlStyle.Bar,
				HorizontalAlignment = UIControlContentHorizontalAlignment.Center,
			};

			for (var i = 0; i < e.NewElement.Children.Count; i++) {
				segmentedControl.InsertSegment (e.NewElement.Children [i].Text, i, false);
			}

			segmentedControl.ValueChanged += (sender, eventArgs) => {
				e.NewElement.SelectedValue = segmentedControl.TitleAt(segmentedControl.SelectedSegment);
			};

			segmentedControl.SelectedSegment = 0;

			Console.WriteLine ("SEG SEL: " + segmentedControl.SelectedSegment.ToString());

			SetNativeControl (segmentedControl);
		}
			
	}
}

