﻿using System;
using Xamarin.Forms;
using Detalle;
using Detalle.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(NavigationPageCustom), typeof(NavigationPageRender))]

namespace Detalle.iOS
{
	public class NavigationPageRender : NavigationRenderer
	{
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationPageCustom obj = (NavigationPageCustom)this.Element;
			if (obj.IsTranslucent) this.NavigationBar.Translucent = true;
			if (obj.IsCombined) this.NavigationBar.BarStyle = UIBarStyle.BlackTranslucent;

			this.SetNavigationBarHidden (true, true);
			//this.NavigationBar.BackgroundColor = UIColor.FromRGBA (0,0,0,0);
			this.NavigationBar.TintColor = obj.TintColor.ToUIColor();
		}
	}
}

