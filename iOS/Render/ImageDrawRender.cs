﻿using System;
using Xamarin.Forms.Platform.iOS;
using Detalle;
using Detalle.iOS;
using Xamarin.Forms;
using System.ComponentModel;
using System.Drawing;

[assembly: ExportRenderer(typeof(ImageDrawCustom), typeof(ImageDrawRender))]

namespace Detalle.iOS
{
	public class ImageDrawRender : ViewRenderer<ImageDrawCustom, DrawView> 
	{
		DrawView dv = null;

		protected override void OnElementChanged(ElementChangedEventArgs<ImageDrawCustom> e)
		{
			base.OnElementChanged(e);

			dv = new DrawView (RectangleF.Empty);

			SetNativeControl(dv);
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == ImageDrawCustom.CurrentLineColorProperty.PropertyName)
			{
				UpdateControl();
			}
		}

		private void UpdateControl()
		{
			Control.CurrentLineColor = Element.CurrentLineColor.ToUIColor();
		}
	}
}

