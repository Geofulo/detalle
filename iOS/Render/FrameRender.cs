﻿using System;
using Detalle;
using Detalle.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using CoreGraphics;

[assembly: ExportRenderer (typeof (FrameCustom), typeof (FrameRender))]

namespace Detalle.iOS
{
	public class FrameRender : FrameRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Frame> e)
		{
			base.OnElementChanged (e);

			var obj = (FrameCustom) this.Element;
			if (obj == null)
				return;			
			Layer.ShadowRadius = (nfloat) obj.ShadowRadius;
			var height = (nfloat) obj.ShadowOffsetHeight;
			var width = (nfloat) obj.ShadowOffsetWidth;
			Layer.ShadowOffset = new CGSize(width, height);
			Layer.ShadowOpacity = obj.ShadowOpacity;
			Layer.CornerRadius = (nfloat) obj.BorderRadius;
		}
	}
}

