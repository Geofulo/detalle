﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Detalle;
using Detalle.iOS;
using CoreGraphics;
using System.ComponentModel;
using UIKit;

[assembly: ExportRenderer (typeof (EditorCustom), typeof (EditorRender))]

namespace Detalle.iOS
{
	public class EditorRender : EditorRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged (e);
			var obj = (EditorCustom) this.Element;
			if (Element == null)
				return;			
			if (Control != null) {
				Control.Layer.BorderWidth = (nfloat) obj.BorderSize;
				Control.Layer.BorderColor = obj.BorderColor.ToUIColor ().CGColor;
				Control.TextColor = obj.TextColor.ToUIColor ();
				Control.Layer.CornerRadius = (nfloat) obj.BorderRadius;
				Control.Text = obj.Placeholder;
				Control.TextColor = UIColor.LightGray;
				//Control.TextInputView.AccessibilityHint = obj.Placeholder;
				//Control.Font.PointSize;
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			var obj = (EditorCustom) this.Element;

			if (e.PropertyName == EditorCustom.TextColorProperty.PropertyName)
			{
				Control.TextColor = obj.TextColor.ToUIColor ();
			}
		}
	}
}

