﻿using System;
using Xamarin.Forms.Platform.iOS;
using Detalle;
using Xamarin.Forms;
using Detalle.iOS;

[assembly: ExportRenderer (typeof (TextCellCustom), typeof (TextCellRender))]

namespace Detalle.iOS
{
	public class TextCellRender : TextCellRenderer
	{		


		public override UIKit.UITableViewCell GetCell (Cell item, UIKit.UITableViewCell reusableCell, UIKit.UITableView tv)
		{			
			var cell = base.GetCell (item, reusableCell, tv);
			//new DataSourceRender();
			var obj = (TextCellCustom)item;
			if (obj.IsCheckmarkAccessory) {
				cell.Accessory = UIKit.UITableViewCellAccessory.Checkmark;
			} 
			if(obj.IsDetailAccessory){
				cell.Accessory = UIKit.UITableViewCellAccessory.DetailButton;
			}
			if(obj.IsDisclosureAccessory){
				cell.Accessory = UIKit.UITableViewCellAccessory.DisclosureIndicator;
			}
			return cell;
		}	
			

	}
}

