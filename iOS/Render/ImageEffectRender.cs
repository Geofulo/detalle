﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using UIKit;
using System.Diagnostics;
using CoreGraphics;
using Accelerate;
using Detalle;
using Detalle.iOS;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(ImageEffectCustom), typeof(ImageEffectRender))]

namespace Detalle.iOS
{
	public class ImageEffectRender : ImageRenderer {

		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);
			var obj = (ImageEffectCustom) this.Element;
			if (Element == null)
				return;
			if (obj.IsBlurLight) {
				var tintColor = UIColor.FromWhiteAlpha (1.0f, 0.3f);
				Control.Image = ApplyBlur (Control.Image, 30, tintColor, 1.8f);
			} 
			if (obj.IsBlurLightMedium) {
				var tintColor = UIColor.FromWhiteAlpha (1.0f, 0.3f);
				Control.Image = ApplyBlur (Control.Image, 10, tintColor, 1.8f);
				//Control.Image = ApplyBlur (Control.Image, 30, tintColor, 1.8f);
			} 
			if(obj.IsDarkBlur){
				var tintColor = UIColor.FromWhiteAlpha (0.11f, 0.73f);
				Control.Image = ApplyBlur (Control.Image, 20, tintColor, 1.8f);
			}
			if(obj.IsDarkBlurMedium){
				var tintColor = UIColor.FromWhiteAlpha (0.5f, 0.5f);
				Control.Image = ApplyBlur (Control.Image, 2, tintColor, 0.9f);
				//var tintColor = UIColor.FromWhiteAlpha (0.11f, 0.73f);
				//Control.Image = ApplyBlur (Control.Image, 20, tintColor, 1.8f);
			}
			if(obj.IsCircle){
				CreateCircle ();
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			var obj = (ImageEffectCustom) this.Element;
			if (obj.IsCircle) {
				if (e.PropertyName == VisualElement.HeightProperty.PropertyName ||
				    e.PropertyName == VisualElement.WidthProperty.PropertyName ||
				    e.PropertyName == ImageEffectCustom.BorderColorProperty.PropertyName ||
				    e.PropertyName == ImageEffectCustom.BorderSizeProperty.PropertyName) 
				{
					CreateCircle ();
				}
			}
		}

		public UIImage ApplyBlur (UIImage image, float blurRadius, UIColor tintColor, float saturationDeltaFactor)
		{
			if (image.Size.Width < 1 || image.Size.Height < 1) {
				Debug.WriteLine (@"*** error: invalid size: ({0} x {1}). Both dimensions must be >= 1: {2}", image.Size.Width, image.Size.Height, image);
				return null;
			}
			if (image.CGImage == null) {
				Debug.WriteLine (@"*** error: image must be backed by a CGImage: {0}", image);
				return null;
			}

			var imageRect = new CGRect (CGPoint.Empty, image.Size);
			var effectImage = image;

			bool hasBlur = blurRadius > float.Epsilon;
			bool hasSaturationChange = Math.Abs (saturationDeltaFactor - 1) > float.Epsilon;

			if (hasBlur || hasSaturationChange) {
				UIGraphics.BeginImageContextWithOptions (image.Size, false, UIScreen.MainScreen.Scale);
				var contextIn = UIGraphics.GetCurrentContext ();
				contextIn.ScaleCTM (1.0f, -1.0f);
				contextIn.TranslateCTM (0, -image.Size.Height);
				contextIn.DrawImage (imageRect, image.CGImage);
				var effectInContext = contextIn.AsBitmapContext () as CGBitmapContext;

				var effectInBuffer = new vImageBuffer () {
					Data = effectInContext.Data,
					Width = (int)effectInContext.Width,
					Height = (int)effectInContext.Height,
					BytesPerRow =(int) effectInContext.BytesPerRow
				};

				UIGraphics.BeginImageContextWithOptions (image.Size, false, UIScreen.MainScreen.Scale);
				var effectOutContext = UIGraphics.GetCurrentContext ().AsBitmapContext () as CGBitmapContext;
				var effectOutBuffer = new vImageBuffer () {
					Data = effectOutContext.Data,
					Width = (int)effectOutContext.Width,
					Height = (int)effectOutContext.Height,
					BytesPerRow = (int)effectOutContext.BytesPerRow
				};

				if (hasBlur) {
					var inputRadius = blurRadius * UIScreen.MainScreen.Scale;
					uint radius = (uint)(Math.Floor (inputRadius * 3 * Math.Sqrt (2 * Math.PI) / 4 + 0.5));
					if ((radius % 2) != 1)
						radius += 1;
					vImage.BoxConvolveARGB8888 (ref effectInBuffer, ref effectOutBuffer, IntPtr.Zero, 0, 0, radius, radius, Pixel8888.Zero, vImageFlags.EdgeExtend);
					vImage.BoxConvolveARGB8888 (ref effectOutBuffer, ref effectInBuffer, IntPtr.Zero, 0, 0, radius, radius, Pixel8888.Zero, vImageFlags.EdgeExtend);
					vImage.BoxConvolveARGB8888 (ref effectInBuffer, ref effectOutBuffer, IntPtr.Zero, 0, 0, radius, radius, Pixel8888.Zero, vImageFlags.EdgeExtend);
				}
				bool effectImageBuffersAreSwapped = false;
				if (hasSaturationChange) {
					var s = saturationDeltaFactor;
					var floatingPointSaturationMatrix = new float [] {
						0.0722f + 0.9278f * s,  0.0722f - 0.0722f * s,  0.0722f - 0.0722f * s,  0,
						0.7152f - 0.7152f * s,  0.7152f + 0.2848f * s,  0.7152f - 0.7152f * s,  0,
						0.2126f - 0.2126f * s,  0.2126f - 0.2126f * s,  0.2126f + 0.7873f * s,  0,
						0,                    0,                    0,  1,
					};
					const int divisor = 256;
					var saturationMatrix = new short [floatingPointSaturationMatrix.Length];
					for (int i = 0; i < saturationMatrix.Length; i++)
						saturationMatrix [i] = (short)Math.Round (floatingPointSaturationMatrix [i] * divisor);
					if (hasBlur) {
						vImage.MatrixMultiplyARGB8888 (ref effectOutBuffer, ref effectInBuffer, saturationMatrix, divisor, null, null, vImageFlags.NoFlags);
						effectImageBuffersAreSwapped = true;
					} else
						vImage.MatrixMultiplyARGB8888 (ref effectInBuffer, ref effectOutBuffer, saturationMatrix, divisor, null, null, vImageFlags.NoFlags);
				}
				if (!effectImageBuffersAreSwapped)
					effectImage = UIGraphics.GetImageFromCurrentImageContext ();
				UIGraphics.EndImageContext ();
				if (effectImageBuffersAreSwapped)
					effectImage = UIGraphics.GetImageFromCurrentImageContext ();
				UIGraphics.EndImageContext ();
			}

			// Setup up output context
			UIGraphics.BeginImageContextWithOptions (image.Size, false, UIScreen.MainScreen.Scale);
			var outputContext = UIGraphics.GetCurrentContext ();
			outputContext.ScaleCTM (1, -1);
			outputContext.TranslateCTM (0, -image.Size.Height);

			// Draw base image
			if (hasBlur) {
				outputContext.SaveState ();
				outputContext.DrawImage (imageRect, effectImage.CGImage);
				outputContext.RestoreState ();
			}

			if (tintColor != null) {
				outputContext.SaveState ();
				outputContext.SetFillColor (tintColor.CGColor);
				outputContext.FillRect (imageRect);
				outputContext.RestoreState ();
			}
			var outputImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();
			return outputImage;
		}

		private void CreateCircle()
		{
			try
			{
				double min = Math.Min(Element.Width, Element.Height);
				Control.Layer.CornerRadius = (float)(min / 2.0);
				Control.Layer.MasksToBounds = false;
				Control.Layer.BorderColor = ((ImageEffectCustom)Element).BorderColor.ToCGColor();
				Control.Layer.BorderWidth = ((ImageEffectCustom)Element).BorderSize;
				Control.ClipsToBounds = true;
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Unable to create circle image: " + ex);
			}
		}
	}
}

