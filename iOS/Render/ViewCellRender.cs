﻿using System;
using Xamarin.Forms.Platform.iOS;
using Detalle;
using Detalle.iOS;
using Xamarin.Forms;
using UIKit;

[assembly: ExportRenderer (typeof (ViewCellCustom), typeof (ViewCellRender))]

namespace Detalle.iOS
{
	public class ViewCellRender : ViewCellRenderer
	{
		public override UITableViewCell GetCell (Cell item, UITableViewCell reusableCell, UITableView tv)
		{			
			var cell = base.GetCell (item, reusableCell, tv);

			var obj = (ViewCellCustom)item;
			if (obj.IsCheckmarkAccessory) {
				cell.Accessory = UITableViewCellAccessory.Checkmark;
			} 
			if(obj.IsDetailAccessory){
				cell.Accessory = UITableViewCellAccessory.DetailButton;
			}
			if(obj.IsDisclosureAccessory){
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			return cell;
		}	
	}
}

