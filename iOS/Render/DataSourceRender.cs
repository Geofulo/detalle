﻿using System;
using UIKit;

namespace Detalle.iOS
{
	public class DataSourceRender : UITableViewDataSource
	{
		public DataSourceRender () {}

		public override UITableViewCell GetCell (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			//var cell = tableView.DequeueReusableCell("TableCell");
			var cell = new UITableViewCell (UITableViewCellStyle.Default, "TableCell");
			cell.Accessory = UIKit.UITableViewCellAccessory.Checkmark;
			return cell;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			switch (section) {
				case 0:
					return 5;
				case 1:
					return 6;
				default:
					return 0;
			}
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			throw new System.NotImplementedException ();
		}

		public override bool CanEditRow (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			return true;
		}

		/*
		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			Console.WriteLine ("Row Selected " + indexPath.Item);
		}*/
	}
}

