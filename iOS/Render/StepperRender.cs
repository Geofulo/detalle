﻿using System;
using Detalle;
using Detalle.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (StepperCustom), typeof (StepperRender))]

namespace Detalle.iOS
{
	public class StepperRender : StepperRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Stepper> e)
		{
			base.OnElementChanged (e);
			var obj = (StepperCustom) this.Element;
			if (Element == null)
				return;			
			if (Control != null) {
				Control.TintColor = obj.BorderColor.ToUIColor ();
			}
		}
	}
}

