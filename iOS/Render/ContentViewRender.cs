﻿using System;
using Xamarin.Forms.Platform.iOS;
using Detalle;
using Detalle.iOS;
using Xamarin.Forms;

[assembly: ExportRenderer (typeof (ContentViewCustom), typeof (ContentViewRender))]

namespace Detalle.iOS
{
	public class ContentViewRender : ViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged (e);

			var obj = (EditorCustom) this.Element;
			if (Element == null)
				return;			
			if (Control != null) {
				Control.Layer.BorderWidth = (nfloat) obj.BorderSize;
				Control.Layer.BorderColor = obj.BorderColor.ToUIColor ().CGColor;
			}
		}
	}
}

