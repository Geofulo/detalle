﻿using System;
using Xamarin.Forms.Platform.iOS;
using Detalle.iOS;
using Detalle;
using Xamarin.Forms;
using System.ComponentModel;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer (typeof (ImageCustom), typeof (ImageRender))]

namespace Detalle.iOS
{
	public class ImageRender : ImageRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				this.DrawImage();
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			this.DrawImage();
		}

		private void DrawImage()
		{
			var viw = (ImageCustom)this.Element;
			/*
			var image = this.GetVectorImage(
				(nfloat)this.Element.WidthRequest, 
				(nfloat)this.Element.HeightRequest,
				viw.Vector);
			*/
			if (viw.Colour.HasValue)
			{              
				this.Control.Image = UIImage.FromResource (null, "gift.svg");
				this.Control.TintColor = UIColor.Blue;
				//this.Control.Image = image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
				//this.Control.TintColor = viw.Colour.Value.ToUIColor();
			}
			else
			{
				this.Control.Image = UIImage.FromResource (null, "gift");
				this.Control.TintColor = UIColor.Blue;
				//this.Control.Image = image;
			}
		}

		private UIImage GetVectorImage(nfloat width, nfloat height, Image image)
		{
			UIImage output;

			UIGraphics.BeginImageContextWithOptions(new CGSize(width, height), false, 0f);
			/*
			if (image == Image.InfoIcon)
			{
				RocheDASAppStyleKit.DrawInfoIconCanvas((float)width, (float)height);
			}*/

			output = UIGraphics.GetImageFromCurrentImageContext();

			UIGraphics.EndImageContext();

			return output;
		}
	}
}

