﻿using System;
using UIKit;

namespace Detalle.iOS
{
	public class VESLine{
		public UIBezierPath Path {
			get;
			set;
		}

		public UIColor Color {
			get;
			set;
		}

		public byte Index {
			get;
			set;
		}
	}
}

