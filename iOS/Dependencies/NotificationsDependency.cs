﻿using System;
using Detalle;
using UIKit;
using Foundation;

[assembly: Xamarin.Forms.Dependency (typeof (INotifications))]

namespace Detalle.iOS
{
	public class NotificationsDependency : INotifications
	{
		public NotificationsDependency () {}
			
		public void ShowMessageRouteInit (){
			UILocalNotification notification = new UILocalNotification();
			NSDate.FromTimeIntervalSinceNow(15);
			notification.AlertTitle = "Regalo en camino";
			notification.AlertBody = "Su regalo ya va en camino, da click aqui para ver la ruta";
			notification.SoundName = UILocalNotification.DefaultSoundName;
			UIApplication.SharedApplication.ScheduleLocalNotification(notification);
		}

		public void ShowMessageRouteFinished (){
			UILocalNotification notification = new UILocalNotification();
			NSDate.FromTimeIntervalSinceNow(15);
			notification.AlertTitle = "Regalo entregado";
			notification.AlertBody = "Su regalo llegó a su destino";
				notification.ApplicationIconBadgeNumber = 1;
			notification.SoundName = UILocalNotification.DefaultSoundName;
			UIApplication.SharedApplication.ScheduleLocalNotification(notification);
		}
	}
}

