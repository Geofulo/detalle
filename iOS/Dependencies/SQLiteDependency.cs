﻿using System;
using Detalle;
using System.IO;

[assembly: Xamarin.Forms.Dependency (typeof (ISQLite))]

namespace Detalle.iOS
{
	public class SQLiteDependency : ISQLite
	{		
		public SQLiteDependency() {}

		public SQLite.SQLiteConnection GetConnection ()
		{
			var sqliteFilename = "Detalles1.db3";
			string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
			var path = Path.Combine(libraryPath, sqliteFilename);
			// Create the connection
			var conn = new SQLite.SQLiteConnection(path);
			// Return the database connection
			return conn;
		}
	}
}

