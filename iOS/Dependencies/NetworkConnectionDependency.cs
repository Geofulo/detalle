﻿using System;
using Detalle;
using SystemConfiguration;

[assembly: Xamarin.Forms.Dependency (typeof (INetworkConnection))]

namespace Detalle.iOS
{
	public class NetworkConnectionDependency : INetworkConnection
	{
		#region INetworkConnection implementation

		public bool IsConnected {
			get {
				//throw new NotImplementedException ();
				var internetStatus = Reachability.InternetConnectionStatus ();
				if (internetStatus == NetworkStatus.ReachableViaWiFiNetwork)
					return true;
				else
					return false;
				//NetworkStatus wifiStatus = Reachability.LocalWifiConnectionStatus ();

			}
		}

		#endregion



	}
}

