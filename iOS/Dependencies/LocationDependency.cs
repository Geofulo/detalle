﻿using System;
using Detalle;
using System.Threading.Tasks;
using CoreLocation;
using Xamarin.Forms.Maps;
using System.Threading;
using Xamarin.Forms;
using UIKit;

[assembly: Xamarin.Forms.Dependency (typeof (ILocation))]

namespace Detalle.iOS
{
	public class LocationDependency : ILocation
	{
		Map map;
		//CLLocationManager DealerLM;
		//CLLocationManager DestinationLM;
		Position DealerPosition = new Position (19.432108, -99.152056);	
		Position DestinationPosition = new Position (19.416311, -99.159900);
		Position AveragePosition;
		CLLocation DealerLocation = new CLLocation (19.432108, -99.152056);
		CLLocation DestinationLocation = new CLLocation (19.416311, -99.159900);
		Pin DealerPin;
		Pin DestinationPin;
		double AverageDistance;

		public LocationDependency () {}

		public void ManageLocation(Map map){
			this.map = map;
			ManageDestinationLocation ();
			ManageDealerLocation ();

		}

		void ManageDealerLocation ()
		{
			//DealerLM = new CLLocationManager ();
			//DealerLocation = DealerLM.Location;
			//DealerPosition = new Position (DealerLocation.Coordinate.Latitude, DealerLocation.Coordinate.Longitude);
			DealerPin = new Pin(){
				Label = "Repartidor",
				Position = DealerPosition,				
			};
			map.Pins.Add (DealerPin);
			AveragePosition = new Position ((DealerPosition.Latitude + DestinationPosition.Latitude) / 2, 
				(DealerPosition.Longitude + DestinationPosition.Longitude) / 2);
			AverageDistance = DealerLocation.DistanceFrom (DestinationLocation);
			map.MoveToRegion (MapSpan.FromCenterAndRadius(AveragePosition, Distance.FromMeters(AverageDistance / 2)));

			nint taskID = UIApplication.SharedApplication.BeginBackgroundTask( () => {});
			new Task ( () => {
				Device.BeginInvokeOnMainThread (delegate {
					DependencyService.Get<INotifications> ().ShowMessageRouteInit ();
				});

				SetDealerRoute();

				Device.BeginInvokeOnMainThread (delegate {
					DependencyService.Get<INotifications> ().ShowMessageRouteFinished ();
				});
				UIApplication.SharedApplication.EndBackgroundTask(taskID);
			}).Start();


			//DealerLM.StartUpdatingLocation();
			/*
			DealerLM.LocationsUpdated += (sender, e) => {				
				DealerLocation = e.Locations[e.Locations.Length - 1];
				DealerPosition = new Position (DealerLocation.Coordinate.Latitude, DealerLocation.Coordinate.Longitude);
				DealerPin.Position = DealerPosition;
				map.MoveToRegion (MapSpan.FromCenterAndRadius(DealerPosition, Distance.FromKilometers(2)));
			};*/
		}

			
		void ManageDestinationLocation ()
		{
			//DestinationLM = new CLLocationManager ();
			//DestinationLocation = DestinationLM.Location;
			//DestinationPosition = new Position (DestinationLocation.Coordinate.Latitude, DestinationLocation.Coordinate.Longitude);
			DestinationPin = new Pin () {
				Label = "Destino",
				Position = DestinationPosition,
				Type = PinType.Place,
			};
			map.Pins.Add (DestinationPin);
		}

		public void SetDealerRoute() {
			while (AverageDistance != 0) {		
				var newLatitude = 0.0;
				var newLongitude = 0.0;

				if (DestinationPosition.Latitude - DealerPosition.Latitude < 0) {
					newLatitude = DealerLocation.Coordinate.Latitude - 0.0005;
				} else 
					newLatitude = DestinationPosition.Latitude;
				if(DestinationPosition.Longitude - DealerPosition.Longitude < 0){
					newLongitude = DealerLocation.Coordinate.Longitude - 0.0005;
				} else 
					newLongitude = DestinationPosition.Longitude;

				DealerLocation = new CLLocation (newLatitude, newLongitude);					
				DealerPosition = new Position (newLatitude, newLongitude);

				AveragePosition = new Position ((DealerPosition.Latitude + DestinationPosition.Latitude) / 2, 
					(DealerPosition.Longitude + DestinationPosition.Longitude) / 2);
				AverageDistance = DealerLocation.DistanceFrom (DestinationLocation);
				Console.WriteLine (AverageDistance);

				Device.BeginInvokeOnMainThread (delegate {
					map.Pins.Add (new Pin(){
						Label = AverageDistance.ToString(),
						Position = DealerPosition,
					});					
					//DealerPin.Position = DealerPosition;
					map.MoveToRegion (MapSpan.FromCenterAndRadius(AveragePosition, Distance.FromMeters(AverageDistance / 2)));
				});
					
				Thread.Sleep (2000);
			}

		}


	}
}

