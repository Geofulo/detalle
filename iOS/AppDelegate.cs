﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms;

namespace Detalle.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			if (UIDevice.CurrentDevice.CheckSystemVersion (8, 0)) {
				var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes (
					UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null
				);
				app.RegisterUserNotificationSettings (notificationSettings);
			} 


			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
			#endif

			DependencyService.Register<PreferencesDependency>();
			DependencyService.Register<LocationDependency>();
			DependencyService.Register<NotificationsDependency>();
			DependencyService.Register<SQLiteDependency>();
			//DependencyService.Register<OfflineDataDependency>();
			DependencyService.Register<NetworkConnectionDependency>();

			App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;

			LoadApplication (new App ());

			return base.FinishedLaunching (app, options);
		}
	}
}

