﻿using System;
using Newtonsoft.Json;

namespace Detalle
{
	[JsonObject]
	public class CommentsModel
	{
		[JsonProperty("_id")]
		public string Id { get; set; }
		[JsonProperty("Text")]
		public string Text { get; set; }
		[JsonProperty("DatePublished")]
		public DateTime DatePublished { get; set; }
		[JsonProperty("Ranking")]
		public int Ranking { get; set; }

		public CommentsModel () {}
	}
}

