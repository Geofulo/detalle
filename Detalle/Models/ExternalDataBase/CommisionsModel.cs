﻿using System;
using Newtonsoft.Json;
using KinveyXamarin;
using SQLite.Net.Attributes;

namespace Detalle
{	
	[JsonObject]
	public class CommisionsModel
	{
		[JsonProperty("_id")]
		public string Id { get; set; }
		[JsonProperty]
		public string ReceiverName { get; set; }
		[JsonProperty]
		public string MessageWritten { get; set; }
		[JsonProperty]
		public string ProductName { get; set; }
		[JsonProperty]
		public string ProductPrice { get; set; }
		[JsonProperty]
		public string ImagePath { get; set; }

	}

}

