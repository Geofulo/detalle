﻿using System;
using SQLite;
using Newtonsoft.Json;
using KinveyXamarin;

namespace Detalle
{
	[JsonObject]
	public class OrdersModel
	{
		[JsonProperty("_id")]
		public string Id { get; set; }
		[JsonProperty]
		public int Status { get; set; }
		[JsonProperty]
		public string DateOrder { get; set; }
		[JsonProperty]
		public string DateDeliver { get; set; }
		[JsonProperty]
		public string TimeDeliver { get; set; }
		[JsonProperty]
		public KinveyReference<CommisionsModel>[] Commisions { get; set; }
		[JsonProperty]
		public string Address { get; set; }
		[JsonProperty]
		public string Reference { get; set; }
		[JsonProperty]
		public string Phone { get; set; }
	}

}

