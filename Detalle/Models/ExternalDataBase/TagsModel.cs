﻿using System;
using Newtonsoft.Json;
using KinveyXamarin;

namespace Detalle
{
	[JsonObject]
	public class TagsModel
	{		
		[JsonProperty("_id")]
		public string Id { get; set; }
		[JsonProperty]
		public string Name { get; set; }
		[JsonProperty("_kmd")]
		private KinveyMetaData meta {get; set;} 

		//public TagsModel () {}
	}
}

