﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using KinveyXamarin;


namespace Detalle
{
	[JsonObject]
	//[JsonObject(MemberSerialization.OptOut)]
	public class ProductsModel
	{
		[JsonProperty("_id")]
		public string Id { get; set; }
		[JsonProperty]
		public string Name { get; set; }
		[JsonProperty]
		public string Description { get; set; }
		[JsonProperty]
		public float Price { get; set; }
		[JsonProperty]
		public int Priority { get; set; }
		//public List<CommentsModel> Comments { get; set; }
		[JsonProperty]
		public int DeliveredCount { get; set; }
		[JsonProperty]
		public int RankingAverage { 
			get;
			set;
			/*
			get { 
				int average = 0;
				foreach(CommentsModel comment in Comments){
					average += comment.Ranking;
				}
				return average / Comments.Count;
			}*/
		}
		[JsonProperty]
		public KinveyReference<CategoriesModel> Category { get; set; }
		[JsonProperty]
		public KinveyReference<TagsModel>[] Tags { get; set; }	
		[JsonProperty]
		public KinveyReference<AttributesModel>[] Attributes { get; set; }	
		[JsonProperty]
		public FileMetaData[] Images { get; set; }	
		


	}
}

