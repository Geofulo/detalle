﻿using System;
using Newtonsoft.Json;

namespace Detalle
{
	[JsonObject]
	public class AttributesModel
	{
		[JsonProperty("_id")]
		public string Id { get; set; }
		[JsonProperty]
		public string Key { get; set; }
		[JsonProperty]
		public string Value { get; set; }
	}
}

