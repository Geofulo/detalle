﻿using System;
using Newtonsoft.Json;

namespace Detalle
{
	[JsonObject]
	public class CategoriesModel
	{
		[JsonProperty("_id")]
		public string Id { get; set; }
		[JsonProperty]
		public string Name { get; set; }
	}
}

