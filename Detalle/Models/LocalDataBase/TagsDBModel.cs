﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class TagsDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdTag { get; set; }

		public string IdKinvey { get; set; }

		public string Name { get; set; }
	}
}

