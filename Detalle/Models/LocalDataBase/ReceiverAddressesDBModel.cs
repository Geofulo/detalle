﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class ReceiverAddressesDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdReceiverAddress { get; set; }

		public int IdReceiver { get; set; }

		public int IdAddress { get; set; }
	}
}

