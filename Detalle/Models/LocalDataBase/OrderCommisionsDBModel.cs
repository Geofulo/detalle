﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class OrderCommisionsDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdOrderCommision { get; set; }

		public int IdOrder { get; set; }

		public string ReceiverName { get; set; }

		public string MessageWritten { get; set; }

		public string ProductName { get; set; }

		public float ProductPrice { get; set; }

		public string ImagePath { get; set; }
	}
}

