﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class CategoriesDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdCategory { get; set; }

		public string IdKinvey { get; set; }

		public string Name { get; set; }
	}
}

