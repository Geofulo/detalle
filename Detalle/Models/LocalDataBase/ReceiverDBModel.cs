﻿using System;
using Newtonsoft.Json;
using KinveyXamarin;
using System.ComponentModel;
using SQLite;
using System.Collections.Generic;

namespace Detalle
{
	public class ReceiverDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdReceiver { get; set; }

		public string Name { get; set; }

		public string Email { get; set; }

		public List<AddressDBModel> Addresses {
			get { 
				return App.LocalDataMan.GetAddresses (this);
			}
		}

	}		
}

