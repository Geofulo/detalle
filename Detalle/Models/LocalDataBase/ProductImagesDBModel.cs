﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class ProductImagesDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdProductImage { get; set; }

		public int IdProduct { get; set; }

		public string Path { get; set; }
	}
}

