﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class ProductsDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdProduct { get; set; }

		public int IdCategory { get; set; }

		public string IdKinvey { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public float Price { get; set; }

		public int Priority { get; set; }

		public bool IsElectronic { get; set; }

	}
}

