﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class CommisionsDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdCommision { get; set; }

		public int IdReceiver { get; set; }

		public int IdProduct { get; set; }

		public string MessageWritten { get; set; }

		public bool InCart { get; set; }

		public ProductsDBModel Product { 
			get { 
				return App.LocalDataMan.getProduct (this.IdProduct);
			} 
		}

		public string ReceiverName { 
			get { 
				return App.LocalDataMan.getReceiver (this.IdReceiver).Name;
			} 
		}

	}
}

