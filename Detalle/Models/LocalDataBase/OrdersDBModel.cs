﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class OrdersDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdOrder { get; set; }

		public string IdKinvey { get; set; }

		public int Status { get; set; }

		public string DateOrder { get; set; }

		public string TimeDeliver { get; set; }

		public string DateDeliver { get; set; }

		public string Address { get; set; }

		public string Reference { get; set; }

		public string Phone { get; set; }

		public string Sender { get; set; }

		public string SenderPhone { get; set; }
	}
}

