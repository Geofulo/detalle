﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class ProductTagsDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdProductTag { get; set; }

		public int IdProduct { get; set; }

		public int IdTag { get; set; }
	}
}

