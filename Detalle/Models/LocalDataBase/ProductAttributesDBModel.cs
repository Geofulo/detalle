﻿using System;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class ProductAttributesDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdProductAttribute { get; set; }

		public int IdProduct { get; set; }

		public string Key { get; set; }

		public string Value { get; set; }
	}
}

