﻿using System;
using Newtonsoft.Json;
using KinveyXamarin;
using System.ComponentModel;
using SQLite;

namespace Detalle
{
	public class AddressDBModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdAddress { get; set; }

		public string TagAddress { get; set; }

		public string Address { get; set; }

		public string Reference { get; set; }

	}

}

