﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class DeveloperPage : TabbedPage
	{
		DevProductsPage ProductsPage;

		public DeveloperPage ()
		{			
			createPages ();

			Title = "Developer";
			Children.Add (ProductsPage);
		}

		void createPages ()
		{
			ProductsPage = new DevProductsPage ();
		}
	}
}

