﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class DevProductsPage : ContentPage
	{
		ToolbarItem AddItem;

		public DevProductsPage ()
		{
			createElements ();
			setListeners ();

			Title = "Productos";
			ToolbarItems.Add (AddItem);
		}

		void createElements ()
		{
			AddItem = new ToolbarItem {
				Text = "Agregar",
			};
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				
			};
		}
	}
}

