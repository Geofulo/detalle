﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class DevCreateProductPage : ContentPage
	{
		ToolbarItem SaveItem;

		public DevCreateProductPage ()
		{
			createElements ();
			setListeners ();

			Title = "Crear producto";
			ToolbarItems.Add (SaveItem);
		}

		void createElements ()
		{
			SaveItem = new ToolbarItem {
				Text = "Guardar",
			};
		}

		void setListeners ()
		{
			SaveItem.Clicked += (sender, e) => {

			};
		}
	}
}

