﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class MyOrdersPendingTemplate : ViewCell
	{
		Label NameProductLabel;
		Label DateOrderLabel;
		Label DateDeliverLabel;
		StackLayout Stack;
		StackLayout DatesStack;
		BoxView StatusView;

		public MyOrdersPendingTemplate ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();
			setListeners ();

			View = Stack;
		}

		void createElements ()
		{
			NameProductLabel = new Label {
				FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.CenterAndExpand,	
				HorizontalOptions = LayoutOptions.StartAndExpand					
			};
			DateOrderLabel = new Label ();
			DateDeliverLabel = new Label ();
			StatusView = new BoxView ();
		}

		void createLayouts ()
		{
			Stack = new StackLayout (){
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Padding = new Thickness(10,0),
			};
			DatesStack = new StackLayout (){
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.EndAndExpand
			};
		}

		void setLayouts ()
		{
			DatesStack.Children.Add (DateOrderLabel);
			DatesStack.Children.Add (DateDeliverLabel);

			Stack.Children.Add (NameProductLabel);
			Stack.Children.Add (DatesStack);
		}

		void setBindings ()
		{
			//NameProductLabel.SetBinding (Label.TextProperty, "Product.Resolved.Name");
			DateOrderLabel.SetBinding (Label.TextProperty, "DateOrder");
			DateDeliverLabel.SetBinding (Label.TextProperty, "DateDeliver");
		}

		void setListeners ()
		{
			
		}
	}
}

