﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class MyOrdersPendingPage : ContentPage
	{
		ListView PendingListView;
		List<OrdersDBModel> Orders;

		public MyOrdersPendingPage ()
		{
			getOrders ();
			setElements ();
			setListeners ();

			Title = "Solicitados";
			Content = PendingListView;
		}

		void getOrders ()
		{
			Orders = App.LocalDataMan.GetPendingOrders ();
		}

		void setElements ()
		{
			PendingListView = new ListView {
				RowHeight = 60,
				ItemsSource = Orders,
				ItemTemplate = new DataTemplate (GetOrdersPendingTemplate),
				IsPullToRefreshEnabled = true,
			};
		}

		void setListeners ()
		{
			PendingListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var item = e.SelectedItem as OrdersModel;
				//this.Navigation.PushAsync(new MyOrdersDetailPage(item));
				PendingListView.SelectedItem = null;
			};
			PendingListView.Refreshing += (sender, e) => {
				getOrders ();
				getOrders ();
				PendingListView.ItemsSource = Orders;
				PendingListView.EndRefresh();
			};
		}


		Cell GetOrdersPendingTemplate ()
		{
			var cell = new MyOrdersPendingTemplate ();
			var cancelItem = new MenuItem () {
				Text = "Cancelar",
			};
			cancelItem.SetBinding (MenuItem.CommandParameterProperty, ".");
			cancelItem.Clicked += CancelPendingOrder;
			cell.ContextActions.Add(cancelItem);
			return cell;
		}

		void CancelPendingOrder (object sender, EventArgs e)
		{
			DisplayAlert ("Proximamente", "Esta función estará disponible proximamente", "OK");
		}
	}
}

