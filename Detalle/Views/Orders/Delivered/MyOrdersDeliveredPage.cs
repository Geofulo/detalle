﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class MyOrdersDeliveredPage : ContentPage
	{
		List<OrdersDBModel> Orders;
		ToolbarItem DeleteAllItem;
		ListView DeliveredListView;

		public MyOrdersDeliveredPage ()
		{			
			getOrders ();
			createElements ();
			setListeners ();

			Title = "Entregados";
			ToolbarItems.Add (DeleteAllItem);
			Content = DeliveredListView;
		}

		void getOrders ()
		{
			Orders = App.LocalDataMan.GetDeliveredOrders ();
		}

		void createElements ()
		{
			DeleteAllItem = new ToolbarItem {
				Text = "Vaciar"
			};
			DeliveredListView = new ListView {
				RowHeight = 60,
				ItemsSource = Orders,
				ItemTemplate = new DataTemplate (GetMyOrdersDeliveredTemplate),
				IsPullToRefreshEnabled = true,
			};
		}

		void setListeners ()
		{
			DeliveredListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var item = e.SelectedItem as OrdersDBModel;
				//this.Navigation.PushAsync(new MyOrdersDetailPage(item));
				DeliveredListView.SelectedItem = null;
			};
			DeliveredListView.Refreshing += (sender, e) => {
				getOrders ();
				DeliveredListView.ItemsSource = Orders;
				DeliveredListView.EndRefresh();
			};
		}

		Cell GetMyOrdersDeliveredTemplate ()
		{
			var cell = new MyOrdersDeliveredTemplate ();
			var deleteItem = new MenuItem () {
				Text = "Eliminar",
				IsDestructive = true
			};
			deleteItem.SetBinding (MenuItem.CommandParameterProperty, ".");
			deleteItem.Clicked += DeleteDeliveredOrder;
			cell.ContextActions.Add(deleteItem);
			return cell;
		}

		async void DeleteDeliveredOrder (object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);
			var orderSelected = mi.CommandParameter as OrdersDBModel;
			var isDeleted = App.LocalDataMan.DeleteDeliveredOrder (orderSelected);
			//var isDeleted = await App.DataMan.DeleteOrder (orderSelected);
			if (isDeleted) {				
				getOrders ();
				DeliveredListView.ItemsSource = Orders;
			} else {
				DisplayAlert ("Error", "Ocurrió un error al intentar eliminar éste pedido", "OK");  
			}
		}
	}
}

