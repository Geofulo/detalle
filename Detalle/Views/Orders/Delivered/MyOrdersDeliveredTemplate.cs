﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class MyOrdersDeliveredTemplate : ViewCell
	{
		Label NameProductLabel;
		Label DateDeliverLabel;
		StackLayout Stack;

		public MyOrdersDeliveredTemplate ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();

			View = Stack;
		}

		void createElements ()
		{
			NameProductLabel = new Label {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontAttributes = FontAttributes.Bold,
			};
			DateDeliverLabel = new Label {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10,0),
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (NameProductLabel);
			Stack.Children.Add (DateDeliverLabel);
		}

		void setBindings ()
		{
			NameProductLabel.SetBinding (Label.TextProperty, "Product.Name");
			DateDeliverLabel.SetBinding (Label.TextProperty, "DateDeliver");
		}
	}
}

