﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class MyOrdersPage : TabbedPage
	{		
		ContentPage PendingPage;
		ContentPage DeliveredPage;

		public MyOrdersPage ()
		{
			createPages ();
			setMessagingCenter ();

			Title = "Envíos";
			Children.Add (PendingPage);
			Children.Add (DeliveredPage);
		}

		void createPages ()
		{
			PendingPage = new MyOrdersPendingPage ();
			DeliveredPage = new MyOrdersDeliveredPage ();
		}

		void setMessagingCenter ()
		{
			MessagingCenter.Subscribe<MyOrdersPendingTemplate> (this, "OpenMap", (sender) => { 
				this.Navigation.PushModalAsync(new NavigationPage(new MapPage()));
			});
		}
	}
}

