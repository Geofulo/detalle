﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class MyOrdersDetailPage : ContentPage
	{
		OrdersDBModel Order;
		AddressDBModel Address;
		ToolbarItem OKItem;
		StackLayout Stack;

		public MyOrdersDetailPage (OrdersDBModel Order, AddressDBModel Address)
		{
			this.Order = Order;
			this.Address = Address;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();		

			Title = "Orden de compra";
			ToolbarItems.Add (OKItem);
			Content = Stack;
		}	

		void createElements ()
		{
			OKItem = new ToolbarItem {
				Text = "Pagar",
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 10,
				Padding = new Thickness(10),
			};
		}

		void setLayouts ()
		{
			
		}

		void setListeners ()
		{
			OKItem.Clicked += (sender, e) => {				
				var commisions = App.LocalDataMan.GetShoppingCart();
				foreach(var commision in commisions)
				{
					var product = App.LocalDataMan.getProduct(commision.IdProduct);
					var receiver = App.LocalDataMan.getReceiver(commision.IdReceiver);
					var orderCommision = new OrderCommisionsDBModel {
						ProductName = product.Name,
						ProductPrice = product.Price,
						MessageWritten = commision.MessageWritten,
						ReceiverName = App.LocalDataMan.getReceiver(commision.IdReceiver).Name,
						IdOrder = Order.IdOrder,
					};
					if (App.LocalDataMan.CreateOrderCommision(orderCommision)) {
						if (App.LocalDataMan.DeleteCommision(commision)) {
							if (App.LocalDataMan.CreateAddress(Address)) {
								var receiverAddress = App.LocalDataMan.GetReceiverAddresses(receiver, Address);
								if (receiverAddress == null) {
									var receiverAddressCreated = new ReceiverAddressesDBModel() {
										IdAddress = Address.IdAddress,
										IdReceiver = receiver.IdReceiver,
									};
									if (!App.LocalDataMan.CreateReceiverAddress(receiverAddressCreated)) {
										DisplayAlert("Error", "Ocurrió un error al intentar agregar la dirección al destinatario", "OK");
									}
								}
							} else {
								DisplayAlert("Error", "Ocurrió un error al intentar crear la dirección", "OK");
							}
						} else {
							DisplayAlert("Error", "Ocurrió un error al intentar quitar el producto del carrito", "OK");
						}
					} else {
						DisplayAlert("Error", "Ocurrió un error al intentar agregar el producto al pedido", "OK");
					}
				}
				if (App.LocalDataMan.CreatePendingOrder(Order)) {
					MessagingCenter.Send<MyOrdersDetailPage>(this, "OrderPaid");
				} else {
					DisplayAlert("Error", "Ocurrió un error al intentar pagar el pedido", "OK");
				}
			};
		}

	}
}

