﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class CreateSenderPage : ContentPage
	{
		OrdersDBModel Order;
		AddressDBModel Address;
		ToolbarItem OKItem;
		StackLayout Stack;
		Entry NameEntry;
		Entry PhoneEntry;

		public CreateSenderPage (OrdersDBModel Order, AddressDBModel Address)
		{
			this.Order = Order;
			this.Address = Address;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "¿De quién?";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (OKItem);
			Content = Stack;
		}

		void createElements ()
		{
			OKItem = new ToolbarItem {
				Text = "Continuar",
			}; 
			NameEntry = new Entry {
				Placeholder = "Remitente",
				HeightRequest = 60,
			};
			PhoneEntry = new Entry {
				Placeholder = "Teléfono del remitente",
				HeightRequest = 60,
				Keyboard = Keyboard.Telephone,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout (){
				Padding = new Thickness(10),
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (NameEntry);
			Stack.Children.Add (PhoneEntry);
		}

		void setListeners ()
		{
			OKItem.Clicked += (sender, e) => {				
				Order.Sender = NameEntry.Text;
				Order.SenderPhone = PhoneEntry.Text;
				Navigation.PushAsync(new OrdersDateTimePage(Order, Address));
			};
		}
	}
}

