﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class OrdersPayPage : ContentPage
	{
		OrdersDBModel Order;
		ToolbarItem OKItem;

		public OrdersPayPage (OrdersDBModel Order)
		{
			this.Order = Order;

			createElements ();
			setListeners ();

			Title = "Método de Pago";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (OKItem);
		}

		void createElements ()
		{
			OKItem = new ToolbarItem { 
				Text = "Terminar"
			};
		}

		void setListeners ()
		{
			OKItem.Clicked += (sender, e) => {			
				if (App.LocalDataMan.CreatePendingOrder(Order)) {
					MessagingCenter.Send<OrdersPayPage>(this, "OrderPaid");
				} else {
					DisplayAlert("Error", "Se ha producido un error al intentar pagar el pedido", "OK");
				}
			};
		}
	}
}

