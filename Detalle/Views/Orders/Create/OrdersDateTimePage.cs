﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class OrdersDateTimePage : ContentPage
	{
		OrdersDBModel Order;
		AddressDBModel Address;

		ToolbarItem OKItem;
		TableView DeliverTable;
		ViewCell Deliver1Cell;
		ViewCell Deliver2Cell;
		ViewCell Deliver3Cell;
		ViewCell DeliverPersonalizeCell;
		DatePicker OrderDatePicker;
		TimePicker OrderTimePicker;

		StackLayout Stack;
		ScrollView Scroll;

		public OrdersDateTimePage (OrdersDBModel Order, AddressDBModel Address)  
		{
			this.Order = Order;
			this.Address = Address;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();		

			Title = "¿Cuándo?";
			NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add (OKItem);
			Content = Scroll;
		}

		void createElements ()
		{
			OKItem = new ToolbarItem {
				Text = "Continuar",
			};
			Deliver1Cell = new ViewCell {
				View = new StackLayout {
					BackgroundColor = Color.White,
					Padding = new Thickness(20, 10),
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					Children = {									
						new StackLayout {
							Children = {
								new Label {
									Text = "Estafeta",
									WidthRequest = 150,
									FontAttributes = FontAttributes.Bold,
								},
								new Label {
									Text = "2-3 días hábiles",
									TextColor = Color.FromHex("6563a4"),
								},
							},
						},
						new Label {
							Text = "fecha aproximada...",
							TextColor = Color.Gray,
						},
					},
				},
			};
			Deliver2Cell = new ViewCell {
				View = new StackLayout {
					BackgroundColor = Color.White,
					Padding = new Thickness(20, 10),
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					Children = {
						new StackLayout {
							Children = {
								new Label {
									Text = "Pullman",
									WidthRequest = 150,
									FontAttributes = FontAttributes.Bold,
								},
								new Label {
									Text = "5 días hábiles",
									TextColor = Color.FromHex("6563a4"),
								},
							},
						},
						new Label {
							Text = "fecha aproximada...",
							TextColor = Color.Gray,
						},
					},
				},
			};
			Deliver3Cell = new ViewCell {
				View = new StackLayout {
					BackgroundColor = Color.White,
					Padding = new Thickness(20, 10),
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					Children = {
						new StackLayout {
							Children = {
								new Label {
									Text = "Correos de México",
									WidthRequest = 150,
									FontAttributes = FontAttributes.Bold,
								},
								new Label {
									Text = "7 días hábiles",
									TextColor = Color.FromHex("6563a4"),
								},
							},
						},
						new Label {
							Text = "fecha aproximada...",
							TextColor = Color.Gray,
						},
					},
				},
			};
			DeliverPersonalizeCell = new ViewCell {
				View = new StackLayout {
					BackgroundColor = Color.White,
					Padding = new Thickness(20, 10),
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					Children = {
						new StackLayout {
							Children = {
								new Label {
									Text = "Calendario",
									WidthRequest = 150,
									FontAttributes = FontAttributes.Bold,
								},
								new Label {
									Text = "Fecha específica",
									TextColor = Color.FromHex("6563a4"),
								},
							},
						},
						new Label {
							Text = "fecha aproximada...",
							TextColor = Color.Gray,
						},
					},
				},
			};
			DeliverTable = new TableView {
				BackgroundColor = Color.FromHex ("f8f8f9"),
				RowHeight = 70,
				HasUnevenRows = true,
				Root = new TableRoot {					
					new TableSection {
						Deliver1Cell, 
						Deliver2Cell, 
						Deliver3Cell,
						DeliverPersonalizeCell,
					},
				},
				Intent = TableIntent.Settings,
			};
			OrderDatePicker = new DatePicker {
				MinimumDate = DateTime.Now.Date,
				HeightRequest = 60,
			};
			OrderTimePicker = new TimePicker {
				Time = DateTime.Now.TimeOfDay,
				HeightRequest = 60,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 10,
				//Padding = new Thickness(10),
			};
			Scroll = new ScrollView ();
		}

		void setLayouts ()
		{
			//Stack.Children.Add (OrderDatePicker);
			//Stack.Children.Add (OrderTimePicker);
			Stack.Children.Add (DeliverTable);
			Scroll.Content = Stack;
		}

		void setListeners ()
		{
			OKItem.Clicked += (sender, e) => {
				Order.DateDeliver = OrderDatePicker.Date.ToString();
				Order.TimeDeliver = OrderTimePicker.Time.ToString();
				Navigation.PushAsync(new MyOrdersDetailPage(Order, Address));
			};

		}
	}
}

