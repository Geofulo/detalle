﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class OrdersNotesPage : TabbedPage
	{
		ProductsDBModel Product;
		OrdersDBModel Order;
		ReceiverDBModel Receiver; 
		string Address;
		DateTime Date; 
		TimeSpan Time;
		ToolbarItem OKItem;
		OrdersNotesDrawPage NotesDrawPage;
		OrdersNotesWritePage NotesWritePage;
		bool isNewOrder;
		public bool can2Backs = false;

		public OrdersNotesPage (ProductsDBModel Product, ReceiverDBModel Receiver, string Address, OrdersDBModel Order, bool isNewOrder)
		{
			this.Product = Product;
			this.Receiver = Receiver;
			this.Address = Address;
			this.Order = Order;
			this.isNewOrder = isNewOrder;

			createElements ();
			createPages ();
			setListeners ();

			Title = "Nota";
			ToolbarItems.Add (OKItem);
			Children.Add (NotesDrawPage);
			Children.Add (NotesWritePage);
		}			

		void createElements ()
		{			
			OKItem = new ToolbarItem {
				Text = "Continuar",
			};
		}

		void createPages ()
		{
			NotesDrawPage = new OrdersNotesDrawPage ();
			NotesWritePage = new OrdersNotesWritePage ();
		}

		void setListeners ()
		{
			OKItem.Clicked += (sender, e) => {
				
			}; 

		}
			
		protected override bool OnBackButtonPressed ()
		{
			System.Diagnostics.Debug.WriteLine ("Back!");
			base.OnBackButtonPressed ();
			return true;
			//MessagingCenter.Send<OrdersNotesPage> (this, "2Backs");
			//Navigation.PopToRootAsync ();

		}
	}
}

