﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class OrdersNotesWritePage : ContentPage
	{			
		Label MessageLabel;
		public Editor MessageEditor;
		StackLayout Stack;

		public OrdersNotesWritePage ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();

			Title = "Escribir";
			Content = Stack;
		}			

		void createElements ()
		{
			MessageLabel = new Label {
				Text = "Escribe tu nota:",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				HeightRequest = 60,
			};
			MessageEditor = new Editor {
				HeightRequest = 120,
				BackgroundColor = Color.Gray,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout ();
		}

		void setLayouts ()
		{
			Stack.Children.Add (MessageLabel);
			Stack.Children.Add (MessageEditor);
		}
	}
}

