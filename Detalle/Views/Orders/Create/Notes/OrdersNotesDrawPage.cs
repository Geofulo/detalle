﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class OrdersNotesDrawPage : ContentPage
	{
		Label MessageLabel;
		Button CleanButton;
		ImageDrawCustom ImageDraw;
		Frame ImageFrame;
		ContentView ImageView;
		List<Color> Colors = new List<Color>{Color.Black, Color.Red, Color.Blue, Color.Green, 
			Color.Yellow, Color.Pink, Color.Purple, Color.Maroon, Color.Navy, Color.Silver, Color.Teal, Color.Aqua, Color.Lime};
		List<Button> ColorsButtons = new List<Button> ();
		ScrollView ColorsScroll;
		StackLayout ColorsStack;
		StackLayout ToolsStack;
		StackLayout Stack;
		int colorSelectedIndex = 0;

		public OrdersNotesDrawPage ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();
			setListeners ();

			Title = "Dibujar";
			Content = Stack;
		}

		void createElements ()
		{
			MessageLabel = new Label {
				Text = "Dibuja tu nota",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			ImageDraw = new ImageDrawCustom {
				CurrentLineColor = Color.Blue,
				BackgroundColor = Color.White,
			};
			CleanButton = new Button {
				Text = "Borrador",
			};
			foreach(var color in Colors) {
				ColorsButtons.Add (new Button {
					HeightRequest = 30,
					WidthRequest = 30,
					BackgroundColor = color,
					TextColor = Color.White,
				});
				if(Colors.IndexOf(color) == 0) {
					ColorsButtons [0].IsEnabled = false;
					ColorsButtons [0].Opacity = 0.8;
					ColorsButtons [0].Text = "0";
				}
			}
		}

		void createLayouts ()
		{			
			ImageFrame = new Frame ();
			ImageView = new ContentView {
				WidthRequest = 120,
				HeightRequest = 420,
				Padding = new Thickness(5),
				VerticalOptions = LayoutOptions.Fill,
			};

			ColorsStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			ColorsScroll = new ScrollView {
				Orientation = ScrollOrientation.Horizontal,
			};

			ToolsStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 10,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			Stack = new StackLayout ();
		}

		void setLayouts ()
		{			
			ImageFrame.Content = ImageDraw;
			ImageView.Content = ImageFrame;

			foreach(var colorBtn in ColorsButtons) {
				ColorsStack.Children.Add (colorBtn);
			}
			ColorsScroll.Content = ColorsStack;

			ToolsStack.Children.Add (CleanButton);

			Stack.Children.Add (ImageView);
			Stack.Children.Add (ColorsScroll);
			Stack.Children.Add (ToolsStack);
		}

		void setBindings ()
		{
			ImageDraw.SetBinding(ImageDrawCustom.CurrentLineColorProperty, "CurrentLineColor");
		}

		void setListeners ()
		{
			CleanButton.Clicked += (sender, e) => {
				ImageDraw.CurrentLineColor = Color.White;
				CleanButton.IsEnabled = false;
				CleanButton.Focus();
			};
			foreach(var colorBtn in ColorsButtons)
			{
				colorBtn.Clicked += (sender, e) => {
					ImageDraw.CurrentLineColor = colorBtn.BackgroundColor;
					colorBtn.IsEnabled = false;
					colorBtn.Opacity = 0.8;
					colorBtn.Text = "0";
					ColorsButtons[colorSelectedIndex].IsEnabled = true;
					ColorsButtons[colorSelectedIndex].Opacity = 1;
					ColorsButtons[colorSelectedIndex].Text = "";
					colorSelectedIndex = ColorsButtons.IndexOf(colorBtn);
					CleanButton.IsEnabled = true;
					CleanButton.Unfocus();
				};
			}
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			MessagingCenter.Send<OrdersNotesDrawPage> (this, "DisableAnimationMenu");
		}

		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();
			MessagingCenter.Send<OrdersNotesDrawPage> (this, "EnableAnimationMenu");
		}

		protected override bool OnBackButtonPressed ()
		{				
			System.Diagnostics.Debug.WriteLine ("Back!");
			base.OnBackButtonPressed ();
			return true;
			//Navigation.PopToRootAsync ();

			//MessagingCenter.Send<OrdersNotesDrawPage> (this, "2Backs");

		}
			
	}
}

