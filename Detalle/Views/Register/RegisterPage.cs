﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class RegisterPage : ContentPage
	{
		ToolbarItem CancelItem;
		ToolbarItem OKItem;
		Entry UsernameEntry;
		Entry PasswordEntry;
		Entry ConfirmPasswordEntry;
		StackLayout Stack;

		public RegisterPage ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Registro";
			ToolbarItems.Add (CancelItem);
			ToolbarItems.Add (OKItem);
			Content = Stack;
		}

		void createElements ()
		{
			CancelItem = new ToolbarItem () {
				Text = "Cancelar",
			};
			OKItem = new ToolbarItem {
				Text = "Hecho",
			};
			UsernameEntry = new Entry {
				Placeholder = "Username",
				HeightRequest = 40,
			};
			PasswordEntry = new Entry {
				Placeholder = "Contraseña",
				IsPassword = true,
				HeightRequest = 40,
			};
			ConfirmPasswordEntry = new Entry{ 
				Placeholder = "Confirmar contraseña",
				IsPassword = true,
				HeightRequest = 40,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout ();
		}

		void setLayouts ()
		{
			Stack.Children.Add (UsernameEntry);
			Stack.Children.Add (PasswordEntry);
			Stack.Children.Add (ConfirmPasswordEntry);
		}

		void setListeners ()
		{
			CancelItem.Clicked += (sender, e) => {
				this.Navigation.PopModalAsync();
			};
			OKItem.Clicked += (sender, e) => {
				if(PasswordEntry.Text.Equals(ConfirmPasswordEntry.Text)){
					App.ActiveUser = UserManager.SignUp(UsernameEntry.Text, PasswordEntry.Text);	
					MessagingCenter.Send<RegisterPage>(this, "SignUp");
				} else {
					
				}
			};
		}
	}
}

