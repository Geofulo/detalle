﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class CalendarPage : ContentPage
	{
		ToolbarItem CelebrationsItem;

		public CalendarPage ()
		{
			createElements ();
			setListeners ();

			Title = "Ocasiones";
			ToolbarItems.Add (CelebrationsItem);
		}

		void createElements ()
		{
			CelebrationsItem = new ToolbarItem {
				Text = "Festejos",
			};
		}

		void setListeners ()
		{
			CelebrationsItem.Clicked += (sender, e) => {
				this.Navigation.PushAsync(new CelebrationsPage());
			};
		}
	}
}

