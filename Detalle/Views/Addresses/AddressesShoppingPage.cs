﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class AddressesShoppingPage : ContentPage
	{
		List<AddressDBModel> Addresses;
		OrdersDBModel Order;
		ReceiverDBModel Receiver;

		Image ReceiverImage;
		Label ReceiverNameLabel;
		ToolbarItem AddItem;
		ListView AddressesListView;

		StackLayout StackReceiver;
		StackLayout Stack;

		public AddressesShoppingPage (OrdersDBModel Order, ReceiverDBModel Receiver)
		{			
			this.Order = Order;
			this.Receiver = Receiver;

			getAddresses ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "¿A dónde?";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (AddItem);
			Content = Stack;
			Padding = new Thickness (20, 20);
		}

		void getAddresses ()
		{
			Addresses = App.LocalDataMan.GetAddresses (Receiver);
		}

		void createElements ()
		{
			AddItem = new ToolbarItem {
				Text = "Agregar",
			};
			AddressesListView = new ListView {				
				RowHeight = 90,
				ItemsSource = Addresses,
				ItemTemplate = new DataTemplate(GetAddressTemplate),
				IsPullToRefreshEnabled = true,
			};
			ReceiverImage = new ImageEffectCustom {
				Source = ImageSource.FromResource("Chocolates.png"),
				WidthRequest = 50,
				HeightRequest = 50,
				Aspect = Aspect.AspectFill,
				IsCircle = true,
			};
			ReceiverNameLabel = new Label {
				Text = Receiver.Name,
				FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			StackReceiver = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
			};
			Stack = new StackLayout {
				Spacing = 10,
			};
		}

		void setLayouts ()
		{
			StackReceiver.Children.Add (ReceiverImage);
			StackReceiver.Children.Add (ReceiverNameLabel);

			Stack.Children.Add (StackReceiver);
			Stack.Children.Add (AddressesListView);
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				var addressCreatePage = new AddressesCreatePage(Order);
				addressCreatePage.Title = "Nuevo";
				Navigation.PushAsync(addressCreatePage);
			};
			AddressesListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var addressSelected = e.SelectedItem as AddressDBModel;
				Navigation.PushAsync(new CreateSenderPage(Order, addressSelected));
				AddressesListView.SelectedItem = null;
			};				
			AddressesListView.Refreshing += (sender, e) => {
				getAddresses();
				AddressesListView.ItemsSource = Addresses;
				AddressesListView.EndRefresh();
			};
		}			

		Cell GetAddressTemplate ()
		{
			var cell = new AddressesTemplate ();
			var deleteItem = new MenuItem () {
				Text = "Eliminar",
				IsDestructive = true
			};
			deleteItem.SetBinding (MenuItem.CommandParameterProperty, ".");
			deleteItem.Clicked += DeleteAddress;
			cell.ContextActions.Add(deleteItem);
			return cell;
		}

		void DeleteAddress (object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);
			var addressSelected = mi.CommandParameter as AddressDBModel;
			App.LocalDataMan.DeleteReceiverAddress (Receiver, addressSelected);
			if (App.LocalDataMan.DeleteAddress (addressSelected)) {								
				Addresses.Remove (addressSelected);
				AddressesListView.ItemsSource = Addresses;
			} else {
				DisplayAlert ("Error", "Ocurrió un error al intentar eliminar éste producto", "OK");  
			}
		}
	}
}

