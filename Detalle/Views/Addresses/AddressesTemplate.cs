﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class AddressesTemplate : ViewCell
	{
		Label TagLabel;
		Label Address1Label;

		MenuItem DeleteItem;
		StackLayout Stack;

		public AddressesTemplate ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();
			setListeners ();

			//ContextActions.Add (DeleteItem);
			View = Stack;
		}

		void createElements ()
		{
			TagLabel = new Label {
				TextColor = Color.FromHex("6563a4"),
				FontAttributes = FontAttributes.Bold,
				YAlign = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			Address1Label = new Label { 
				YAlign = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			DeleteItem = new MenuItem {
				Text = "Eliminar",
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout{
				Padding = new Thickness(70, 10),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (TagLabel);
			Stack.Children.Add (Address1Label);
		}

		void setBindings ()
		{
			TagLabel.SetBinding (Label.TextProperty, "TagAddress");
			Address1Label.SetBinding (Label.TextProperty, "Address");
			DeleteItem.SetBinding (MenuItem.CommandParameterProperty, new Binding ("."));
		}

		void setListeners ()
		{
			DeleteItem.Clicked += (sender, e) => {
				var mi = ((MenuItem)sender);
				var address = mi.CommandParameter as AddressDBModel;
				App.LocalDataMan.DeleteAddress (address);
				//var status = App.DataMan.DeleteAddress(address);
			};
		}

	}
}

