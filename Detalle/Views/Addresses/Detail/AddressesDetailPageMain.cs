﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class AddressesDetailPageMain : ContentPage
	{
		public AddressDBModel Address;
		ReceiverDBModel Receiver;
		ToolbarItem FinishedItem;
		Entry TagAddressEntry;
		Editor Address1Editor;
		Editor ReferenceEditor;
		StackLayout StackEntries;
		bool isNewReceiver;

		public AddressesDetailPageMain (ReceiverDBModel Receiver, AddressDBModel Address, bool isNewReceiver)
		{
			this.Receiver = Receiver;
			this.Address = Address;
			this.isNewReceiver = isNewReceiver;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Dirección " + Address.TagAddress;
			ToolbarItems.Add (FinishedItem);
			Content = StackEntries;
		}

		void createElements ()
		{			
			FinishedItem = new ToolbarItem {
				Text = "Hecho",
			};
			TagAddressEntry = new Entry {
				Text = Address.TagAddress,
			};
			Address1Editor = new Editor {
				Text = Address.Address,
			};
			ReferenceEditor = new Editor {
				Text = Address.Reference,
			};
		}

		void createLayouts ()
		{			
			StackEntries = new StackLayout {
				Spacing = 10,
				Padding = new Thickness(10),
			};
		}

		void setLayouts ()
		{			
			StackEntries.Children.Add (TagAddressEntry);
			StackEntries.Children.Add (Address1Editor);
			StackEntries.Children.Add (ReferenceEditor);
		}

		void setListeners ()
		{			
			FinishedItem.Clicked += (sender, e) => {
				Address.TagAddress = TagAddressEntry.Text;
				Address.Address = Address1Editor.Text;
				Address.Reference = ReferenceEditor.Text;

				if (isNewReceiver) {
					if (App.LocalDataMan.UpdateAddress (Address)) {
						if (App.LocalDataMan.CreateReceiver (Receiver)) {
							if (App.LocalDataMan.CreateReceiverAddress(Receiver, Address)) {
								MessagingCenter.Send<AddressesDetailPageMain>(this, "ReceiverCreated");
							} else {
								DisplayAlert("Error", "Ocurrio un error al intentar guardar el destinatario", "OK");
							}
						} else {
							DisplayAlert("Error", "Ocurrio un error al intentar crear el destinatario", "OK");
						}
					} else {
						DisplayAlert("Error", "Ocurrio un error al intentar editar la dirección", "OK");				
					}
				} else {
					if (App.LocalDataMan.UpdateAddress(Address)) {
						MessagingCenter.Send<AddressesDetailPageMain>(this, "AddressUpdated");
					} else {
						DisplayAlert("Error", "Ocurrio un error al intentar editar la dirección", "OK");
					}
				}
			};
		}
	}
}

