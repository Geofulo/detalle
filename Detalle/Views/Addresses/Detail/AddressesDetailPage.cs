﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class AddressesDetailPage : ContentPage
	{
		public AddressDBModel Address;
		ReceiverDBModel Receiver;
		ToolbarItem EditItem;
		ToolbarItem OKItem;
		ToolbarItem FinishedItem;
		Label TagAddressLabel;
		Label Address1Label;
		Label ReferenceLabel;
		Entry TagAddressEntry;
		Editor Address1Editor;
		Editor ReferenceEditor;
		StackLayout StackLabels;
		StackLayout StackEntries;

		public AddressesDetailPage (AddressDBModel Address)
		{
			this.Address = Address;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Dirección " + Address.TagAddress;
			ToolbarItems.Add (EditItem);
			Content = StackLabels;
		}

		public AddressesDetailPage (ReceiverDBModel Receiver, AddressDBModel Address) 			// from menu
		{
			this.Receiver = Receiver;
			this.Address = Address;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Dirección " + Address.TagAddress;
			ToolbarItems.Add (OKItem);
			Content = StackEntries;
		}

		void createElements ()
		{
			EditItem = new ToolbarItem {
				Text = "Editar",
			};
			OKItem = new ToolbarItem {
				Text = "Hecho",
			};
			FinishedItem = new ToolbarItem {
				Text = "Hecho",
			};
			TagAddressLabel = new Label {
				Text = Address.TagAddress,
			};
			Address1Label = new Label {
				Text = Address.Address,
			};
			ReferenceLabel = new Label {
				Text = Address.Reference,
			};
			TagAddressEntry = new Entry {
				Text = Address.TagAddress,
			};
			Address1Editor = new Editor {
				Text = Address.Address,
			};
			ReferenceEditor = new Editor {
				Text = Address.Reference,
			};

		}

		void createLayouts ()
		{
			StackLabels = new StackLayout {
				Spacing = 10,
				Padding = new Thickness(10),
			};
			StackEntries = new StackLayout {
				Spacing = 10,
				Padding = new Thickness(10),
			};
		}

		void setLayouts ()
		{			
			StackLabels.Children.Add (TagAddressLabel);	
			StackLabels.Children.Add (Address1Label);
			StackLabels.Children.Add (ReferenceLabel);

			StackEntries.Children.Add (TagAddressEntry);
			StackEntries.Children.Add (Address1Editor);
			StackEntries.Children.Add (ReferenceEditor);
		}

		void setListeners ()
		{
			EditItem.Clicked += (sender, e) => {
				Content = StackEntries;
				this.ToolbarItems.Clear();
				this.ToolbarItems.Add(FinishedItem);
			};
			OKItem.Clicked += async (sender, e) => {
				Address.TagAddress = TagAddressEntry.Text;
				Address.Address = Address1Editor.Text;
				Address.Reference = ReferenceEditor.Text;

				var addressUpdated = App.LocalDataMan.CreateAddress(Address);
				//var addressUpdated = await App.DataMan.CreateAddress(Address);
				//var receiverCreated = App.DataMan.CreateReceiver(Receiver, Address);
				if (addressUpdated != null)
					MessagingCenter.Send<AddressesDetailPage>(this, "ReceiverCreated");
				else 
					await DisplayAlert("Error", "Ocurrio un error al intentar crear el destinatario", "OK");				
			};
			FinishedItem.Clicked += async (sender, e) => {
				Address.TagAddress = TagAddressEntry.Text;
				Address.Address = Address1Editor.Text;
				Address.Reference = ReferenceEditor.Text;

				var addressUpdated = App.LocalDataMan.CreateAddress(Address);
				//var addressUpdated = await App.DataMan.UpdateAddress(Address);
				//var receiverUpdated = App.DataMan.CreateReceiver(Receiver, addressUpdated);
				if (addressUpdated != null) {
					MessagingCenter.Send<AddressesDetailPage>(this, "AddressUpdated");
				} else {
					DisplayAlert("Error", "Ocurrio un error al intentar editar el destinatario", "OK");				
				}
			};
		}
	}
}

