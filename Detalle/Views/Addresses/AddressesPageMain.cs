﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Detalle
{
	public class AddressesPageMain : ContentPage
	{
		ReceiverDBModel Receiver;
		List<AddressDBModel> Addresses;
		bool HaveAddresses = false;

		Image ReceiverImage;
		Label ReceiverNameLabel;
		ToolbarItem AddItem;
		ListView AddressesListView;

		StackLayout StackReceiver;
		StackLayout Stack;

		public AddressesPageMain (ReceiverDBModel Receiver, bool HaveAddresses)
		{
			this.Receiver = Receiver;
			this.HaveAddresses = HaveAddresses;

			getAddresses ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Direcciones";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (AddItem);
			Content = Stack;
			Padding = new Thickness (20, 20, 0, 0);
		}

		void getAddresses ()
		{
			if (HaveAddresses) {
				Addresses = App.LocalDataMan.GetAddresses (Receiver);
			} else {
				Addresses = App.LocalDataMan.GetAddresses ();
			}
		}

		void createElements ()
		{
			AddItem = new ToolbarItem {
				Icon = "navbar_ico_add",
			};
			AddressesListView = new ListView {				
				RowHeight = 90,
				ItemsSource = Addresses,
				ItemTemplate = new DataTemplate(GetAddressTemplate),
				IsPullToRefreshEnabled = true,
			};
			ReceiverImage = new ImageEffectCustom {
				Source = ImageSource.FromResource("Chocolates.png"),
				WidthRequest = 50,
				HeightRequest = 50,
				Aspect = Aspect.AspectFill,
				IsCircle = true,
			};
			ReceiverNameLabel = new Label {
				Text = Receiver.Name,
				FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			StackReceiver = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
			};
			Stack = new StackLayout {
				Spacing = 10,
			};
		}

		void setLayouts ()
		{
			StackReceiver.Children.Add (ReceiverImage);
			StackReceiver.Children.Add (ReceiverNameLabel);

			Stack.Children.Add (StackReceiver);
			Stack.Children.Add (AddressesListView);
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				this.Navigation.PushAsync(new AddressesCreatePageMain(Receiver, !HaveAddresses));
			};
			AddressesListView.ItemSelected += (sender, e) => {				
				if (e.SelectedItem == null)
					return;
				var addressSelected = e.SelectedItem as AddressDBModel;
				Navigation.PushAsync(new AddressesDetailPageMain(Receiver, addressSelected, !HaveAddresses));
				AddressesListView.SelectedItem = null;
			};				
			AddressesListView.Refreshing += (sender, e) => {
				getAddresses();
				AddressesListView.ItemsSource = Addresses;
				AddressesListView.EndRefresh();
			};
		}			

		Cell GetAddressTemplate ()
		{
			var cell = new AddressesTemplate ();
			var deleteItem = new MenuItem () {
				Text = "Eliminar",
				IsDestructive = true
			};
			deleteItem.SetBinding (MenuItem.CommandParameterProperty, ".");
			deleteItem.Clicked += DeleteAddress;
			cell.ContextActions.Add(deleteItem);
			return cell;
		}

		void DeleteAddress (object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);
			var addressSelected = mi.CommandParameter as AddressDBModel;
			App.LocalDataMan.DeleteReceiverAddress (Receiver, addressSelected);
			if (App.LocalDataMan.DeleteAddress (addressSelected)) {								
				Addresses.Remove (addressSelected);
				AddressesListView.ItemsSource = Addresses;
			} else {
				DisplayAlert ("Error", "Ocurrió un error al intentar eliminar éste producto", "OK");  
			}
		}
	}
}

