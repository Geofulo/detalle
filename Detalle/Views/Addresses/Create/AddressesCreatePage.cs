﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class AddressesCreatePage : ContentPage
	{
		OrdersDBModel Order;
		ToolbarItem OKItem;
		Entry TagAddressEntry;
		Editor Address1Entry;
		Editor ReferenceEntry;
		public AddressDBModel AddressCreated;
		bool isNewReceiver;
		bool isNewOrder;

		StackLayout Stack;
		ScrollView Scroll;

		public AddressesCreatePage (OrdersDBModel Order)
		{
			this.Order = Order;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "¿A dónde?";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (OKItem);
			Content = Stack;
		}

		void createElements ()
		{
			OKItem = new ToolbarItem {
				Text = "Continuar",
			}; 
			TagAddressEntry = new Entry {
				Placeholder = "Etiqueta",
				HeightRequest = 60,
			};
			Address1Entry = new EditorCustom {				
				HeightRequest = 120,
				BorderSize = 0.5f,
				BorderColor = Color.FromHex("aaaaaa"),
				BorderRadius = 5,
				Placeholder = "Dirección",
			};
			ReferenceEntry = new EditorCustom {
				HeightRequest = 120,
				BorderSize = 0.5f,
				BorderColor = Color.FromHex("aaaaaa"),
				BorderRadius = 5,
				Placeholder = "Referencia",
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout (){
				Padding = new Thickness(10),
			};
			Scroll = new ScrollView ();
		}

		void setLayouts ()
		{
			Stack.Children.Add (TagAddressEntry);
			Stack.Children.Add (Address1Entry);
			Stack.Children.Add (ReferenceEntry);

			Scroll.Content = Stack;
		}

		void setListeners ()
		{
			OKItem.Clicked += (sender, e) => {				
				if(TagAddressEntry.Text != null && Address1Entry.Text != null){
					AddressCreated = new AddressDBModel () {
						TagAddress = TagAddressEntry.Text,
						Address = Address1Entry.Text,
						Reference = ReferenceEntry.Text,
					};
					Order.Address = Address1Entry.Text;
					Order.Reference = ReferenceEntry.Text;
					Navigation.PushAsync(new CreateSenderPage(Order, AddressCreated));
				} else {
					DisplayAlert("Error", "Se deben ingresar los campos obligatorios", "Entiendo");
				}
			};
			Address1Entry.Focused += (sender, e) => {				
				Address1Entry.Text = "";

			};
		}
			
	}
}

