﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class AddressesCreatePageMain : ContentPage
	{
		ReceiverDBModel Receiver;
		bool isNewReceiver;

		ToolbarItem FinishedItem;
		Entry TagAddressEntry;
		Editor Address1Entry;
		Editor ReferenceEntry;

		StackLayout Stack;
		ScrollView Scroll;

		public AddressesCreatePageMain (ReceiverDBModel Receiver, bool isNewReceiver)	
		{
			this.Receiver = Receiver;
			this.isNewReceiver = isNewReceiver;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Nueva";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (FinishedItem);
			Content = Scroll;
		}

		void createElements ()
		{
			FinishedItem = new ToolbarItem {
				Text = "Hecho",
			};
			TagAddressEntry = new Entry {
				Placeholder = "Etiqueta",
				HeightRequest = 60,
			};
			Address1Entry = new EditorCustom {				
				HeightRequest = 120,
				BorderSize = 0.5f,
				BorderColor = Color.FromHex("aaaaaa"),
				BorderRadius = 5,
				Placeholder = "Dirección",
			};
			ReferenceEntry = new EditorCustom {
				HeightRequest = 120,
				BorderSize = 0.5f,
				BorderColor = Color.FromHex("aaaaaa"),
				BorderRadius = 5,
				Placeholder = "Referencia",
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Padding = new Thickness(10),
				Spacing = 10,
			};
			Scroll = new ScrollView ();
		}

		void setLayouts ()
		{
			Stack.Children.Add (TagAddressEntry);
			Stack.Children.Add (Address1Entry);
			Stack.Children.Add (ReferenceEntry);

			Scroll.Content = Stack;
		}

		void setListeners ()
		{			
			FinishedItem.Clicked += (sender, e) => {
				if(TagAddressEntry.Text != null && Address1Entry.Text != null){
					var address = new AddressDBModel () {
						TagAddress = TagAddressEntry.Text,
						Address = Address1Entry.Text,
						Reference = ReferenceEntry.Text,
					};
					if (App.LocalDataMan.CreateAddress(address)) {
						if (isNewReceiver) {
							if (!App.LocalDataMan.CreateReceiver(Receiver)) {
								DisplayAlert("Error", "Ocurrió un error al intentar crear el destinatario", "OK");
							}
						} 							
						if (App.LocalDataMan.CreateReceiverAddress(Receiver, address))
							MessagingCenter.Send<AddressesCreatePageMain>(this, "AddressCreated");
						else 
							DisplayAlert("Error", "Ocurrió un error al intentar guardar el destinatario", "OK");
					} else {
						DisplayAlert("Error", "Ocurrió un error al intentar crear la dirección", "OK");
					}
				} else {
					DisplayAlert("Error", "Se deben ingresar los campos obligatorios", "Entiendo");
				}
			};
		}

	}
}

