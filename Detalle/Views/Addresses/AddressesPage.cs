﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class AddressesPage : ContentPage
	{
		OrdersDBModel Order;
		List<AddressDBModel> Addresses;
		List<ReceiverDBModel> Receivers;

		ToolbarItem AddItem;
		Label AllTitleLabel;
		ListView ReceiversListView;

		StackLayout Stack;

		public AddressesPage (OrdersDBModel Order)
		{
			this.Order = Order;

			getReceivers ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "¿A dónde?";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (AddItem);
			Padding = new Thickness (0, 20);
			BackgroundColor = Color.FromHex ("f8f8f9");
			Content = Stack;
		}

		void getReceivers ()
		{
			Receivers = App.LocalDataMan.GetReceivers ();
		}

		void createElements ()
		{
			AddItem = new ToolbarItem {
				Text = "Agregar",
			};
			AllTitleLabel = new Label {
				Text = "TODOS",
				TextColor = Color.Gray,
				FontSize = 12,
			};
			ReceiversListView = new ListView {
				//RowHeight = 60,
				IsPullToRefreshEnabled = true,
				HasUnevenRows = true,
				ItemsSource = Receivers,
				ItemTemplate = new DataTemplate(typeof(ReceiversAddressTemplate)),
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 0,
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(25, 0, 0, 20),
				Children = { AllTitleLabel },
			});
			Stack.Children.Add (new BoxView {
				HeightRequest = 0.5,
				WidthRequest = App.ScreenWidth,
				BackgroundColor = Color.Gray,
			});
			Stack.Children.Add (ReceiversListView);
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				var addressCreatePage = new AddressesCreatePage(Order);
				addressCreatePage.Title = "Nuevo";
				Navigation.PushAsync(addressCreatePage);
			};
			ReceiversListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var receiverSelected = e.SelectedItem as ReceiverDBModel;
				Navigation.PushAsync(new AddressesShoppingPage(Order, receiverSelected));
				//Navigation.PushAsync(new CreateSenderPage(Order, addressSelected));
				ReceiversListView.SelectedItem = null;
			};				
			ReceiversListView.Refreshing += (sender, e) => {
				getReceivers();
				ReceiversListView.ItemsSource = Addresses;
				ReceiversListView.EndRefresh();
			};
		}
	}
}

