﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class LoginPage : ContentPage 
	{
		StackLayout Stack;
		Button FacebookLoginButton;
		Button RegisterButton;

		public LoginPage ()
		{			
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Ingresar";
			Content = Stack;
		}

		void createElements ()
		{
			FacebookLoginButton = new Button {
				Text = "Facebook Login",
			};
			RegisterButton = new Button {
				Text = "Registrarse",
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Padding = new Thickness (30),
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (FacebookLoginButton);
			Stack.Children.Add (RegisterButton);
		}

		void setListeners ()
		{
			FacebookLoginButton.Clicked += async (sender, e) => {				
				UserManager.LoginWithFacebook();
				/*
				var user = await DependencyService.Get<ILogin>().LoginFacebook();
				if(user != null){
					MessagingCenter.Send<LoginPage>(this, "Connected");
				}*/
			};
			RegisterButton.Clicked += (sender, e) => {
				this.Navigation.PushModalAsync (new NavigationPage(new RegisterPage()));
			};
		}
	}
}

