﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class MainMasterPage : ContentPage
	{
		readonly Color _colorLabel = Color.White;
		readonly Color _colorSelected = Color.FromHex("595891");
		readonly int _heightRow = 55;
		public StackLayout Stack;
		public Label ProfileLabel;
		public Label IndexLabel;
		public Label ProductsLabel;
		public Label WishesListLabel;
		public Label ShoppingCartLabel;
		public Label OrdersLabel;
		public Label CalendarLabel;
		public Label ReceiversLabel;
		public Label LabelSelected;
		public List<Label> CategoriesLabels = new List<Label>() ;

		public MainMasterPage ()
		{			
			createElements ();
			createLayouts ();
			setLayouts ();
			setMessagingCenter ();

			Title = "Menú"; 
			Icon = "navbar_ico_menu.png";
			Padding = new Thickness (0,30,0,20);
			BackgroundColor = Color.FromHex ("6563a4");
			Content = Stack;
		}

		void createElements ()
		{
			ProfileLabel = new Label {
				Text = "\tMis Datos",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
			};
			IndexLabel = new Label {
				Text = "\tDestacado",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
				BackgroundColor = _colorSelected,
			};
			ProductsLabel = new Label {
				Text = "\tProductos",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
			};
			WishesListLabel = new Label {
				Text = "\tLista de Deseos",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
			};
			ShoppingCartLabel = new Label {
				Text = "\tCarrito",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
			};
			OrdersLabel = new Label {
				Text = "\tEnvíos",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
			};
			CalendarLabel = new Label {
				Text = "\tOcasiones",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
			};
			ReceiversLabel = new Label {
				Text = "\tDestinatarios",
				HeightRequest = _heightRow,
				TextColor = _colorLabel,
				YAlign = TextAlignment.Center,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 0,
			};
		}

		void setLayouts ()
		{
			if (!App.IsRealUser) {
				Stack.Children.Add (ProfileLabel);
				Stack.Children.Add (getSeparator());
			}
			Stack.Children.Add (IndexLabel);
			Stack.Children.Add (getSeparator());

			Stack.Children.Add (ProductsLabel);
			Stack.Children.Add (getSeparator());

			Stack.Children.Add (WishesListLabel);
			Stack.Children.Add (getSeparator());

			Stack.Children.Add (ShoppingCartLabel);
			Stack.Children.Add (getSeparator());

			Stack.Children.Add (OrdersLabel);
			Stack.Children.Add (getSeparator());

			Stack.Children.Add (CalendarLabel);
			Stack.Children.Add (getSeparator());

			Stack.Children.Add (ReceiversLabel);

			LabelSelected = IndexLabel;
		}

		void setMessagingCenter ()
		{
			
		}

		BoxView getSeparator()
		{
			return new BoxView {
				HeightRequest = 0.5f,
				WidthRequest = 220,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
			};
		}

	}
}

