﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class MainMasterDetailPage : MasterDetailPage
	{
		readonly Color _colorSelected = Color.FromHex("595891");
		MainMasterPage MasterPage;
		ProductsPage BestProductsPage;

		public MainMasterDetailPage ()
		{
			createPages ();
			setListeners ();
			setMessagingCenter ();

			Title = "EnviaRegalos";
			Master = MasterPage;
			var navPage = new NavigationPageCustom (BestProductsPage){
				BarBackgroundColor = Color.White,
				BarTextColor = Color.Black,
				TintColor = Color.FromHex("ff3366"),
			};
			Detail = navPage;	
		}

		void createPages ()
		{
			MasterPage = new MainMasterPage ();
			BestProductsPage = new ProductsPage ();
		}

		void setListeners ()
		{	
			var TapGesture = new TapGestureRecognizer();
			TapGesture.Tapped += openNavigationPage;

			MasterPage.ProfileLabel.GestureRecognizers.Add (TapGesture);
			MasterPage.IndexLabel.GestureRecognizers.Add (TapGesture);
			MasterPage.ProductsLabel.GestureRecognizers.Add (TapGesture);
			foreach(var categoryLabel in MasterPage.CategoriesLabels)
			{
				categoryLabel.GestureRecognizers.Add (TapGesture);
			}
			MasterPage.WishesListLabel.GestureRecognizers.Add (TapGesture);
			MasterPage.ShoppingCartLabel.GestureRecognizers.Add (TapGesture);
			MasterPage.OrdersLabel.GestureRecognizers.Add (TapGesture);
			MasterPage.CalendarLabel.GestureRecognizers.Add (TapGesture);
			MasterPage.ReceiversLabel.GestureRecognizers.Add (TapGesture);

		}

		void setMessagingCenter ()
		{			

			MessagingCenter.Subscribe<RegisterPage> (this, "SignUp", (sender) => { 
				//MasterPage.Stack.Children.Remove(MasterPage.ProfileLabel);
				//MasterPage.Stack.Children.Add (MasterPage.LoginLabel);
				Detail = new NavigationPage(BestProductsPage);
				IsPresented = false;
			});

			MessagingCenter.Subscribe<OrdersNotesDrawPage> (this, "DisableAnimationMenu", (sender) => { 
				this.IsGestureEnabled = false;
			});

			MessagingCenter.Subscribe<ProductsDetailNoteView> (this, "DisableAnimationMenu", (sender) => { 
				this.IsGestureEnabled = false;
			});

			MessagingCenter.Subscribe<OrdersNotesDrawPage> (this, "EnableAnimationMenu", (sender) => { 
				this.IsGestureEnabled = true;
			});

			MessagingCenter.Subscribe<ProductsDetailNoteView> (this, "EnableAnimationMenu", (sender) => { 
				this.IsGestureEnabled = true;
			});

			MessagingCenter.Subscribe<OrdersPayPage> (this, "OrderPaid", (sender) => { 
				setNavigationPage(new MyOrdersPage());
				setBackgroundLabel (MasterPage.OrdersLabel);
				IsPresented = false;
				DisplayAlert("Gracias por su compra", "El pedido ha pasado a pendiente", "OK");
			});
		}

		void openNavigationPage (object sender, EventArgs e)
		{
			if(sender == MasterPage.ProfileLabel)
			{
				setNavigationPage(new ProfilePage());
			} 
			if(sender == MasterPage.IndexLabel)
			{
				setNavigationPage(new ProductsPage());
			} 
			if(sender == MasterPage.ProductsLabel)
			{
				var categories = App.LocalDataMan.GetCategories ();
				if (categories.Count > 1) {
					setNavigationPage(new CategoriesPage(categories));
				} else {
					setNavigationPage (new ProductsPage(categories[0]));
				}
			} 
			if(sender == MasterPage.WishesListLabel)
			{
				setNavigationPage(new WishesListPage());
			} 
			if(sender == MasterPage.ShoppingCartLabel)
			{
				setNavigationPage(new ShoppingCartPage());
			} 
			if(sender == MasterPage.OrdersLabel)
			{
				setNavigationPage(new MyOrdersPage());
			} 
			if(sender == MasterPage.CalendarLabel)
			{
				setNavigationPage(new CalendarPage());
			} 
			if(sender == MasterPage.ReceiversLabel)
			{
				setNavigationPage(new ReceiversPageMain());
			} 
			var label = sender as Label;
			setBackgroundLabel (label);
			IsPresented = false;
		}

		void setBackgroundLabel (Label selected)
		{
			MasterPage.LabelSelected.BackgroundColor = Color.Transparent;
			selected.BackgroundColor = _colorSelected;
			MasterPage.LabelSelected = selected;
		}

		void setNavigationPage (Page page)
		{
			Detail = new NavigationPageCustom (page) {
				BarBackgroundColor = Color.White,
				BarTextColor = Color.Black,
				TintColor = Color.FromHex("ff3366"),
			};
		}
	}
}

