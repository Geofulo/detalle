﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Detalle
{
	public class WishesListTemplate : StackLayout
	{		
		public CommisionsDBModel Commision;
		public ProductsDBModel Product;
		ReceiverDBModel Receiver;
		StackLayout StackProperties;
		Image ProductImage;
		Label ProductNameLabel;
		Label ProductPriceLabel;
		Label ReceiverNameLabel;
		//public Button CartButton;
		public Button WishButton;
		TapGestureRecognizer _tapGesture = new TapGestureRecognizer();

		public WishesListTemplate (CommisionsDBModel Commision)
		{
			this.Commision = Commision;
			this.Product = App.LocalDataMan.getProduct (Commision.IdProduct);
			this.Receiver = App.LocalDataMan.getReceiver (Commision.IdReceiver);
			
			createElements ();
			createLayouts ();
			setLayouts ();

			GestureRecognizers.Add (_tapGesture);
			BackgroundColor = Color.White;
			HeightRequest = 200;
			WidthRequest = App.ScreenWidth - 20;
			Children.Add (ProductImage);
			Children.Add (ProductNameLabel);
			//Children.Add (ProductPriceLabel);
			Children.Add (ReceiverNameLabel);
			Children.Add (StackProperties);
			//Children.Add (CartButton);
			//Children.Add (DeleteButton);
		}

		void createElements ()
		{			
			if (Product != null) {
				var image = App.LocalDataMan.getProductImage (Product.IdProduct);
				if(image != null){
					try {
						ProductImage = new Image {
							Source = ImageSource.FromUri(new Uri(image)),
							HeightRequest = 120,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							Aspect = Aspect.AspectFill,
						};	
					} catch(Exception e) {
						System.Diagnostics.Debug.WriteLine (e.Message);
						ProductImage = new Image {
							Source = "chocolates.png",
							HeightRequest = 120,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							Aspect = Aspect.AspectFill,
						};
					}
				} else {
					ProductImage = new Image {
						Source = "chocolates.png",
						HeightRequest = 120,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						Aspect = Aspect.AspectFill,
					};
				}
				ProductNameLabel = new Label {
					Text = Product.Name,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
				};
				ProductPriceLabel = new Label {
					Text = "$" + Product.Price.ToString(),
					TextColor = Color.FromHex("6563a4"),
					XAlign = TextAlignment.End,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.EndAndExpand,
				};
			}
			if (Receiver != null) {
				ReceiverNameLabel = new Label {
					Text = "Para: " + Receiver.Name,
					FontAttributes = FontAttributes.Italic,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
				};
			} else {
				ReceiverNameLabel = new Label {
					Text = "Sin destinatario",
					VerticalOptions = LayoutOptions.CenterAndExpand,
					FontAttributes = FontAttributes.Bold,
				};
			}
			WishButton = new Button {
				Image = "ico_like_hover.png",
				TextColor = Color.FromHex("ff3366"),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
			};
		}

		void createLayouts ()
		{
			StackProperties = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10, 0),
			};
		}

		void setLayouts ()
		{
			StackProperties.Children.Add (WishButton);
			StackProperties.Children.Add (ProductPriceLabel);
		}
		/*
		void setBindings ()
		{			
			ProductNameLabel.SetBinding (Label.TextProperty, "Product.Name");
			ProductPriceLabel.SetBinding (Label.TextProperty, "Product.Price", stringFormat: "${0}");
			ReceiverNameLabel.SetBinding (Label.TextProperty, "Receiver.Name", stringFormat: "Para: {0}");
		}*/

		void setListeners ()
		{
			_tapGesture.Tapped += (sender, e) => {
				Navigation.PushAsync(new ProductsDetailPage(Product, Commision));
			};
		}
	}
}

