﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class WishesListPage : ContentPage
	{
		readonly int _numColumns = 2;
		List<CommisionsDBModel> Commisions;
		Label NoCommisionLabel;
		Grid Grid;
		ScrollView Scroll;

		public WishesListPage ()
		{
			getCommisions ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setMessagingCenter ();

			Title = "Lista de deseos";
			if (Commisions.Count > 0)
				Content = Scroll;
			else
				Content = NoCommisionLabel;
		}

		void getCommisions ()
		{
			Commisions = App.LocalDataMan.GetWishesList ();
		}

		void createElements ()
		{			
			NoCommisionLabel = new Label {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Text = "No tienes ningún producto en la lista de deseos.",
			};
		}

		void createLayouts ()
		{
			Grid = new Grid {
				RowSpacing = 10,
				ColumnSpacing = 10,
				ColumnDefinitions = new ColumnDefinitionCollection {
					new ColumnDefinition {
						Width = (App.ScreenWidth / 2) - 20,
					},
				},

				RowDefinitions = new RowDefinitionCollection {
					new RowDefinition {
						Height = 240,
					},
				},

			};
			Scroll = new ScrollView {
				Padding = new Thickness(10),
			};
		}

		void setLayouts ()
		{
			Grid.Children.Clear ();
			int aux = 0;
			double rows = Math.Ceiling ((double)Commisions.Count / (double)_numColumns);
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < _numColumns; j++) {
					if (aux < Commisions.Count) {
						var item = new WishesListTemplate (Commisions [aux]);
						item.WishButton.Clicked += (sender, e) => WishButtonClickedAction(item);
						var frame = new FrameCustom {
							HasShadow = true,
							Content = item,
							OutlineColor = Color.FromHex("f8f8f9"),
							ShadowRadius = 0.3f,
							ShadowOffsetHeight = 2.5f,
							ShadowOffsetWidth = 2.5f,
							ShadowOpacity = 0.1f,
							BorderRadius = 0,
							Padding = new Thickness(0),
						};
						//item.CartButton.Clicked += (sender, e) => CommisionToCartAction(item.Commision);
						//item.DeleteButton.Clicked += DeleteCommisionAction;
						Grid.Children.Add (frame, j, i);
						aux++;
					}
				}
			}
			Scroll.Content = Grid;
		}	
			
		void setMessagingCenter ()
		{
			MessagingCenter.Subscribe<ReceiversPage> (this, "ProductAddedToCart", (sender) => { 								
				this.Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El producto fue agregado al carrito exitosamente", "OK");
				getCommisions();
				Grid.Children.Clear ();
				setLayouts ();
			});

			MessagingCenter.Subscribe<ReceiversCreatePage> (this, "ProductAddedToCart", (sender) => { 								
				this.Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El producto fue agregado al carrito exitosamente", "OK");
				getCommisions();
				Grid.Children.Clear ();
				setLayouts ();
			});
		}

		void CommisionToCartAction (CommisionsDBModel commisionSelected)
		{
			//var mi = ((MenuItem)sender);
			//var commisionSelected = mi.CommandParameter as CommisionsDBModel;
			//var wishTemplate = sender as WishesListTemplate;
			//var commisionSelected = wishTemplate.Commision;
			//var commisionSelected = e as CommisionsDBModel;
			var receiver = App.LocalDataMan.getReceiver (commisionSelected.IdReceiver);
			if (receiver != null) {
				commisionSelected.InCart = true;
				if (App.LocalDataMan.UpdateCommision (commisionSelected)) {
					DisplayAlert("Correcto", "El producto fue agregado al carrito exitosamente", "OK");
					getCommisions();
					Grid.Children.Clear ();
					setLayouts ();
				} else {
					DisplayAlert("Error", "Ocurrió un error al intentar agregar el producto al carrito", "OK");
				}
			} else {
				var receivers = App.LocalDataMan.GetReceivers();
				if(receivers.Count == 0 || receivers == null) {
					var receiverCreatePage = new ReceiversCreatePage(commisionSelected, true, true);
					receiverCreatePage.Title = "¿A quién?";
					Navigation.PushAsync(receiverCreatePage);
				} else {
					Navigation.PushAsync(new ReceiversPage(commisionSelected, true, true));
				}					
			}
		}
		/*
		void DeleteCommisionAction (object sender, EventArgs e)
		{
			//var mi = ((MenuItem)sender);
			//var commisionSelected = mi.CommandParameter as CommisionsDBModel;
			var commisionSelected = e as CommisionsDBModel;
			if (App.LocalDataMan.DeleteCommision (commisionSelected)) {
				getCommisions();
				if (Commisions.Count == 0) {
					Content = NoCommisionLabel;
				} else {
					Grid.Children.Clear ();
					setLayouts ();
				}
			} else {
				DisplayAlert ("Error", "Ocurrió un error al intentar eliminar éste envío", "OK");  
			}
		}*/

		async void WishButtonClickedAction (WishesListTemplate item)
		{
			var canDelete = await DisplayAlert ("Eliminar", "¿Esta seguro de querer eliminar el producto de la lista de deseos?", "Eliminar", "Cancelar");
			if (canDelete) {	
				var commision = App.LocalDataMan.GetCommisionFromWishList (item.Product);
				App.LocalDataMan.DeleteCommision (commision);
				item.WishButton.Image = "ico_like.png";
				getCommisions ();
				setLayouts ();
				if (Commisions.Count > 0)
					Content = Scroll;
				else
					Content = NoCommisionLabel;
			}
		}
	}
}

