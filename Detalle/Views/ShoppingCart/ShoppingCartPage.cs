﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Detalle
{
	public class ShoppingCartPage : ContentPage
	{
		List<CommisionsDBModel> Commisions;
		ToolbarItem PayItem;
		ToolbarItem AddItem;

		ListView CartListView;
		Label NoCommisionsLabel;

		Button CleanButton;
		Label SubtotalLabel;
		Label SubtotalPriceLabel;

		StackLayout Stack;
		StackLayout StackPrice;
		float SubtotalPrice;

		public ShoppingCartPage ()
		{
			getCommisions ();
			setElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();
			setMessagingCenter ();

			Title = "Carrito";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (AddItem);
			if (Commisions.Count > 0)
				ToolbarItems.Add (PayItem);
			Content = Stack;
		}

		void getCommisions ()
		{			
			Commisions = App.LocalDataMan.GetShoppingCart ();
			SubtotalPrice = 0;
			foreach (var commision in Commisions) {
				SubtotalPrice += commision.Product.Price;
			}
		}

		void setElements ()
		{
			PayItem = new ToolbarItem {
				Text = "Enviar",
			};
			AddItem = new ToolbarItem {
				//Text = "+",
				Icon = "navbar_ico_add.png",
			};
			CartListView = new ListView {
				RowHeight = 160,
				IsPullToRefreshEnabled = true,
				SeparatorVisibility = SeparatorVisibility.None,
				BackgroundColor = Color.FromHex("f8f8f9"),
				ItemsSource = Commisions,
				ItemTemplate = new DataTemplate (GetShoppingCartTemplate),
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			NoCommisionsLabel = new Label {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Text = "No tienes ningún producto en el carrito.",
			};
			SubtotalLabel = new Label {
				Text = "Subtotal:",
				TextColor = Color.White,
				YAlign = TextAlignment.End,
				HeightRequest = 30,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
			};
			SubtotalPriceLabel = new Label {
				Text = "$" + SubtotalPrice.ToString(),
				TextColor = Color.White,
				FontSize = 23,
				FontAttributes = FontAttributes.Bold,
				YAlign = TextAlignment.End,
				HeightRequest = 30,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
			};
			CleanButton = new Button {
				Image = "ico_empty.png",
				Text = "Vaciar carrito",
				TextColor = Color.White,
			};
		}

		void createLayouts ()
		{			
			Stack = new StackLayout ();
			StackPrice = new StackLayout { 				
				BackgroundColor = Color.FromHex("6563a4"),
				Padding = new Thickness(10),
				Spacing = 10,
				Orientation = StackOrientation.Horizontal,
				HeightRequest = 90,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void setLayouts ()
		{
			StackPrice.Children.Add (CleanButton);
			StackPrice.Children.Add (SubtotalLabel);
			StackPrice.Children.Add (SubtotalPriceLabel);

			if (Commisions.Count > 0) {
				Stack.Children.Add (CartListView);
			} else {
				Stack.Children.Add (NoCommisionsLabel);
			}
			Stack.Children.Add (StackPrice);
		}

		void setListeners ()
		{
			PayItem.Clicked += (sender, e) => {
				var order = new OrdersDBModel() {
					Status = 1,
				};
				var addresses = App.LocalDataMan.GetAddresses();
				if (addresses.Count == 0 || addresses == null) {
					Navigation.PushAsync(new AddressesCreatePage(order));
				} else {					
					Navigation.PushAsync(new AddressesPage(order));
				}
			};
			AddItem.Clicked += (sender, e) => {
				
			};
			CleanButton.Clicked += async (sender, e) => {
				var canDelete = await DisplayAlert ("Eliminar", "¿Esta seguro de querer vacíar el carrito?", "Eliminar", "Cancelar");
				if (canDelete) {
					foreach(var commision in Commisions)
					{
						updateCommision(commision);
					}
				}
			};
			CartListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var commision = e.SelectedItem as CommisionsDBModel;
				//var receiver = App.LocalDataMan.getReceiver (commision.IdReceiver);
				//this.Navigation.PushAsync(new ProductsDetailPage(();
				CartListView.SelectedItem = null;
			};
			CartListView.Refreshing += (sender, e) => {
				getCommisions();
				if (Commisions.Count == 0) {
					Content = NoCommisionsLabel;
				} else {
					CartListView.ItemsSource = Commisions;
				}
				CartListView.EndRefresh();
			};		
		}

		void setMessagingCenter ()
		{
			MessagingCenter.Subscribe<MyOrdersDetailPage> (this, "OrderPaid", (sender) => {
				Navigation.PopToRootAsync();
				Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El pedido fue pagado exitosamente", "OK");
				getCommisions();
				if (Commisions.Count == 0) {
					Content = NoCommisionsLabel;
				} else {
					CartListView.ItemsSource = Commisions;
				}
			});
		}

		Cell GetShoppingCartTemplate ()
		{
			var cell = new ShoppingCartTemplate ();
			var deleteItem = new MenuItem () {
				Text = "Eliminar",
				IsDestructive = true
			};
			deleteItem.SetBinding (MenuItem.CommandParameterProperty, ".");
			deleteItem.Clicked += DeleteCommision;
			cell.ContextActions.Add(deleteItem);
			return cell;
		}

		async void DeleteCommision(object sender, EventArgs e)  
		{  			
			var mi = ((MenuItem)sender);
			var commisionSelected = mi.CommandParameter as CommisionsDBModel;
			var canDelete = await DisplayAlert ("Eliminar", "¿Esta seguro de querer eliminar el producto del carrito?", "Eliminar", "Cancelar");
			if (canDelete) {	
				updateCommision (commisionSelected);
			}
		} 
			
		void updateCommision(CommisionsDBModel commision)
		{
			commision.InCart = false;
			if (App.LocalDataMan.UpdateCommision (commision)) {
				getCommisions();
				if (Commisions.Count == 0) {
					Content = NoCommisionsLabel;
				} else {
					CartListView.ItemsSource = Commisions;
				}
			} else {
				DisplayAlert ("Error", "Ocurrió un error al intentar eliminar el producto del carrito", "OK");  
			}
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			getCommisions();
			CartListView.ItemsSource = Commisions;
		}
	}
}

