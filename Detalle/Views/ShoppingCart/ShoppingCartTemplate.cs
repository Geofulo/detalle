﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ShoppingCartTemplate : ViewCell
	{
		public OrdersModel Order;
		double Count = 1;

		Image ProductImage;
		Label ReceiverNameLabel;
		Label ProductNameLabel;
		Label ProductPriceLabel;
		Stepper CountStepper;
		Label CountLabel;

		Frame Frame;
		StackLayout Stack;
		StackLayout StackProduct;
		StackLayout StackInfo;
		StackLayout StackOptions;

		public ShoppingCartTemplate ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();
			setBindings ();

			View = Stack;
		}

		void createElements ()
		{
			ProductImage = new Image {
				Source = "Chocolates.png",
				WidthRequest = App.ScreenWidth / 2,
				Aspect = Aspect.AspectFill,
			};

			ProductNameLabel = new Label {				
				XAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			ReceiverNameLabel = new Label {
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			ProductPriceLabel = new Label {
				TextColor = Color.FromHex("6563a4"),
				FontSize = 21,
				XAlign = TextAlignment.End,
				VerticalOptions = LayoutOptions.EndAndExpand,
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};
			CountStepper = new StepperCustom {
				Increment = 1,
				Minimum = 1,
				Value = Count,
				Scale = 0.8,
				BorderColor = Color.FromHex("ff3366"),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			CountLabel = new Label {
				Text = "Cantidad: " + Count.ToString(),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{			
			StackInfo = new StackLayout {
				Spacing = 5,
				Padding = new Thickness(10, 5),
				WidthRequest = App.ScreenWidth / 2,
				BackgroundColor = Color.White,
			};
			StackProduct = new StackLayout {
				Orientation = StackOrientation.Horizontal,
			};
			StackOptions = new StackLayout {
				Orientation = StackOrientation.Horizontal,
			};
			Stack = new StackLayout {
				Padding = new Thickness(0, 15),
				BackgroundColor = Color.FromHex("f8f8f9"),
			};
		}

		void setLayouts ()
		{			
			StackInfo.Children.Add (ProductNameLabel);
			StackInfo.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = { 
					new Label {
						Text = "Para: ",
						TextColor = Color.FromHex("6563a4"),
					}, 
					ReceiverNameLabel
				},
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			StackInfo.Children.Add (ProductPriceLabel);

			StackProduct.Children.Add (ProductImage);
			StackProduct.Children.Add (StackInfo);

			StackOptions.Children.Add (CountLabel);
			StackOptions.Children.Add (CountStepper);

			Stack.Children.Add (StackProduct);
			Stack.Children.Add (StackOptions);

		}

		void setListeners()
		{
			CountStepper.ValueChanged += (sender, e) => {
				Count = CountStepper.Value;
				CountLabel.Text = "Cantidad: " + Count.ToString();
			};
		}

		void setBindings ()
		{
			ProductNameLabel.SetBinding (Label.TextProperty, "Product.Name");
			ProductPriceLabel.SetBinding (Label.TextProperty, "Product.Price", stringFormat: "${0}");
			ReceiverNameLabel.SetBinding (Label.TextProperty, "ReceiverName");
		}


	}
}

