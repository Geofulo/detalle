﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Detalle
{
	public class MapPage : ContentPage {

		Map map;
		Position position;
		ToolbarItem CloseItem;

		public MapPage ()
		{
			createElements ();
			setListeners ();

			Title = "Mapa";
			ToolbarItems.Add (CloseItem);
			Content = map;

			DependencyService.Get<ILocation> ().ManageLocation (map);
		}

		void createElements ()
		{
			map = new Map () {										
				HasScrollEnabled = true,
				HasZoomEnabled = true,
				IsShowingUser = true,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				MapType = MapType.Street,
			};

			CloseItem = new ToolbarItem {
				Text = "Cerrar",
			};
		}

		void setListeners ()
		{
			CloseItem.Clicked += (sender, e) => {
				this.Navigation.PopModalAsync();
			};
		}
	}
}

