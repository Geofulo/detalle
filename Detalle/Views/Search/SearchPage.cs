﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class SearchPage : ContentPage
	{		
		readonly int _numColumns = 2;
		string SearchText;
		StackLayout Stack;
		Grid Grid;
		List<ProductsDBModel> Data;

		public SearchPage (string SearchText)
		{
			this.SearchText = SearchText;

			//getData ();
			createLayouts ();
			setLayouts ();

			Title = "Buscar";
			Content = Stack;
		}

		void getData ()
		{
			Data = new List<ProductsDBModel> ();
		}

		void createLayouts ()
		{
			Stack = new StackLayout ();
			Grid = new Grid ();
		}

		void setLayouts ()
		{
			int aux = 0;
			double rows = Math.Ceiling((double)Data.Count / (double)_numColumns);
			for (int i = 0; i < _numColumns; i++) {
				for (int j = 0; j < rows; j++) {
					if (aux < Data.Count) {
						var item = new ProductsTemplate (Data [aux++]);
						Grid.Children.Add(item, j, i);
					}
				}
			}
			Stack.Children.Add (Grid);
		}
	}
}

