﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ProfilePage : ContentPage
	{
		ToolbarItem PaymentItem;
		ToolbarItem LogoutItem;
		Label UsernameLabel;

		public ProfilePage ()
		{
			createElements ();
			setListeners ();

			Title = "Mis Datos";
			ToolbarItems.Add (PaymentItem);
			ToolbarItems.Add (LogoutItem);
			Content = UsernameLabel;
		}

		void createElements ()
		{			
			PaymentItem = new ToolbarItem {
				Text = "Pago",
				Order = ToolbarItemOrder.Secondary,
			};
			LogoutItem = new ToolbarItem {
				Text = "Cerrar Sesión",
				Order = ToolbarItemOrder.Secondary,
			};
			UsernameLabel = new Label {
				Text = App.ActiveUser.UserName,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setListeners ()
		{
			PaymentItem.Clicked += (sender, e) => {
				this.Navigation.PushAsync(new PaymentPages());
			};
			LogoutItem.Clicked += (sender, e) => {
				//DependencyService.Get<ILogin>().LogoutFacebook();
				MessagingCenter.Send<ProfilePage>(this, "Disconnected");
			};
		}
	}
}

