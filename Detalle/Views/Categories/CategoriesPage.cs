﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class CategoriesPage : ContentPage
	{
		List<CategoriesDBModel> Categories;
		StackLayout Stack;
		ScrollView Scroll;

		public CategoriesPage (List<CategoriesDBModel> Categories)
		{
			this.Categories = Categories;

			createLayouts ();
			setLayouts ();

			Title = "Productos";
			Content = Scroll;
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 0,
			};
			Scroll = new ScrollView ();
		}

		void setLayouts ()
		{
			foreach(var category in Categories)
			{
				var categoryContent = new CategoriesTemplate (category);
				Stack.Children.Add (categoryContent);
			}
			Scroll.Content = Stack;
		}

	}
}

