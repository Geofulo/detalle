﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class CategoriesTemplate : ContentView
	{
		CategoriesDBModel Category;
		TapGestureRecognizer TapGesture = new TapGestureRecognizer();
		RelativeLayout Relative;
		Button NameButton;
		Label ProductsCountLabel;
		ImageEffectCustom CategoryImage;

		public CategoriesTemplate (CategoriesDBModel Category)
		{
			this.Category = Category;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			GestureRecognizers.Add (TapGesture);
			Content = Relative;
			HorizontalOptions = LayoutOptions.FillAndExpand;
			BackgroundColor = Color.Gray;
			HeightRequest = 200;
		}

		void createElements ()
		{
			NameButton = new Button {
				Text = Category.Name,
				FontSize = 24,
				TextColor = Color.White,
				BorderRadius = 20,
				BorderColor = Color.White,
				BorderWidth = 2,
				WidthRequest = 160,
				HeightRequest = 40,
			};
			var products = App.LocalDataMan.GetProducts (Category);
			ProductsCountLabel = new Label {
				Text = products.Count.ToString(),
				TextColor = Color.White,
				WidthRequest = 40,
				XAlign = TextAlignment.Center,
			};
			CategoryImage = new ImageEffectCustom() {
				//Source = ImageSource.FromResource(Category.Name + ".png"),
				Source = ImageSource.FromFile(Category.Name + ".png"),
				Aspect = Aspect.Fill,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions	= LayoutOptions.FillAndExpand,
				IsDarkBlurMedium = true,
			};
		}

		void createLayouts ()
		{
			Relative = new RelativeLayout {				
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setLayouts ()
		{
			Relative.Children.Add (CategoryImage, 
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (NameButton, 
				Constraint.RelativeToParent((parent) => { return (parent.Width / 2) - (NameButton.WidthRequest / 2); }),
				Constraint.RelativeToParent((parent) => { return (parent.Height / 2) - (NameButton.HeightRequest / 2); })
			);
			Relative.Children.Add (ProductsCountLabel,
				Constraint.RelativeToParent((parent) => { return (parent.Width / 2) - (ProductsCountLabel.WidthRequest / 2); }),
				Constraint.RelativeToParent((parent) => { return (parent.Height / 2) + NameButton.Height + 3; })
			);
		}
			
		void setListeners ()
		{
			TapGesture.Tapped += (sender, e) => {
				Navigation.PushAsync(new ProductsPage(Category));
			};
		}
	}
}

