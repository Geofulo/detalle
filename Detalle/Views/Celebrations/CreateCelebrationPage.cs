﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class CreateCelebrationPage : ContentPage
	{
		ToolbarItem CancelItem;
		ToolbarItem OKItem;

		public CreateCelebrationPage ()
		{
			createElements ();
			setListeners ();

			Title = "Nuevo Festejo";
			ToolbarItems.Add (CancelItem);
			ToolbarItems.Add (OKItem);
		}

		void createElements ()
		{
			CancelItem = new ToolbarItem { 
				Text = "Cancelar",
			};
			OKItem = new ToolbarItem { 
				Text = "Hecho",
			};
		}

		void setListeners ()
		{
			CancelItem.Clicked += (sender, e) => {
				this.Navigation.PopModalAsync();
			};
			OKItem.Clicked += async (sender, e) => {
				await DisplayAlert("Festejo agregado", "Su nuevo festejo ha sido guardado", "OK");
				this.Navigation.PopModalAsync();
			};
		}
	}
}

