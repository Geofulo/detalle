﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class CelebrationsPage : ContentPage
	{
		ToolbarItem AddItem;

		public CelebrationsPage ()
		{
			createElements ();
			setListeners ();

			Title = "Celebraciones";
			ToolbarItems.Add (AddItem);
		}

		void createElements ()
		{
			AddItem = new ToolbarItem {
				Text = "Nuevo"
			};
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				this.Navigation.PushModalAsync(new NavigationPage(new CreateCelebrationPage()));
			};
		}
	}
}

