﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ReceiversCreatePageMain : ContentPage
	{
		ToolbarItem OKItem;
		StackLayout Stack;
		Entry NameEntry;

		public ReceiversCreatePageMain ()  	
		{			
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Nuevo";
			ToolbarItems.Add (OKItem);
			Content = Stack;
		}

		void createElements ()
		{
			OKItem = new ToolbarItem { 
				Text = "Continuar",
			};
			NameEntry = new Entry {
				Placeholder = "Nombre",
				HeightRequest = 60,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Padding = new Thickness(10),
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (NameEntry);
		}

		void setListeners ()
		{
			OKItem.Clicked += async (sender, e) => {
				if (NameEntry.Text != null) {
					var receiverCreated = new ReceiverDBModel () {
						Name = NameEntry.Text,
					};
					var addressCount = App.LocalDataMan.countAddresses();
					if (addressCount > 0)
						await Navigation.PushAsync(new AddressesPageMain(receiverCreated, false));
					else
						await Navigation.PushAsync(new AddressesCreatePageMain(receiverCreated, true));
				} else {
					await DisplayAlert("Error", "Se debe ingresar el nombre", "Entiendo");
				}
			};
		}
	}
}

