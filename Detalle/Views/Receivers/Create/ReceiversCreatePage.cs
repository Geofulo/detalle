﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ReceiversCreatePage : ContentPage
	{
		CommisionsDBModel Commision;
		ToolbarItem OKItem;
		StackLayout Stack;
		Entry NameEntry;
		Entry EmailEntry;
		bool isToCart;
		bool fromWishes;

		public ReceiversCreatePage (CommisionsDBModel Commision, bool isToCart, bool fromWishes)	
		{			
			this.Commision = Commision;
			this.isToCart = isToCart;
			this.fromWishes = fromWishes;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Nuevo";
			NavigationPage.SetBackButtonTitle (this, "");
			//NavigationPage.SetBackButtonTitle (this, "¿A quién?");
			ToolbarItems.Add (OKItem);
			Content = Stack;
		}

		void createElements ()
		{
			OKItem = new ToolbarItem { 
				Text = "Hecho",
			};
			NameEntry = new Entry {
				Placeholder = "Nombre",
				HeightRequest = 60,
			};
			EmailEntry = new Entry {
				Placeholder = "Correo",
				HeightRequest = 60,
				Keyboard = Keyboard.Email,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Padding = new Thickness(10),
				Spacing = 10,
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (NameEntry);
			Stack.Children.Add (EmailEntry);
		}

		void setListeners ()
		{			
			OKItem.Clicked += (sender, e) => {
				if (NameEntry.Text != null) {
					var receiverCreated = new ReceiverDBModel () {
						Name = NameEntry.Text,
						Email = EmailEntry.Text,
					};
					if (App.LocalDataMan.CreateReceiver(receiverCreated)) {						
						Commision.IdReceiver = receiverCreated.IdReceiver;
						bool isCommisionSaved;
						if (isToCart) {
							Commision.InCart = true;
							if (fromWishes)
								isCommisionSaved = App.LocalDataMan.UpdateCommision(Commision);
							else
								isCommisionSaved = App.LocalDataMan.CreateCommision(Commision);
						} else {
							Commision.InCart = false;
							isCommisionSaved = App.LocalDataMan.CreateCommision(Commision);
						}					
						if (isCommisionSaved) {
							if (isToCart) {
								Navigation.PopToRootAsync();
								MessagingCenter.Send<ReceiversCreatePage>(this, "ProductAddedToCart");
							} else {
								Navigation.PopToRootAsync();
								MessagingCenter.Send<ReceiversCreatePage>(this, "ProductAddedToWishes");							
							}
						} else {
							showErrorMessage();
						}
					} else {
						DisplayAlert("Error", "Ocurrió un error al intentar crear el destinatario", "OK");
					}
				} else {
					DisplayAlert("Error", "Se debe ingresar el nombre", "Entiendo");
				}
			};

		}

		void showErrorMessage()
		{
			if(isToCart)
				DisplayAlert("Error", "Ocurrió un error al intentar agregar el producto al carrito", "OK");
			else
				DisplayAlert("Error", "Ocurrió un error al intentar agregar el producto en la lista de deseos", "OK");
		}
	}
}

