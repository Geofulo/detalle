﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class ReceiversPageMain : ContentPage
	{
		List<ReceiverDBModel> Receivers = new List<ReceiverDBModel>();
		ToolbarItem AddItem;
		Label AllTitleLabel;
		ListView ReceiversListView;
		Label NoReceiversLabel;
		StackLayout Stack;

		public ReceiversPageMain ()						
		{
			getReceivers ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();
			setMessagingCenter ();

			Title = "Destinatarios";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (AddItem);
			if (Receivers.Count > 0)
				Content = Stack;
			else
				Content = NoReceiversLabel;
			Padding = new Thickness (0, 20);
			BackgroundColor = Color.FromHex ("f8f8f9");
		}

		void getReceivers ()
		{
			Receivers = App.LocalDataMan.GetReceivers ();
		}

		void createElements ()
		{
			AddItem = new ToolbarItem {
				Icon = "navbar_ico_add.png",
			};
			AllTitleLabel = new Label {
				Text = "TODOS",
				TextColor = Color.Gray,
				FontSize = 12,
			};
			ReceiversListView = new ListView {
				//RowHeight = 100,
				ItemsSource = Receivers,
				ItemTemplate = new DataTemplate(typeof(ReceiversAddressTemplate)),
				IsPullToRefreshEnabled = true,
				HasUnevenRows = true,
				SeparatorColor = Color.FromHex("aaaaaa"),
			};
			NoReceiversLabel = new Label {
				Text = "No tienes ningún destinatario agregado",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}	

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 0,
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(25, 0, 0, 20),
				Children = { AllTitleLabel },
			});
			Stack.Children.Add (new BoxView {
				HeightRequest = 0.5,
				WidthRequest = App.ScreenWidth,
				BackgroundColor = Color.Gray,
			});
			Stack.Children.Add (ReceiversListView);
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				this.Navigation.PushAsync(new ReceiversCreatePageMain());
			};
			ReceiversListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var receiver = e.SelectedItem as ReceiverDBModel;				
				this.Navigation.PushAsync (new AddressesPageMain (receiver, true));
				ReceiversListView.SelectedItem = null;
			};
			ReceiversListView.Refreshing += (sender, e) => {
				getReceivers ();
				ReceiversListView.ItemsSource = Receivers;
				ReceiversListView.EndRefresh();
			};
		}

		void setMessagingCenter ()
		{			
			MessagingCenter.Subscribe<AddressesDetailPageMain> (this, "ReceiverCreated", (sender) => { 								
				this.Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El destinatario ha sido creado exitosamente", "OK");
				ReceiversListView.BeginRefresh();
				ReceiversListView.BeginRefresh();
				Content = Stack;
			});
			MessagingCenter.Subscribe<AddressesCreatePageMain> (this, "AddressCreated", (sender) => { 								
				this.Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El destinatario ha sido creado exitosamente", "OK");
				ReceiversListView.BeginRefresh();
				ReceiversListView.BeginRefresh();
				Content = Stack;
			});
			MessagingCenter.Subscribe<AddressesDetailPageMain> (this, "AddressUpdated", (sender) => { 								
				this.Navigation.PopAsync();
				ReceiversListView.BeginRefresh();
				ReceiversListView.BeginRefresh();
				Content = Stack;
			});

			MessagingCenter.Subscribe<ReceiversTemplate> (this, "DeleteReceiver", (sender) => { 								

			});
		}
	}
}

