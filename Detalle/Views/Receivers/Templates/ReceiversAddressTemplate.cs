﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ReceiversAddressTemplate : ViewCellCustom
	{
		Label ReceiverNameLabel;
		Image ReceiverImage;
		ListView AddressesListView;

		StackLayout Stack;
		StackLayout StackReceiver;

		public ReceiversAddressTemplate ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();

			View = Stack;
			IsDisclosureAccessory = true;
			Height = 100;
			//Height = 50 + AddressesListView.HeightRequest;
		}

		void createElements ()
		{			
			ReceiverImage = new ImageEffectCustom {
				Source = "Chocolates.png",
				WidthRequest = 50,
				HeightRequest = 50,
				VerticalOptions = LayoutOptions.StartAndExpand,
				IsCircle = true,
				Aspect = Aspect.AspectFill,
			};
			ReceiverNameLabel = new Label {
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			AddressesListView = new ListView {
				ItemTemplate = new DataTemplate(typeof(AddressReceiversTemplate)),
				SeparatorVisibility = SeparatorVisibility.None,
				IsEnabled = false,
				VerticalOptions = LayoutOptions.End,
				MinimumHeightRequest = 80,
			};

		}

		void createLayouts ()
		{
			StackReceiver = new StackLayout {				
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
			};
			Stack = new StackLayout {
				Padding = new Thickness(20, 10, 10, 10),
				Spacing = 0,
				MinimumHeightRequest = 100,
			};
		}

		void setLayouts ()
		{
			StackReceiver.Children.Add (ReceiverImage);
			StackReceiver.Children.Add (ReceiverNameLabel);

			Stack.Children.Add (StackReceiver);
			Stack.Children.Add (AddressesListView);
		}

		void setBindings ()
		{
			ReceiverNameLabel.SetBinding (Label.TextProperty, "Name");
			AddressesListView.SetBinding (ListView.ItemsSourceProperty, "Addresses");
		}

	}
}

