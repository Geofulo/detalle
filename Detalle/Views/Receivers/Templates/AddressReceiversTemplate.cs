﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class AddressReceiversTemplate : ViewCell
	{
		Label AddressLabel;
		StackLayout Stack;

		public AddressReceiversTemplate ()
		{
			createElements ();
			setBindings ();

			View = Stack;
		}

		void createElements ()
		{
			AddressLabel = new Label {
				TextColor = Color.FromHex("6563a4"),
			};
			Stack = new StackLayout {
				Padding = new Thickness(70, 0),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			Stack.Children.Add (AddressLabel);
		}

		void setBindings ()
		{
			AddressLabel.SetBinding (Label.TextProperty, "TagAddress");
		}
	}
}

