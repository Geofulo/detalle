﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ReceiversTemplate : ViewCellCustom
	{
		public ReceiverDBModel Receiver;
		Label NameLabel;
		Image ReceiverImage;

		MenuItem DeleteItem;

		StackLayout Stack;

		public ReceiversTemplate ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();
			setListeners ();

			View = Stack;
		}

		void createElements ()
		{
			NameLabel = new Label {				
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			ReceiverImage = new ImageEffectCustom {
				Source = "Chocolates.png",
				WidthRequest = 50,
				HeightRequest = 50,
				VerticalOptions = LayoutOptions.StartAndExpand,
				IsCircle = true,
				Aspect = Aspect.AspectFill,
			};
			DeleteItem = new MenuItem {
				Text = "Eliminar",
				IsDestructive = true,
			};

		}

		void createLayouts ()
		{			
			Stack = new StackLayout {
				Padding = new Thickness(20, 10, 10, 10),
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
			};
		}

		void setLayouts ()
		{			
			Stack.Children.Add (ReceiverImage);
			Stack.Children.Add (NameLabel);
		}

		void setBindings ()
		{			
			NameLabel.SetBinding (Label.TextProperty, "Name");
			DeleteItem.SetBinding (MenuItem.CommandParameterProperty, new Binding ("."));
		}

		void setListeners ()
		{
			DeleteItem.Clicked += (sender, e) => {
				var mi = ((MenuItem)sender);
				var receiver = mi.CommandParameter as ReceiverDBModel;

				App.LocalDataMan.DeleteReceiver(receiver);
				//var status = App.DataMan.DeleteReceiver(receiver);
			};
		}
			
	}
}

