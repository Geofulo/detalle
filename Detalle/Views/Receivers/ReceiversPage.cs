﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class ReceiversPage : ContentPage
	{
		CommisionsDBModel Commision;
		List<ReceiverDBModel> Receivers = new List<ReceiverDBModel>();
		ToolbarItem AddItem;
		ToolbarItem SkipItem;
		Label AllTitleLabel;
		ListView ReceiversListView;
		bool isToCart;
		bool fromWishes;
		StackLayout Stack;

		public ReceiversPage(CommisionsDBModel Commision, bool isToCart, bool fromWishes)
		{
			this.Commision = Commision;
			this.isToCart = isToCart;
			this.fromWishes = fromWishes;

			getReceivers ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "¿A quién?";
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (AddItem);
			if(!isToCart)
				ToolbarItems.Add (SkipItem);
			Content = Stack;
			Padding = new Thickness (0, 20);
			BackgroundColor = Color.FromHex ("f8f8f9");
		}

		void getReceivers ()
		{
			Receivers = App.LocalDataMan.GetReceivers ();
		}

		void createElements ()
		{
			AddItem = new ToolbarItem {
				//Text = "Agregar",
				Icon = "navbar_ico_add.png",
			};
			SkipItem = new ToolbarItem { 
				Text = "Omitir",
			};
			AllTitleLabel = new Label {
				Text = "TODOS",
				TextColor = Color.Gray,
				FontSize = 12,
			};
			ReceiversListView = new ListView {
				//RowHeight = 60,
				ItemsSource = Receivers,
				ItemTemplate = new DataTemplate(typeof(ReceiversTemplate)),
				IsPullToRefreshEnabled = true,
				SeparatorColor = Color.FromHex("aaaaaa"),
				HasUnevenRows = true,
			};

		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 0,
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(25, 0, 0, 20),
				Children = { AllTitleLabel },
			});
			Stack.Children.Add (new BoxView {
				HeightRequest = 0.5,
				WidthRequest = App.ScreenWidth,
				BackgroundColor = Color.FromHex("aaaaaa"),
			});
			Stack.Children.Add (ReceiversListView);
		}

		void setListeners ()
		{
			AddItem.Clicked += (sender, e) => {
				Navigation.PushAsync(new ReceiversCreatePage(Commision, isToCart, fromWishes));
			};
			SkipItem.Clicked += (sender, e) => {
				Commision.InCart = false;
				if (App.LocalDataMan.CreateCommision(Commision)) {					
					MessagingCenter.Send<ReceiversPage>(this, "ProductAddedToWishes");
				} else {
					showErrorMessage();
				}				
			};
			ReceiversListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var receiverSelected = e.SelectedItem as ReceiverDBModel;	
				Commision.IdReceiver = receiverSelected.IdReceiver;
				bool isCommisionSaved;
				if (isToCart) {
					Commision.InCart = true;
					if (fromWishes)
						isCommisionSaved = App.LocalDataMan.UpdateCommision(Commision);
					else
						isCommisionSaved = App.LocalDataMan.CreateCommision(Commision);
				} else {
					Commision.InCart = false;
					isCommisionSaved = App.LocalDataMan.CreateCommision(Commision);
				}					
				if (isCommisionSaved) {
					if (isToCart) {
						Navigation.PopToRootAsync();
						MessagingCenter.Send<ReceiversPage>(this, "ProductAddedToCart");
					} else {
						Navigation.PopToRootAsync();
						MessagingCenter.Send<ReceiversPage>(this, "ProductAddedToWishes");					
					}
				} else {
					showErrorMessage();
				}
				ReceiversListView.SelectedItem = null;
			};
			ReceiversListView.Refreshing += (sender, e) => {
				getReceivers ();
				ReceiversListView.ItemsSource = Receivers;
				ReceiversListView.EndRefresh();
			};
		}

		void showErrorMessage()
		{
			if(isToCart)
				DisplayAlert("Error", "Ocurrió un error al intentar agregar el producto al carrito", "OK");
			else
				DisplayAlert("Error", "Ocurrió un error al intentar agregar el producto en la lista de deseos", "OK");
		}
	}		
}

