﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ReceiverDetailPage : ContentPage
	{
		public ReceiverDBModel Receiver;
		ToolbarItem EditItem;
		ToolbarItem OKItem;
		Label NameLabel;
		Label EmailLabel;
		Entry NameEntry;
		Entry EmailEntry;
		StackLayout Stack;

		public ReceiverDetailPage (ReceiverDBModel Receiver)
		{
			this.Receiver = Receiver;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Title = "Destinatario " + Receiver.IdReceiver;
			NavigationPage.SetBackButtonTitle (this, "");
			ToolbarItems.Add (EditItem);
			Content = Stack;
		}

		void createElements ()
		{
			EditItem = new ToolbarItem {
				Text = "Editar",
			};
			OKItem = new ToolbarItem {
				Text = "Hecho",
			};
			NameLabel = new Label {
				Text = Receiver.Name,
			};
			EmailLabel = new Label {
				Text = Receiver.Email,
			};
			NameEntry = new Entry {
				Text = Receiver.Name,
				HeightRequest = 40,
			};
			EmailEntry = new Entry {
				Text = Receiver.Email,
				HeightRequest = 40,
				Keyboard = Keyboard.Email,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout (); 
		}

		void setLayouts ()
		{
			Stack.Children.Clear ();
			Stack.Children.Add (NameLabel);
			Stack.Children.Add (EmailLabel);
		}

		void setLayoutsEdit()
		{
			Stack.Children.Clear ();
			Stack.Children.Add (NameEntry);	
			Stack.Children.Add (EmailLabel);	
		}

		void setListeners ()
		{
			EditItem.Clicked += (sender, e) => {
				setLayoutsEdit();
				ToolbarItems.Clear();
				ToolbarItems.Add(OKItem);
			};
			OKItem.Clicked += (sender, e) => {
				Receiver.Name = NameEntry.Text;
				Receiver.Email = EmailEntry.Text;
				App.LocalDataMan.CreateReceiver (Receiver);
				//Receiver = App.DataMan.UpdateReceiver(Receiver);
				MessagingCenter.Send<ReceiverDetailPage>(this, "ReceiverSet");
				this.Navigation.PopAsync();
			};
		}
	}
}

