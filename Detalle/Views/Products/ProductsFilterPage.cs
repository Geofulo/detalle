﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class ProductsFilterPage : ContentPage
	{				
		CategoriesDBModel Category;
		List<TagsDBModel> Tags;
		List<CategoriesDBModel> Categories;
		public TagsDBModel TagSelected;
		public CategoriesDBModel CategorySelected;
		ToolbarItem OKItem;
		TableView FilterTableView;
		TableSection TagsTableSection;
		List<TextCell> TextCells;
		int indexSelected = 0;

		public ProductsFilterPage (CategoriesDBModel Category)
		{
			this.Category = Category;

			createElements ();
			getTags ();
			setListeners (0);

			Title = "Filtrar";
			ToolbarItems.Add (OKItem);
			Content = FilterTableView;
		}

		public ProductsFilterPage () 				// Best Products
		{
			createElements ();
			getCategories ();
			setListeners (1);

			Title = "Filtrar";
			ToolbarItems.Add (OKItem);
			Content = FilterTableView;
		}

		void createElements ()
		{
			OKItem = new ToolbarItem {
				Text = "Hecho"
			};
			FilterTableView = new TableView {
				Intent = TableIntent.Settings,		
				Root = new TableRoot(),
			};
			TagsTableSection = new TableSection ("Categorias");
		}

		void getTags ()
		{
			Tags = App.LocalDataMan.GetTags (Category);
			//Tags = App.DataMan.GetTags (Category);

			setCells (0);
		}

		void getCategories ()
		{
			Categories = App.LocalDataMan.GetCategories ();
			//Categories = App.DataMan.GetCategories ();

			setCells (1);
		}

		void setCells (int typeCell)
		{
			TextCells = new List<TextCell> ();

			if(typeCell == 0){
				foreach(var tag in Tags){
					TextCells.Add (new TextCell{
						Text = tag.Name,
					});
				}
			} else {
				foreach(var cat in Categories){
					TextCells.Add (new TextCell{
						Text = cat.Name,
					});
				}
			}		
			TextCells.Insert (0, new TextCell { 
				Text = "Todos",
				TextColor = Color.Aqua,
			});

			setLayouts ();
		}

		void setLayouts ()
		{
			foreach(var cell in TextCells){
				TagsTableSection.Add (cell);
			}
			FilterTableView.Root.Add (TagsTableSection);
		}

		void setListeners (int typeCell)
		{
			OKItem.Clicked += (sender, e) => {
				this.Navigation.PopModalAsync();
				MessagingCenter.Send<ProductsFilterPage>(this, "ReloadData");
			};
				
			foreach(var cell in TextCells){
				cell.Tapped += (sender, e) => {
					int index = TextCells.IndexOf(cell);
					TextCells[index].TextColor = Color.Aqua;
					if(typeCell == 0) {												
						TagSelected = Tags[index - 1];
					} else {
						CategorySelected = Categories[index - 1];
					}
				};
			}			
		}
	}
}

