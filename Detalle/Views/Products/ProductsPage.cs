﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Detalle
{
	public class ProductsPage : ContentPage
	{
		readonly int _numColumns = 2;
		CategoriesDBModel Category;
		ToolbarItem FilterItem;
		ActivityIndicator ProductsIndicator;
		StackLayout Stack;
		Grid Grid;
		ScrollView Scroll;
		List<ProductsDBModel> Data;
		bool BestProducts = false;

		public ProductsPage (CategoriesDBModel Category) 			// Productos de cada categoria		
		{
			this.Category = Category;

			createElements ();
			createLayouts ();
			getData (null);
			showProducts ();
			setListeners ();
			setMessagingCenter ();

			Title = Category.Name;
			NavigationPage.SetBackButtonTitle (this, "");
			BackgroundColor = Color.FromHex ("f8f8f9");
			ToolbarItems.Add (FilterItem);
			Content = Stack;
		}

		public ProductsPage () 										// Productos destacados
		{
			this.BestProducts = true;
			//this.IsBusy = true;

			createElements ();
			createLayouts ();
			getBestProducts (null);
			showProducts ();
			setListenersFromMenu ();
			setMessagingCenter ();
			setBindings ();

			Title = "Destacado";
			NavigationPage.SetBackButtonTitle (this, "");
			BackgroundColor = Color.FromHex ("f8f8f9");
			ToolbarItems.Add (FilterItem);
			Content = Stack;

			/*
			new Task (async () => {
				getBestProducts (null);
				if (Data != null) {
					showProducts ();
					Scroll.Content = Grid;
					Content = Stack;
				} else {
					await DisplayAlert("Error", "Ocurrió un error al intentar obtener los productos", "OK");
				}
			}).Start ();*/
		}

		void createElements ()
		{
			FilterItem = new ToolbarItem {				
				Icon = "navbar_ico_filter.png",
			};
			ProductsIndicator = new ActivityIndicator {
				IsRunning = true,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout ();
			Grid = new Grid {
				RowSpacing = 10,
				ColumnSpacing = 10,
			};
			Scroll = new ScrollView {
				Padding = new Thickness(10),
			};
		}

		void getBestProducts (CategoriesDBModel cat)
		{
			Data = App.LocalDataMan.GetProducts (cat);
			setLayouts ();
		}

		void getData (TagsDBModel tag)
		{
			Data = App.LocalDataMan.GetProducts (Category, tag);
			setLayouts ();
		}

		void setLayouts ()
		{
			Grid.Children.Clear ();

			Scroll.Content = Grid;

			Stack.Children.Clear ();
			Stack.Children.Add (Scroll);
		}

		void setListeners ()
		{
			FilterItem.Clicked += (sender, e) => {
				Navigation.PushModalAsync(new NavigationPage(new ProductsFilterPage(Category)));
			};
		}

		void setListenersFromMenu ()
		{
			FilterItem.Clicked += (sender, e) => {
				this.Navigation.PushModalAsync(new NavigationPage(new ProductsFilterPage()));
			};
		}

		void setMessagingCenter ()
		{
			if(BestProducts)
				MessagingCenter.Subscribe<ProductsFilterPage> (this, "ReloadData", (sender) => { 	
					getBestProducts(sender.CategorySelected);
				});
			else
				MessagingCenter.Subscribe<ProductsFilterPage> (this, "ReloadData", (sender) => { 				
					getData(sender.TagSelected);
				});

			MessagingCenter.Subscribe<ReceiversPage> (this, "ProductAddedToWishes", (sender) => { 								
				Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El producto fue agregado a la lista de deseos exitosamente", "OK");
			});

			MessagingCenter.Subscribe<ReceiversCreatePage> (this, "ProductAddedToWishes", (sender) => { 								
				Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El producto fue agregado a la lista de deseos exitosamente", "OK");
				if (BestProducts) {
					getBestProducts(null);
				} else {
					getData(null);
				}
				showProducts();
			});

			MessagingCenter.Subscribe<ReceiversPage> (this, "ProductAddedToCart", (sender) => { 												
				Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El producto fue agregado al carrito exitosamente", "OK");
			});

			MessagingCenter.Subscribe<ReceiversCreatePage> (this, "ProductAddedToCart", (sender) => { 								
				Navigation.PopToRootAsync();
				DisplayAlert("Correcto", "El producto fue agregado al carrito exitosamente", "OK");
			});
		}

		void setBindings ()
		{
			ProductsIndicator.SetBinding (ActivityIndicator.IsVisibleProperty, "IsBusy");
		}

		void showProducts ()
		{			
			Grid.Children.Clear ();
			int aux = 0;
			double rows = Math.Ceiling ((double)Data.Count / (double)_numColumns);
			//int rows = Data.Length /_numColumns;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < _numColumns; j++) {
					if (aux < Data.Count) {
						//new Task (async () => {
						var item = new ProductsTemplate (Data [aux]);
						item.WishButton.Clicked += (sender, e) => WishButtonClickedAction(item);
						var frame = new FrameCustom {
							HasShadow = true,
							Content = item,
							OutlineColor = Color.FromHex("f8f8f9"),
							ShadowRadius = 0.3f,
							ShadowOffsetHeight = 2.5f,
							ShadowOffsetWidth = 2.5f,
							ShadowOpacity = 0.1f,
							BorderRadius = 0,
							Padding = new Thickness(0),
						};

						Grid.Children.Add (frame, j, i);
						aux++;
						//}).Start ();
					}
				}
			}
			//this.IsBusy = false;
			//Scroll.Content = Grid;
			//Content = Stack;
		}

		async void WishButtonClickedAction (ProductsTemplate item)
		{
			if (App.LocalDataMan.IsProductInWishList (item.Product)) {				
				var canDelete = await DisplayAlert ("Eliminar", "¿Esta seguro de querer eliminar el producto de la lista de deseos?", "Eliminar", "Cancelar");
				if (canDelete) {	
					var commision = App.LocalDataMan.GetCommisionFromWishList (item.Product);
					App.LocalDataMan.DeleteCommision (commision);
					item.WishButton.Image = "ico_like.png";
				}
			} else {
				item.WishButton.Image = "ico_like_hover.png";
				var commision = new CommisionsDBModel {
					IdProduct = item.Product.IdProduct,
					MessageWritten = "",
				};
				var receivers = App.LocalDataMan.GetReceivers();
				if (receivers.Count == 0 || receivers == null) {
					var receiversCreatePage = new ReceiversCreatePage (commision, false, false);
					receiversCreatePage.Title = "¿A quién?";
					Navigation.PushAsync (receiversCreatePage);
				} else {
					Navigation.PushAsync(new ReceiversPage(commision, false, false));	
				}
			}	
		}
	}
}

