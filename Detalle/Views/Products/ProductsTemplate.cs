﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ProductsTemplate : StackLayout
	{
		public ProductsDBModel Product;
		Label NameLabel;
		Label PriceLabel;
		Image ProductImage;
		public Button WishButton;
		StackLayout PropertiesStack;
		TapGestureRecognizer _tapGesture = new TapGestureRecognizer();

		public ProductsTemplate (ProductsDBModel Product)
		{
			this.Product = Product;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			HeightRequest = 200;
			GestureRecognizers.Add (_tapGesture);
			BackgroundColor = Color.White;
			Spacing = 5;
			Children.Add (ProductImage);
			Children.Add (NameLabel);
			Children.Add (PropertiesStack);

		}

		void createElements ()
		{
			NameLabel = new Label {
				Text = Product.Name,
				TextColor = Color.Black,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			WishButton = new Button {
				//Image = "ico_like.png",
				TextColor = Color.FromHex("ff3366"),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
			};
			if (App.LocalDataMan.IsProductInWishList (Product)) {
				WishButton.Image = "ico_like_hover.png";
			} else {
				WishButton.Image = "ico_like.png";
			}
			PriceLabel = new Label {
				Text = "$" + Product.Price.ToString(),
				TextColor = Color.FromHex("6563a4"),
				XAlign = TextAlignment.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};
			var image = App.LocalDataMan.getProductImage (Product.IdProduct);
			if(image != null){
				try {
					ProductImage = new Image {
						Source = ImageSource.FromUri(new Uri(image)),
						HeightRequest = 120,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Aspect = Aspect.AspectFill,
					};	
				} catch(Exception e) {
					System.Diagnostics.Debug.WriteLine (e.Message);
					ProductImage = new Image {
						Source = "chocolates.png",
						HeightRequest = 120,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Aspect = Aspect.AspectFill,
					};
				}
			} else {
				ProductImage = new Image {
					Source = "chocolates.png",
					HeightRequest = 120,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Aspect = Aspect.AspectFill,
				};
			}
		}

		void createLayouts ()
		{
			PropertiesStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10, 2),
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void setLayouts ()
		{
			PropertiesStack.Children.Add (WishButton);
			PropertiesStack.Children.Add (PriceLabel);
		}

		void setListeners ()
		{
			_tapGesture.Tapped += (sender, e) => {
				this.Navigation.PushAsync(new ProductsDetailPage(Product));
			};
		}

	}
}

