﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ProductsDetailPersonalizeView : ContentView
	{
		StackLayout Stack;
		RelativeLayout SharePriceRelative;
		ContentView PropertiesContent;

		Button ShareButton;
		public Label PriceLabel;
		BoxView LineVerticalBox;
		BoxView LineHorizontalBox;

		public ProductsDetailPersonalizeView ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();

			Content = Stack;
		}

		void createElements ()
		{
			ShareButton = new Button {
				//Text = "Comparte",
				Image = "ico_share.png",
				WidthRequest = 80,
				HeightRequest = 50,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
			};
			PriceLabel = new Label {
				TextColor = Color.FromHex("6563a4"),
				WidthRequest = 80,
				HeightRequest = 50,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
			};	
			LineVerticalBox = new BoxView {
				WidthRequest = 0.5f,
				HeightRequest = 75,
				BackgroundColor = Color.FromHex("aaaaaa"),
			};
			LineHorizontalBox = new BoxView {				
				HeightRequest = 0.5f,
				WidthRequest = App.ScreenWidth,
				BackgroundColor = Color.FromHex("aaaaaa"),
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			PropertiesContent = new ContentView {
				HeightRequest = 500,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			SharePriceRelative = new RelativeLayout { 
				HeightRequest = 85,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
				Padding = new Thickness(0, 0, 0, 10),
			};
			Stack = new StackLayout {
				Spacing = 0,
			};
		}

		void setLayouts ()
		{
			SharePriceRelative.Children.Add (ShareButton,
				Constraint.RelativeToParent((parent) => { return (parent.Width / 4) - (ShareButton.WidthRequest / 2); }),
				Constraint.RelativeToParent((parent) => { return (parent.HeightRequest / 2) - (ShareButton.HeightRequest / 2); })
			);

			SharePriceRelative.Children.Add (LineVerticalBox,
				Constraint.RelativeToParent((parent) => { return (parent.Width / 2) - (LineVerticalBox.WidthRequest / 2); }),
				Constraint.Constant(0)
			);

			SharePriceRelative.Children.Add (PriceLabel,
				Constraint.RelativeToParent((parent) => { return ((parent.Width / 4) * 3) - (PriceLabel.WidthRequest / 2); }),
				Constraint.RelativeToParent((parent) => { return ShareButton.Y; })
			);

			Stack.Children.Add (SharePriceRelative);
			Stack.Children.Add (LineHorizontalBox);
			Stack.Children.Add (PropertiesContent);
		}

	}
}

