﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class ProductsDetailNoteView : ContentView
	{
		//StackLayout Stack;
		RelativeLayout Relative;
		StackLayout StackBottom;
		ContentView MessageContent;
		StackLayout StackColors;
		ScrollView ScrollColors;

		Button WriteButton;
		Button DrawButton;
		Button CleanButton;
		List<Button> ColorsButtons = new List<Button>();

		List<Color> Colors = new List<Color> { Color.Black, 
			Color.FromHex("ff0f26"), Color.FromHex("ff7e0f"),
			Color.FromHex("fad70f"), Color.FromHex("5ae10d"), 
			Color.FromHex("0ddcb4"), Color.FromHex("0f94ff"), Color.FromHex("0f20ff"), 
			Color.FromHex("540fff"), Color.FromHex("b50fff"), Color.FromHex("ff0fb7")
		};

		// Write
		public EditorCustom MessageEditor;

		// Draw
		ImageDrawCustom ImageDraw;
		Frame ImageFrame;
		ContentView ImageView;

		public ProductsDetailNoteView ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			Content = Relative;
			VerticalOptions = LayoutOptions.FillAndExpand;
		}

		void createElements ()
		{
			WriteButton = new Button {
				//Text = "Escribir",
				//TextColor = Color.FromHex("ff3366"),
				Image = "toolbar_ico_text_hover.png",
				BorderColor = Color.FromHex("aaaaaa"),
				BorderWidth = 0.5f,
				BorderRadius = 0,
				WidthRequest = App.ScreenWidth / 4,
				BackgroundColor = Color.FromHex("f8f8f9"),
			};
			DrawButton = new Button {
				//Text = "Dibujar",
				//TextColor = Color.Gray,
				Image = "toolbar_ico_draw.png",
				BorderColor = Color.FromHex("aaaaaa"),
				BorderWidth = 0.5f,
				BorderRadius = 0,
				WidthRequest = App.ScreenWidth / 4,
				BackgroundColor = Color.FromHex("f8f8f9"),
			};
			CleanButton = new Button {
				Text = "Eliminar nota",
				TextColor = Color.Gray,
				BorderColor = Color.FromHex("aaaaaa"),
				BorderWidth = 0.5f,
				BorderRadius = 0,
				WidthRequest = (App.ScreenWidth / 2) + 2.5,
				BackgroundColor = Color.FromHex("f8f8f9"),
				IsEnabled = false,
			};

			MessageEditor = new EditorCustom {
				HeightRequest = 320,
				WidthRequest = App.ScreenWidth - 40,
				BackgroundColor = Color.FromHex("f8f8f9"),
				BorderSize = 0.5f,
				BorderColor = Color.FromHex("aaaaaa"),
				TextColor = Color.Black,
				IsEnabled = true,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			MessageEditor.Focus();

			ImageDraw = new ImageDrawCustom {
				CurrentLineColor = Color.Blue,
				BackgroundColor = Color.FromHex("f8f8f9"),
				IsVisible = false,
			};

			foreach(var color in Colors)
			{
				ColorsButtons.Add (new Button {
					BorderRadius = 12,
					BorderWidth = 0,
					BackgroundColor = color,
					HeightRequest = 24,
					WidthRequest = 24,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
				});
			}
		}

		void createLayouts ()
		{
			ImageFrame = new FrameCustom {
				IsVisible = false,
				HasShadow = false,
				BorderRadius = 0,
				Padding = new Thickness(0),
				OutlineColor = Color.FromHex("aaaaaa"),
			};
			ImageView = new ContentView {
				WidthRequest = App.ScreenWidth - 40,
				HeightRequest = 320,
				Padding = new Thickness(20, 10),
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			MessageContent = new ContentView {
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(20, 10),
			};
			StackColors = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 12,
				HeightRequest = 60,
				BackgroundColor = Color.FromHex("f8f8f9"),
				Padding = new Thickness(12, 0),
				//HorizontalOptions = LayoutOptions.Fill,
			};
			ScrollColors = new ScrollView {
				WidthRequest = App.ScreenWidth,
				HeightRequest = 60,
				Orientation = ScrollOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			StackBottom = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = -0.5,
				HeightRequest = 60,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.End,
			};
			Relative = new RelativeLayout ();

		}

		void setLayouts ()
		{
			ImageFrame.Content = ImageDraw;
			ImageView.Content = ImageFrame;

			MessageContent.Content = MessageEditor;

			StackBottom.Children.Add (WriteButton);
			StackBottom.Children.Add (DrawButton);
			StackBottom.Children.Add (CleanButton);

			foreach(var colorButton in ColorsButtons)
			{
				StackColors.Children.Add (colorButton);
			}

			ScrollColors.Content = StackColors;

			Relative.Children.Add (MessageContent,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (ImageView,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (StackBottom,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - StackBottom.HeightRequest; })
			);
			Relative.Children.Add (ScrollColors,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return StackBottom.Y - StackColors.HeightRequest; })
			);
		}

		void setListeners()
		{
			ScrollColors.Scrolled += (sender, e) => { System.Diagnostics.Debug.WriteLine("SCROLLED"); };
			WriteButton.Clicked += (sender, e) => {
				WriteButton.Image = "toolbar_ico_text_hover.png";
				DrawButton.Image = "toolbar_ico_draw.png";
				verifyCleanButton();
				CleanButton.IsEnabled = false;
				MessageContent.IsVisible = true;
				ImageDraw.IsVisible = false;
				ImageFrame.IsVisible = false;
				MessagingCenter.Send<ProductsDetailNoteView> (this, "EnableAnimationMenu");
			};
			DrawButton.Clicked += (sender, e) => {
				DrawButton.Image = "toolbar_ico_draw_hover.png";
				WriteButton.Image = "toolbar_ico_text.png";
				//MessageContent.Content = ImageView;
				CleanButton.IsEnabled = true;
				ImageDraw.IsVisible = true;
				ImageFrame.IsVisible = true;
				MessageContent.IsVisible = false;
				MessagingCenter.Send<ProductsDetailNoteView> (this, "DisableAnimationMenu");
			};
			CleanButton.Clicked += (sender, e) => {
				MessageEditor.Text = "";
			};
			MessageEditor.TextChanged += (sender, e) => {
				verifyCleanButton();
			};

			foreach(var colorButton in ColorsButtons)
			{
				colorButton.Clicked += (sender, e) => {
					var btn = sender as Button;
					deselectColorsButtons();
					btn.BorderWidth = 2;
					btn.BorderColor = Color.Black;
					btn.IsEnabled = false;
					ImageDraw.CurrentLineColor = btn.BackgroundColor;
					MessageEditor.TextColor = btn.BackgroundColor;
				};
			}
		}

		void verifyCleanButton ()
		{
			if (MessageEditor.Text != "") {
				CleanButton.IsEnabled = true;
			} else {
				CleanButton.IsEnabled = false;
			}
		}

		void deselectColorsButtons ()
		{
			foreach (var colorButton in ColorsButtons) {
				colorButton.BorderWidth = 0;
				colorButton.IsEnabled = true;
			}
		}
	}
}

