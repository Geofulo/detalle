﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ProductsDetailPhotosPage : CarouselPage
	{
		Image[] Images;

		public ProductsDetailPhotosPage (Image[] Images)
		{			
			this.Images = Images;

			Title = "Imágenes";

			foreach(var img in Images)
			{
				Children.Add (new ContentPage {
					Content = new Image{
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.FillAndExpand,
						Aspect = Aspect.AspectFit,
						Source = img.Source,
					},
				});
			}
		}
	}
}

