﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Detalle
{
	public class ProductsDetailPage : ContentPage
	{
		ProductsDBModel Product;
		CommisionsDBModel Commision;
		bool fromWishes;

		ScrollView Scroll;
		StackLayout Stack;
		RelativeLayout Relative;
		RelativeLayout RelativeProduct;
		StackLayout StackDots;

		BoxView LineHorizontalBox;

		CarouselViewCustom Carousel;
		Image ProductImage;
		List<Button> Dots = new List<Button>();
		Button ShopCartButton;
		Button WishButton;
		Label DescriptionLabel;

		SegmentedControlCustom SegmentedControl;
		SegmentedControlOption PersonalizeControlOption;
		SegmentedControlOption NoteControlOption;
		SegmentedControlOption CommentsControlOption;

		SegmentControlCustom SegmentControl;
		SegmentControlView SegmentControlView;

		ProductsDetailPersonalizeView PersonalizeView;
		ProductsDetailNoteView NoteView;
		ProductsDetailCommentsView CommentsView;

		ContentView ProductContentView;
		ContentView NoteContentView;

		TapGestureRecognizer TapGestureSegmented = new TapGestureRecognizer();

		public ProductsDetailPage (ProductsDBModel Product)
		{
			this.Product = Product;
			this.fromWishes = false;

			createElements ();
			createViews ();
			createLayouts ();
			setViews ();
			setLayouts ();
			setListeners ();

			Title = Product.Name;
			NavigationPage.SetBackButtonTitle (this, "");
			Content = Scroll;
		}

		public ProductsDetailPage (ProductsDBModel Product, CommisionsDBModel Commision)
		{
			this.Product = Product;
			this.fromWishes = true;

			createElements ();
			createViews ();
			createLayouts ();
			setViews ();
			setLayouts ();
			setListeners ();

			Title = Product.Name;
			NavigationPage.SetBackButtonTitle (this, "");
			Content = Scroll;
		}

		void createElements ()
		{
			Carousel = new CarouselViewCustom {
				WidthRequest = App.ScreenWidth,
			};

			var images = App.LocalDataMan.GetProductImages (Product.IdProduct);
			foreach(var imageProduct in images)
			{
				Carousel.Children.Add (new Image {
					Source = ImageSource.FromUri(new Uri(imageProduct.Path)),
					Aspect = Aspect.AspectFill,
					WidthRequest = App.ScreenWidth,
					HeightRequest = 200,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				});
				Dots.Add (new Button {
					WidthRequest = 10,
					HeightRequest = 10,
					BorderRadius = 5,
					BackgroundColor = Color.FromHex("aaaaaa"),
					VerticalOptions = LayoutOptions.CenterAndExpand,
				});
			}
			Carousel.Children.Add (new Image {
				Source = "Chocolates.png",
				Aspect = Aspect.AspectFill,
				WidthRequest = App.ScreenWidth,
				HeightRequest = 200,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			});
			Dots.Add (new Button {
				WidthRequest = 10,
				HeightRequest = 10,
				BorderRadius = 5,
				BackgroundColor = Color.FromHex("aaaaaa"),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});

			Dots [0].BackgroundColor = Color.FromHex ("6563a4");
			var image = App.LocalDataMan.getProductImage (Product.IdProduct);
			if (image != null) {
				ProductImage = new Image {
					Source = ImageSource.FromUri(new Uri(image)),
					Aspect = Aspect.AspectFill,
					WidthRequest = App.ScreenWidth,
					HeightRequest = 200,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				};
			} else {
				ProductImage = new Image {
					Aspect = Aspect.AspectFill,
					Source = "chocolates.png",
					WidthRequest = App.ScreenWidth,
					HeightRequest = 200,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				};
			}
			ShopCartButton = new Button {
				Image = "ico_shopping_cart.png",
				BackgroundColor = Color.White,
				BorderWidth = 0,
				BorderRadius = 20,
				WidthRequest = 40,
				HeightRequest = 40,
			};
			if (App.LocalDataMan.IsProductInShoppingCart (Product)) {
				ShopCartButton.Image = "ico_shopping_cart_hover.png";
			} else {
				ShopCartButton.Image = "ico_shopping_cart.png";
			}
			WishButton = new Button {
				Image = "ico_like.png",
				BackgroundColor = Color.White,
				BorderWidth = 0,
				BorderRadius = 20,
				WidthRequest = 40,
				HeightRequest = 40,
			};
			if (App.LocalDataMan.IsProductInWishList (Product)) {
				WishButton.Image = "ico_like_hover.png";
			} else {
				WishButton.Image = "ico_like.png";
			}
			PersonalizeControlOption = new SegmentedControlOption {
				Text = "Personaliza",
			};
			NoteControlOption = new SegmentedControlOption {
				Text = "Nota",
			};
			CommentsControlOption = new SegmentedControlOption {
				Text = "Crítica",
			};
			DescriptionLabel = new Label {				
				Text = Product.Description,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			LineHorizontalBox = new BoxView {				
				HeightRequest = 0.5f,
				WidthRequest = App.ScreenWidth,
				BackgroundColor = Color.FromHex("aaaaaa"),
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createViews ()
		{
			PersonalizeView = new ProductsDetailPersonalizeView ();
			NoteView = new ProductsDetailNoteView ();
			CommentsView = new ProductsDetailCommentsView ();
			ProductContentView = new ContentView {
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			NoteContentView = new ContentView {
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			NoteContentView.IsVisible = false;

			PersonalizeView.PriceLabel.Text = "$" + Product.Price.ToString();
		}

		void createLayouts ()
		{
			SegmentedControl = new SegmentedControlCustom {
				WidthRequest = 60,
			};
			SegmentControl = new SegmentControlCustom {
				TintColor = Color.FromHex("ff3366"),
				HeightRequest = 30,
				WidthRequest = 120,			
				Padding = new Thickness(20, 20, 20, 10),
			};
			SegmentControl.AddSegment ("Personaliza");
			SegmentControl.AddSegment ("    Nota    ");
			SegmentControl.AddSegment ("  Crítica  ");
			SegmentControl.SelectedSegment = 0;

			Relative = new RelativeLayout {				
				HeightRequest = 200,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
			};
			StackDots = new StackLayout {
				Spacing = 10,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			RelativeProduct = new RelativeLayout {				
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 10,
			};
			Scroll = new ScrollView ();
		}

		void setViews ()
		{
			SegmentedControl.Children.Add (PersonalizeControlOption);
			SegmentedControl.Children.Add (NoteControlOption);
			SegmentedControl.Children.Add (CommentsControlOption);
		}

		void setLayouts ()
		{
			Relative.Children.Add (Carousel,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);	
			Relative.Children.Add (WishButton,
				Constraint.Constant(10),
				Constraint.RelativeToParent((parent) => { return parent.HeightRequest - WishButton.HeightRequest - 10; })
			);
			Relative.Children.Add (ShopCartButton,
				Constraint.Constant(10),
				Constraint.RelativeToParent((parent) => { return WishButton.Y - ShopCartButton.HeightRequest - 10; })
			);

			foreach(var dot in Dots)
			{
				StackDots.Children.Add (dot);
			}

			ProductContentView.Content = PersonalizeView;
			NoteContentView.Content = NoteView;

			var cont = new ContentView {
				Content = SegmentedControl,
				WidthRequest = 80,
			};

			/*
			RelativeProduct.Children.Add (Relative,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			RelativeProduct.Children.Add (SegmentControl,								
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return Relative.HeightRequest + 10; })
			);
			RelativeProduct.Children.Add (LineHorizontalBox,				
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return SegmentControl.Y + SegmentControl.HeightRequest + 10; })
			);
			RelativeProduct.Children.Add (DescriptionLabel,				
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return LineHorizontalBox.Y + LineHorizontalBox.HeightRequest + 20; })
			);
			RelativeProduct.Children.Add (ProductContentView,				
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return DescriptionLabel.Y + DescriptionLabel.HeightRequest + 10; })
			);
			RelativeProduct.Children.Add (NoteContentView,				
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return ProductContentView.Y + ProductContentView.HeightRequest + 10; })
			);

			Scroll.Content = RelativeProduct;
			*/


			Stack.Children.Add (Relative);
			Stack.Children.Add (StackDots);
			//Stack.Children.Add (cont);
			//Stack.Children.Add (SegmentedControl);
			Stack.Children.Add (SegmentControl);
			Stack.Children.Add (LineHorizontalBox);
			Stack.Children.Add (DescriptionLabel);
			Stack.Children.Add (ProductContentView);
			Stack.Children.Add (NoteContentView);
			//Stack.Children.Add (SegmentControl);
			//Stack.Children.Add (SegmentControlView);
			
			Scroll.Content = Stack;


		}

		void setListeners ()
		{
			WishButton.Clicked += CreateCommisionAction;
			ShopCartButton.Clicked += CreateCommisionAction;
							
			var TapGesture = new TapGestureRecognizer();
			TapGesture.Tapped += openNavigationPage;
			ProductImage.GestureRecognizers.Add (TapGesture);

			SegmentControl.SelectedSegmentChanged += (sender, e) => {
				System.Diagnostics.Debug.WriteLine(SegmentControl.SelectedSegment);
				if (SegmentControl.SelectedSegment == 0) {
					ProductContentView.Content = PersonalizeView;
					ProductContentView.IsVisible = true;
					NoteContentView.IsVisible = false;
					Relative.IsVisible = true;
					StackDots.IsVisible = true;
				} else if (SegmentControl.SelectedSegment == 1) {
					NoteContentView.IsVisible = true;
					ProductContentView.IsVisible = false;
					Relative.IsVisible = false;
					StackDots.IsVisible = false;
				} else if (SegmentControl.SelectedSegment == 2) {
					ProductContentView.Content = CommentsView;
					ProductContentView.IsVisible = true;
					NoteContentView.IsVisible = false;
					Relative.IsVisible = true;
					StackDots.IsVisible = true;
				}
			};
				

			Scroll.Scrolled += (sender, e) => {
				System.Diagnostics.Debug.WriteLine(SegmentControl.Y);
				System.Diagnostics.Debug.WriteLine(RelativeProduct.Y);
				System.Diagnostics.Debug.WriteLine(RelativeProduct.Bounds.Y);
				System.Diagnostics.Debug.WriteLine(RelativeProduct.Bounds.Top);

				if (SegmentControl.Y < 0) 
					System.Diagnostics.Debug.WriteLine("HEY YOU!!");
			};

			Carousel.Scrolled += (sender, e) => {
				cleanDotsSelection();
				Dots[Carousel.SelectedIndex].BackgroundColor = Color.FromHex("6563a4");

			};

			PersonalizeControlOption.GestureRecognizers.Add (TapGestureSegmented);
			NoteControlOption.GestureRecognizers.Add (TapGestureSegmented);
			CommentsControlOption.GestureRecognizers.Add (TapGestureSegmented);


			TapGestureSegmented.Tapped += (sender, e) => {
				System.Diagnostics.Debug.WriteLine ("TAP! " + sender.GetType().ToString());
				if (sender == PersonalizeControlOption) {
					ProductContentView.Content = PersonalizeView;
				} else if (sender == NoteControlOption) {
					ProductContentView.Content = NoteView;
				} else if (sender == CommentsControlOption) {
					ProductContentView.Content = CommentsView;
				}
			};

		}			

		void CreateCommisionAction (object sender, EventArgs e)
		{
			if (Commision == null) {
				Commision = new CommisionsDBModel {
					IdProduct = Product.IdProduct,
					MessageWritten = "",
				};
			}
			var receivers = App.LocalDataMan.GetReceivers();
			if (receivers.Count == 0 || receivers == null) {
				if (sender == ShopCartButton) {
					var receiversCreatePage = new ReceiversCreatePage (Commision, true, fromWishes);
					receiversCreatePage.Title = "¿A quién?";
					Navigation.PushAsync (receiversCreatePage);
				} else {
					var receiversCreatePage = new ReceiversCreatePage (Commision, false, fromWishes);
					receiversCreatePage.Title = "¿A quién?";
					Navigation.PushAsync (receiversCreatePage);
				}
			} else {
				if (sender == ShopCartButton)
					Navigation.PushAsync(new ReceiversPage(Commision, true, fromWishes));	
				else 
					Navigation.PushAsync(new ReceiversPage(Commision, false, fromWishes));	
			}
		}

		void openNavigationPage (object sender, EventArgs e)
		{
			if(sender == ProductImage){
				this.Navigation.PushAsync (new ProductsDetailPhotosPage(new Image[]{ProductImage}));
			} 
		}

		void showMessage()
		{
			DisplayAlert ("Correcto", "Producto añadido al pedido exitosamente", "OK");
		}

		void cleanDotsSelection ()
		{
			foreach (var dot in Dots) {
				dot.BackgroundColor = Color.FromHex("aaaaaa");
			}
		}
	}
}

