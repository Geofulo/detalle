﻿using System;
using KinveyXamarin;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Detalle
{
	public class DataManager
	{				

		AsyncAppData<CategoriesModel> CategoriesData;
		AsyncAppData<TagsModel> TagsData;
		AsyncAppData<ProductsModel> ProductsData;
		AsyncAppData<AttributesModel> AttributesData;
		AsyncAppData<OrdersModel> OrdersData;
		AsyncAppData<CommisionsModel> CommisionsData;
		//AsyncAppData<AddressModel> AddressData;
		//AsyncAppData<ReceiverModel> ReceiversData;

		InMemoryCache<CategoriesModel> CategoriesCache;
		InMemoryCache<ProductsModel> ProductsCache;
		InMemoryCache<TagsModel> TagsCache;
		InMemoryCache<AttributesModel> AttributesCache;
		InMemoryCache<OrdersModel> OrdersCache;
		InMemoryCache<CommisionsModel> CommisionsCache;
		//InMemoryCache<AddressModel> AddressesCache;
		//InMemoryCache<ReceiverModel> ReceiversCache;

		readonly string CategoriesTableName = "Categorias";
		readonly string TagsTableName = "Etiquetas";
		readonly string OrdersTableName = "Pedidos";
		readonly string CommisionsTableName = "Envios";
		//readonly string AddressesTableName = "Direcciones";
		//readonly string ReceiversTableName = "Destinatarios";
		readonly string ProductsTableName = "Productos";
		readonly string AttributesTableName = "Atributos";

		public DataManager () {

			//createLocalDataBase ();

			CategoriesData = App.client.AppData<CategoriesModel>(CategoriesTableName, typeof(CategoriesModel));
			//CategoriesData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			CategoriesCache = new InMemoryCache<CategoriesModel>();
			CategoriesData.setCache(CategoriesCache, CachePolicy.CACHE_FIRST);

			ProductsData = App.client.AppData<ProductsModel>(ProductsTableName, typeof(ProductsModel));
			//ProductsData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			ProductsCache = new InMemoryCache<ProductsModel>();
			ProductsData.setCache(ProductsCache, CachePolicy.CACHE_FIRST);

			TagsData = App.client.AppData<TagsModel>(TagsTableName, typeof(TagsModel));
			//TagsData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			TagsCache = new InMemoryCache<TagsModel>();
			TagsData.setCache(TagsCache, CachePolicy.CACHE_FIRST);

			AttributesData = App.client.AppData<AttributesModel>(AttributesTableName, typeof(AttributesModel));
			//AttributesData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			AttributesCache = new InMemoryCache<AttributesModel>();
			AttributesData.setCache(AttributesCache, CachePolicy.CACHE_FIRST);

			OrdersData = App.client.AppData<OrdersModel>(OrdersTableName, typeof(OrdersModel));
			//OrdersData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			OrdersCache = new InMemoryCache<OrdersModel>();
			OrdersData.setCache(OrdersCache, CachePolicy.CACHE_FIRST);

			CommisionsData = App.client.AppData<CommisionsModel>(CommisionsTableName, typeof(CommisionsModel));
			//CommisionsData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			CommisionsCache = new InMemoryCache<CommisionsModel>();
			CommisionsData.setCache(CommisionsCache, CachePolicy.CACHE_FIRST);

			/*
			AddressData = App.client.AppData<AddressModel>(AddressesTableName, typeof(AddressModel));
			AddressData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.LOCAL_FIRST);
			AddressesCache = new InMemoryCache<AddressModel>();
			AddressData.setCache(AddressesCache, CachePolicy.CACHE_FIRST);

			ReceiversData = App.client.AppData<ReceiverModel>(ReceiversTableName, typeof(ReceiverModel));
			ReceiversData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.LOCAL_FIRST);
			ReceiversCache = new InMemoryCache<ReceiverModel>();
			ReceiversData.setCache(ReceiversCache, CachePolicy.CACHE_FIRST);
			*/
		}

		// ::::::::::::::::::::: CATEGORIES :::::::::::::::::::::

		public CategoriesModel[] GetCategories()
		{
			Print ("GetCategories...");
			var tcs = new TaskCompletionSource<CategoriesModel[]>();
			new Task (async () => {
				try {
					var categories = await CategoriesData.GetAsync();
					tcs.SetResult(categories);
					Print(categories.Length + " categorias");
				} catch(Exception e){
					tcs.SetResult(null);
					Print(e.Message);
				}
			}).Start();
			return tcs.Task.Result;
		}

		public void CreateCategories()
		{
			var category1 = new CategoriesModel {
				Name = "Peluches",
			};
			var category2 = new CategoriesModel {
				Name = "Chocolates",
			};
			var category3 = new CategoriesModel {
				Name = "Flores",
			};

		}

		// ::::::::::::::::::::: PRODUCTS :::::::::::::::::::::

		public ProductsModel[] GetProducts()
		{
			Print ("GetProducts...");
			var TCSProducts = new TaskCompletionSource<ProductsModel[]>();
			new Task (async () => {
				try {
					var products = await ProductsData.GetAsync();
					TCSProducts.SetResult(products);
					Print (products.Length + " productos encontrados");
				} catch(Exception e) {
					TCSProducts.SetResult(null);
					Print(e.Message);
				}
			}).Start ();
			return TCSProducts.Task.Result;
		}	

		ProductsModel getProduct (ProductsDBModel product)
		{
			Print ("GetProduct...");
			var tcs = new TaskCompletionSource<ProductsModel>();
			new Task (async () => {
				try {
					var query = from productK in ProductsData				
							where productK.Id == product.IdKinvey
						select productK;					
					tcs.SetResult(query.First<ProductsModel>());
				} catch(Exception e) {
					tcs.SetResult(null);
					Print(e.Message);
				}
			}).Start ();
			return tcs.Task.Result;
		}

		// ::::::::::::::::::::: TAGS :::::::::::::::::::::

		public TagsModel[] GetTags()
		{
			Print ("GetTags...");
			var TCSTags = new TaskCompletionSource<TagsModel[]>();
			new Task (async () => {			
				try {
					var tags = await TagsData.GetAsync ();
					TCSTags.SetResult(tags);
					Print (tags.Length + " etiquetas encontradas");
				} catch(Exception e) {
					TCSTags.SetResult(null);
					Print (e.Message);
				}
			}).Start ();
			return TCSTags.Task.Result;
		}			

		// ::::::::::::::::::::: ORDERS :::::::::::::::::::::

		public bool CreatePendingOrder(OrdersDBModel order)
		{
			Print ("CreatePendingOrder");
			var tcs = new TaskCompletionSource<bool>();
			new Task (async () => {
				try {										
					var commisionsReference = new List<KinveyReference<CommisionsModel>> ();
					var commisions = App.LocalDataMan.GetCommisions();
					foreach(var commision in commisions)
					{						
						var commisionCreated = createCommision (commision);
						var commisionOrderRef = new KinveyReference<CommisionsModel> (CommisionsTableName, commisionCreated.Id);
						commisionOrderRef.resolved = commisionCreated;
						commisionsReference.Add (commisionOrderRef);
					}
					var orderKinvey = new OrdersModel {
						Status = 1,
						DateOrder = DateTime.Now.ToString(),
						DateDeliver = order.DateDeliver,
						TimeDeliver = order.TimeDeliver,
						Address = order.Address,
						Reference = order.Reference,
						Phone = order.Phone,
						Commisions = commisionsReference.ToArray<KinveyReference<CommisionsModel>>(),
					};
					var orderUpdate = await OrdersData.SaveAsync(orderKinvey);
					Print("Orden " + orderKinvey.Id + " guardada como pendiente");
					tcs.SetResult(true);
				} catch (Exception e) {					
					Print (e.Message);
					tcs.SetResult(false);
				}
			}).Start ();
			return tcs.Task.Result;
		}			
			
		// ::::::::::::::::::::: COMMISIONS :::::::::::::::::::::

		public CommisionsModel createCommision (CommisionsDBModel commision)
		{
			Print ("CreateCommision...");
			var tcs = new TaskCompletionSource<CommisionsModel>();
			new Task (async () => {
				try {					
					var product = App.LocalDataMan.getProduct (commision.IdProduct);
					var commisionCreated = new CommisionsModel {
						MessageWritten = commision.MessageWritten,
						ReceiverName = App.LocalDataMan.getReceiver (commision.IdReceiver).Name,
						ProductName = product.Name,
						ProductPrice = product.Price.ToString(),
						ImagePath = App.LocalDataMan.getProductImage(product.IdProduct),
					};					
					var newCommision = await CommisionsData.SaveAsync (commisionCreated);
					tcs.SetResult(newCommision);
					System.Diagnostics.Debug.WriteLine ("Entrega " + commisionCreated.Id + " guardada");
				} catch (Exception e) {
					tcs.SetResult(null);
					System.Diagnostics.Debug.WriteLine (e.Message);
				}
			}).Start ();
			return tcs.Task.Result;
		}

		// ::::::::::::::::::::: IMAGES :::::::::::::::::::::

		public string GetImageSource(FileMetaData md)
		{
			Print ("GetImageSource...");
			var tcs = new TaskCompletionSource<string>();
			App.client.File ().downloadMetadata (md.id, new KinveyDelegate<FileMetaData>{
				onSuccess = (meta) => {
					tcs.SetResult(meta.downloadURL);
				},
				onError = (obj) => {
					tcs.SetResult("");
				}
			});
			return tcs.Task.Result;
		}


		// ::::::::::::::::::::: OTHERS :::::::::::::::::::::

		public void Print(string msg)
		{
			System.Diagnostics.Debug.WriteLine (msg);
		}
	}
}

