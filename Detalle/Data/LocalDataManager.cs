﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using SQLite;

namespace Detalle
{
	public class LocalDataManager
	{
		static object locker = new object ();

		SQLiteConnection Database;

		TableQuery<CategoriesDBModel> CategoriesTable;
		TableQuery<TagsDBModel> TagsTable;
		TableQuery<ProductsDBModel> ProductsTable;
		TableQuery<ProductTagsDBModel> ProductTagsTable;
		TableQuery<ProductAttributesDBModel> ProductAttributesTable;
		TableQuery<ProductImagesDBModel> ProductImagesTable;
		TableQuery<AddressDBModel> AddressesTable;
		TableQuery<ReceiverDBModel> ReceiversTable;
		TableQuery<ReceiverAddressesDBModel> ReceiverAddressesTable;
		TableQuery<CommisionsDBModel> CommisionsTable;
		TableQuery<OrdersDBModel> OrdersTable;
		TableQuery<OrderCommisionsDBModel> OrderCommisionsTable;

		public LocalDataManager ()
		{
			createLocalDataBase ();
			getTables ();
		}

		void createLocalDataBase ()
		{
			Database = DependencyService.Get<ISQLite> ().GetConnection ();

			Database.CreateTable<CategoriesDBModel> ();
			Database.CreateTable<TagsDBModel> ();
			Database.CreateTable<ProductsDBModel> ();
			Database.CreateTable<ProductTagsDBModel> ();
			Database.CreateTable<ProductAttributesDBModel> ();
			Database.CreateTable<ProductImagesDBModel> ();
			Database.CreateTable<AddressDBModel> ();
			Database.CreateTable<ReceiverDBModel> ();
			Database.CreateTable<ReceiverAddressesDBModel> ();
			Database.CreateTable<CommisionsDBModel> ();
			Database.CreateTable<OrdersDBModel> ();
			Database.CreateTable<OrderCommisionsDBModel> ();

			Database.DeleteAll<CategoriesDBModel> ();
			Database.DeleteAll<TagsDBModel> ();
			Database.DeleteAll<ProductsDBModel> ();
			Database.DeleteAll<ProductTagsDBModel> ();
			Database.DeleteAll<ProductAttributesDBModel> ();
			Database.DeleteAll<ProductImagesDBModel> ();
			Database.DeleteAll<AddressDBModel> ();
			Database.DeleteAll<ReceiverDBModel> ();
			Database.DeleteAll<ReceiverAddressesDBModel> ();
			Database.DeleteAll<CommisionsDBModel> ();
			Database.DeleteAll<OrdersDBModel> ();
			Database.DeleteAll<OrderCommisionsDBModel> ();
		}

		void getTables ()
		{
			CategoriesTable = Database.Table<CategoriesDBModel> ();
			TagsTable = Database.Table<TagsDBModel> ();
			ProductsTable = Database.Table<ProductsDBModel> ();
			ProductTagsTable = Database.Table<ProductTagsDBModel> ();
			ProductAttributesTable = Database.Table<ProductAttributesDBModel> ();
			ProductImagesTable = Database.Table<ProductImagesDBModel> ();
			AddressesTable = Database.Table<AddressDBModel> ();
			ReceiversTable = Database.Table<ReceiverDBModel> ();
			ReceiverAddressesTable = Database.Table<ReceiverAddressesDBModel> ();
			CommisionsTable = Database.Table<CommisionsDBModel> ();
			OrdersTable = Database.Table<OrdersDBModel> ();
			OrderCommisionsTable = Database.Table<OrderCommisionsDBModel> ();
		}

		public void GetDataFromKinvey()
		{
			getCategoriesFromKinvey ();
			getTagsFromKinvey ();
			getProductsFromKinvey ();
		}

		// ::::::::::::::::::::: CATEGORIES :::::::::::::::::::::

		void getCategoriesFromKinvey ()
		{
			Print ("getCategoriesFromKinvey...");
			var categories = App.DataMan.GetCategories ();
			foreach(var category in categories)
			{
				Print (category.Id + " - " + category.Name);
				//lock (locker) {					
				var categoryDB = new CategoriesDBModel() {
						IdKinvey = category.Id,
						Name = category.Name,
				};
				Database.Insert (categoryDB);
				//}
			}
		}

		public List<CategoriesDBModel> GetCategories()
		{
			var result = new List<CategoriesDBModel> ();
			try {
				foreach(var category in Database.Table<CategoriesDBModel>())
				{
					result.Add(category);
				}
				Print (result.Count + " categorias");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}			

		int getIdCategory (string IdKinveyCategory)
		{						
			var cats = from s in Database.Table<CategoriesDBModel> ()
			           where s.IdKinvey == IdKinveyCategory
			           select s;
			var cat = cats.First ();
			return cat.IdCategory;


			/*

			return categories.FirstOrDefault ().IdCategory;
			*/
		}

		// ::::::::::::::::::::: TAGS :::::::::::::::::::::

		void getTagsFromKinvey ()
		{
			Print ("getTagsFromKinvey...");
			var tags = App.DataMan.GetTags ();
			if (tags.Length > 0) {
				foreach(var tag in tags)
				{
					var tagDB = new TagsDBModel() {
						IdKinvey = tag.Id,
						Name = tag.Name,
					};
					Database.Insert (tagDB);
				}
			}
		}

		public List<TagsDBModel> GetTags (CategoriesDBModel category)
		{
			var result = new List<TagsDBModel> ();
			try {					
				foreach(var productTag in ProductTagsTable)
				{
					if(getProduct(productTag.IdProduct).IdCategory == category.IdCategory)						
						result.Add (getTag(productTag.IdTag));
				}
				Print (result.Count + " etiquetas");
				return result; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		int getIdTag (TagsModel tag)
		{
			var tags = from s in TagsTable
					where s.IdKinvey == tag.Id
				select s;
			return tags.First ().IdTag;
		}

		TagsDBModel getTag (int tagId)
		{
			try {
				return Database.Get<TagsDBModel>(tagId);
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		// ::::::::::::::::::::: PRODUCTS :::::::::::::::::::::

		void getProductsFromKinvey ()
		{
			Print ("getProductsFromKinvey...");
			var products = App.DataMan.GetProducts ();
			foreach(var product in products)
			{
				var productDB = new ProductsDBModel() {
					IdKinvey = product.Id,
					Name = product.Name,
					Description = product.Description,
					Price = product.Price,
					Priority = product.Priority,
					IdCategory = getIdCategory(product.Category.resolved.Id),
				};
				Database.Insert (productDB);

				getProductTagsFromKinvey (productDB, product);
				getProductAttributesFromKinvey (productDB, product);
				getProductImagesFromKinvey (productDB, product);
			}
		}	

		public List<ProductsDBModel> GetProducts(CategoriesDBModel category)
		{
			Print ("GetProducts...");
			var result = new List<ProductsDBModel> ();
			try {
				Print(Database.Table<ProductsDBModel>().Count().ToString());
				TableQuery<ProductsDBModel> products;
				if(category != null) {
					products = from ps in Database.Table<ProductsDBModel>()
							where ps.IdCategory == category.IdCategory
						orderby ps.Priority
						select ps;
				} else {
					products = from ps in Database.Table<ProductsDBModel>()							
						orderby ps.Priority
						select ps;
				}
				foreach(var product in products)
				{
					result.Add (product);
				}
				Print (result.Count + " productos");
				return result; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public List<ProductsDBModel> GetProducts(CategoriesDBModel category, TagsDBModel tag)
		{
			Print ("GetProducts...");
			var result = new List<ProductsDBModel> ();
			try {
				var products = from ps in Database.Table<ProductsDBModel>()
						where ps.IdCategory == category.IdCategory
					select ps;
				foreach(var product in products)
				{
					if(tag == null) {						
						result.Add (product);
					} else {
						var productTags = getProductTags(product);
						foreach(var productTag in productTags)
						{
							if(product.IdProduct == productTag.IdProduct) {
								result.Add (product);
							}
						}
					}
				}
				Print (result.Count + " productos");
				return result; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public ProductsDBModel getProduct (int idProduct)
		{
			try {
				return Database.Get<ProductsDBModel>(idProduct);
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		// ::::::::::::::::::::: PRODUCT_TAGS :::::::::::::::::::::

		List<ProductTagsDBModel> getProductTags(ProductsDBModel product)
		{
			var result = new List<ProductTagsDBModel> ();
			try {
				var tags = from pts in Database.Table<ProductTagsDBModel>()
						where pts.IdProduct == product.IdProduct
					select pts;
				foreach(var tag in tags)
				{
					result.Add (tag);
				}
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		// ::::::::::::::::::::: PRODUCT_IMAGES :::::::::::::::::::::

		public string getProductImage (int idProduct)
		{
			try {
				var productImage = from pi in Database.Table<ProductImagesDBModel>()
						where pi.IdProduct == idProduct
					select pi;
				return productImage.First().Path;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public List<ProductImagesDBModel> GetProductImages (int idProduct)
		{
			var result = new List<ProductImagesDBModel> ();
			try {
				var productImage = from pi in Database.Table<ProductImagesDBModel>()
						where pi.IdProduct == idProduct
					select pi;
				foreach(var image in productImage)
				{
					result.Add (image);					
				}
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		// ::::::::::::::::::::: ADDRESS :::::::::::::::::::::
	
		public List<AddressDBModel> GetAddresses()
		{
			Print ("GetAddresses...");
			var result = new List<AddressDBModel> ();
			try {				
				foreach(var address in AddressesTable)
				{
					result.Add (address);
				}
				Print (result.Count + " direcciones");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public List<AddressDBModel> GetAddresses(ReceiverDBModel receiver)
		{
			Print ("GetAddresses...");
			var result = new List<AddressDBModel> ();
			try {
				var receiverAddresses = from ras in Database.Table<ReceiverAddressesDBModel>()
						where ras.IdReceiver == receiver.IdReceiver
					select ras;
				foreach(var receiverAddress in receiverAddresses)
				{
					result.Add (getAddress(receiverAddress.IdAddress));
				}
				Print (result.Count + " direcciones");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public AddressDBModel getAddress (int idAddress)
		{
			try {
				return Database.Get<AddressDBModel>(idAddress);
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool CreateAddress(AddressDBModel address)
		{
			Print ("CreateAddress...");
			try {
				Database.Insert (address);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool UpdateAddress(AddressDBModel address)
		{
			Print ("UpdateAddress...");
			try {
				Database.Update (address);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool DeleteAddress (AddressDBModel address)
		{
			Print ("DeleteAddress...");
			try {
				Database.Delete<AddressDBModel>(address);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}			

		public int countAddresses ()
		{
			try {
				return AddressesTable.Count();
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return 0;
			}
		}

		// ::::::::::::::::::::: RECEIVERS :::::::::::::::::::::

		public bool CreateReceiver(ReceiverDBModel receiver)
		{
			Print ("CreateReceiver...");
			try {
				Database.Insert (receiver);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public List<ReceiverDBModel> GetReceivers()
		{
			Print ("GetReceivers...");
			var result = new List<ReceiverDBModel> ();
			try {				
				foreach(var receiverAddress in ReceiversTable)
				{
					result.Add (receiverAddress);
				}
				Print (result.Count + " destinatarios");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}
		/*
		public List<ReceiverDBModel> GetReceivers(OrdersDBModel order)
		{
			Print ("GetReceivers...");
			var result = new List<ReceiverDBModel> ();
			try {				
				var commisions = from cs in Database.Table<CommisionsDBModel>()
						where cs.IdOrder == order.IdOrder
					select cs;
				foreach(var commision in commisions)
				{
					var receiver = getReceiver(commision.IdReceiver);
					if(!result.Exists(x => x.IdReceiver == receiver.IdReceiver))
						result.Add (receiver);
				}
				Print (result.Count + " destinatarios");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}*/

		public ReceiverDBModel getReceiver (int id)
		{
			try {
				return Database.Get<ReceiverDBModel> (id);
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool DeleteReceiver(ReceiverDBModel receiver)
		{
			Print ("DeleteReceiver...");
			try {
				int status = Database.Delete (receiver);
				System.Diagnostics.Debug.WriteLine ("Status: " + status);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		// ::::::::::::::::::::: RECEIVERS_ADDRESS :::::::::::::::::::::

		List<ReceiverAddressesDBModel> getReceiverAddresses(AddressDBModel address)
		{			
			Print ("CreateReceiverAddress...");
			var result = new List<ReceiverAddressesDBModel> ();
			try {
				var receiverAddresses = from ras in Database.Table<ReceiverAddressesDBModel>()
						where ras.IdAddress == address.IdAddress
					select ras;
				foreach(var receiverAddress in receiverAddresses)
				{
					result.Add (receiverAddress);
				}
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public ReceiverAddressesDBModel GetReceiverAddresses(ReceiverDBModel receiver, AddressDBModel address)
		{			
			Print ("GetReceiverAddresses...");
			try {
				var receiverAddresses = from ras in Database.Table<ReceiverAddressesDBModel>()
						where ras.IdReceiver == receiver.IdReceiver && ras.IdAddress == address.IdAddress
					select ras;				
				return receiverAddresses.First();
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool CreateReceiverAddress(ReceiverDBModel receiver, AddressDBModel address)
		{
			Print ("CreateReceiverAddress...");
			try {
				var receiverAddress = new ReceiverAddressesDBModel {
					IdReceiver = receiver.IdReceiver,
					IdAddress = address.IdAddress,
				};
				Database.Insert (receiverAddress);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool CreateReceiverAddress(ReceiverAddressesDBModel receiverAddress)
		{
			Print ("CreateReceiverAddress...");
			try {				
				Database.Insert (receiverAddress);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool DeleteReceiverAddress (ReceiverDBModel receiver, AddressDBModel address)
		{
			Print ("DeleteReceiverAddress...");
			try {
				var receiverAddresses = from ras in Database.Table<ReceiverAddressesDBModel>()
						where ras.IdReceiver == receiver.IdReceiver && ras.IdAddress == address.IdAddress
					select ras;
				if (receiverAddresses.Count() > 0 || receiverAddresses != null) {
					Database.Delete<ReceiverAddressesDBModel>(receiverAddresses);
					return true;
				} else {
					return false;
				}
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool ExistsReceiverAddress (ReceiverDBModel receiver, AddressDBModel address)
		{
			Print ("ExistsReceiverAddress...");
			try {
				var receiverAddresses = from ras in Database.Table<ReceiverAddressesDBModel>()
						where ras.IdReceiver == receiver.IdReceiver && ras.IdAddress == address.IdAddress
					select ras;
				if (receiverAddresses.Count() > 0 || receiverAddresses != null)
					return true;
				else
					return false;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}			

		// ::::::::::::::::::::: COMMISIONS :::::::::::::::::::::

		public List<CommisionsDBModel> GetCommisions()
		{
			Print ("GetCommisions...");
			var result = new List<CommisionsDBModel> ();
			try {
				foreach(var commision in CommisionsTable)
				{
					result.Add(commision);
				}
				Print (result.Count + " envíos");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}			

		public bool CreateCommision(CommisionsDBModel commision)
		{
			Print ("CreateCommision...");
			try {
				Database.Insert (commision);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool UpdateCommision(CommisionsDBModel commision)
		{
			Print ("UpdateCommision...");
			try {
				Database.Update (commision);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool DeleteCommision(CommisionsDBModel commision)
		{
			Print ("DeleteCommision...");
			try {								
				Database.Delete<CommisionsDBModel>(commision.IdCommision);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		bool CleanCommisions ()
		{
			try {
				Database.DeleteAll<CommisionsDBModel> ();
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;	
			}
		}			

		// ::::::::::::::::::::: WISHES_LIST :::::::::::::::::::::

		public List<CommisionsDBModel> GetWishesList()
		{
			Print ("GetShoppingOrder...");
			var result = new List<CommisionsDBModel> ();
			try {								
				var commisions = from cs in Database.Table<CommisionsDBModel>()
						where cs.InCart == false
					select cs;
				
				foreach(var commision in commisions)
				{
					result.Add (commision);
				}
				Print (result.Count + " pedidos en lista de deseos");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public CommisionsDBModel GetCommisionFromWishList(ProductsDBModel product)
		{
			Print ("GetShoppingOrder...");
			try {								
				var commisions = from cs in Database.Table<CommisionsDBModel>()
						where cs.InCart == false && cs.IdProduct == product.IdProduct
					select cs;
				return commisions.First();
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool IsProductInWishList(ProductsDBModel product)
		{
			Print ("IsProductInWishList...");
			try {								
				var commision = from cs in Database.Table<CommisionsDBModel>()
						where cs.InCart == false && cs.IdProduct == product.IdProduct
					select cs;
				if (commision != null) {
					if (commision.Count() > 0)
						return true;
					else 
						return false;
				} else {
					return false;
				}
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		// ::::::::::::::::::::: SHOPPING_ORDERS :::::::::::::::::::::

		public List<CommisionsDBModel> GetShoppingCart()
		{
			Print ("GetShoppingCart...");
			var result = new List<CommisionsDBModel> ();
			try {								
				var commisions = from cs in Database.Table<CommisionsDBModel>()
						where cs.InCart == true
					select cs;
				foreach(var commision in commisions)
				{
					result.Add (commision);
				}
				Print (result.Count + " pedidos en carrito");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool IsProductInShoppingCart(ProductsDBModel product)
		{
			Print ("IsProductInWishList...");
			try {								
				var commision = from cs in Database.Table<CommisionsDBModel>()
						where cs.InCart == true && cs.IdProduct == product.IdProduct
					select cs;
				if (commision != null) {
					if (commision.Count() > 0)
						return true;
					else 
						return false;
				} else {
					return false;
				}
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		// ::::::::::::::::::::: ORDERS :::::::::::::::::::::

		public bool CreatePendingOrder(OrdersDBModel order)
		{
			Print ("CreatePendingOrder...");
			try {
				App.DataMan.CreatePendingOrder(order);
				Database.Insert (order);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public List<OrdersDBModel> GetPendingOrders()
		{
			Print ("GetPendingOrders...");
			var result = new List<OrdersDBModel> ();
			try {		
				var orders = from os in Database.Table<OrdersDBModel>()
						where os.Status != 4
					select os;
				foreach(var order in orders)
				{
					result.Add (order);
				}
				Print (result.Count + " ordenes");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public List<OrdersDBModel> GetDeliveredOrders()
		{
			Print ("GetDeliveredOrders...");
			var result = new List<OrdersDBModel> ();
			try {		
				var orders = from os in Database.Table<OrdersDBModel>()
						where os.Status == 4
					select os;
				foreach(var order in orders)
				{
					result.Add (order);
				}
				Print (result.Count + " ordenes");
				return result;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool DeleteDeliveredOrder(OrdersDBModel order)
		{
			Print ("DeleteDeliveredOrder...");
			try {		
				Database.Delete<OrdersDBModel>(order);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		// ::::::::::::::::::::: ORDER_COMMISION :::::::::::::::::::::

		public bool CreateOrderCommision (OrderCommisionsDBModel orderCommision)
		{
			try {
				Database.Insert (orderCommision);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}


		// ::::::::::::::::::::: OTHERS :::::::::::::::::::::

		void getProductTagsFromKinvey (ProductsDBModel productDB, ProductsModel product)
		{
			foreach(var tag in product.Tags)
			{
				var productTag = new ProductTagsDBModel() {
					IdProduct = productDB.IdProduct,
					IdTag = getIdTag (tag.resolved),
				};
				Database.Insert (productTag);
			}
		}

		void getProductAttributesFromKinvey (ProductsDBModel productDB, ProductsModel product)
		{
			if (product.Attributes != null) {
				foreach (var attribute in product.Attributes) {
					var productAttribute = new ProductAttributesDBModel () {
						IdProduct = productDB.IdProduct,
						Key = attribute.resolved.Key,
						Value = attribute.resolved.Value,
					};
					Database.Insert (productAttribute);
				}
			}
		}

		void getProductImagesFromKinvey (ProductsDBModel productDB, ProductsModel product)
		{
			foreach (var image in product.Images) 
			{
				var productImage = new ProductImagesDBModel() {
					IdProduct = productDB.IdProduct,
					Path = App.DataMan.GetImageSource(image),
					//Path = image.downloadURL,
				};
				Database.Insert (productImage);
			}
		}

		// ::::::::::::::::::::: OTHERS :::::::::::::::::::::

		void Print(string msg)
		{
			System.Diagnostics.Debug.WriteLine (msg);
		}
	}
}

