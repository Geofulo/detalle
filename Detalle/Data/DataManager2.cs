﻿using System;
using KinveyXamarin;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Detalle
{
	/*
	public class DataManager2
	{				

		AsyncAppData<CategoriesModel> CategoriesData;
		AsyncAppData<ProductsModel> ProductsData;
		AsyncAppData<TagsModel> TagsData;
		//AsyncAppData<OrdersModel> OrdersData;
		//AsyncAppData<CommisionsModel> CommisionsData;
		//AsyncAppData<AddressModel> AddressData;
		//AsyncAppData<ReceiverModel> ReceiversData;

		InMemoryCache<CategoriesModel> CategoriesCache;
		InMemoryCache<ProductsModel> ProductsCache;
		InMemoryCache<TagsModel> TagsCache;
		//InMemoryCache<OrdersModel> OrdersCache;
		//InMemoryCache<CommisionsModel> CommisionsCache;
		//InMemoryCache<AddressModel> AddressesCache;
		//InMemoryCache<ReceiverModel> ReceiversCache;

		readonly string CategoriesTableName = "Categorias";
		readonly string TagsTableName = "Etiquetas";
		readonly string OrdersTableName = "PedidosF";
		readonly string CommisionsTableName = "EntregasF";
		readonly string AddressesTableName = "DireccionesF";
		readonly string ReceiversTableName = "DestinatariosF";
		readonly string ProductsTableName = "Productos";

		public DataManager2 () {

			//createLocalDataBase ();

			CategoriesData = App.client.AppData<CategoriesModel>(CategoriesTableName, typeof(CategoriesModel));
			CategoriesData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			CategoriesCache = new InMemoryCache<CategoriesModel>();
			CategoriesData.setCache(CategoriesCache, CachePolicy.CACHE_FIRST);

			ProductsData = App.client.AppData<ProductsModel>(ProductsTableName, typeof(ProductsModel));
			ProductsData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			ProductsCache = new InMemoryCache<ProductsModel>();
			ProductsData.setCache(ProductsCache, CachePolicy.CACHE_FIRST);

			TagsData = App.client.AppData<TagsModel>(TagsTableName, typeof(TagsModel));
			TagsData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.ALWAYS_ONLINE);
			TagsCache = new InMemoryCache<TagsModel>();
			TagsData.setCache(TagsCache, CachePolicy.CACHE_FIRST);

			OrdersData = App.client.AppData<OrdersModel>(OrdersTableName, typeof(OrdersModel));
			OrdersData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.LOCAL_FIRST);
			OrdersCache = new InMemoryCache<OrdersModel>();
			OrdersData.setCache(OrdersCache, CachePolicy.CACHE_FIRST);

			CommisionsData = App.client.AppData<CommisionsModel>(CommisionsTableName, typeof(CommisionsModel));
			CommisionsData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.LOCAL_FIRST);
			CommisionsCache = new InMemoryCache<CommisionsModel>();
			CommisionsData.setCache(CommisionsCache, CachePolicy.CACHE_FIRST);

			AddressData = App.client.AppData<AddressModel>(AddressesTableName, typeof(AddressModel));
			AddressData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.LOCAL_FIRST);
			AddressesCache = new InMemoryCache<AddressModel>();
			AddressData.setCache(AddressesCache, CachePolicy.CACHE_FIRST);

			ReceiversData = App.client.AppData<ReceiverModel>(ReceiversTableName, typeof(ReceiverModel));
			ReceiversData.setOffline(new SQLiteOfflineStore(), OfflinePolicy.LOCAL_FIRST);
			ReceiversCache = new InMemoryCache<ReceiverModel>();
			ReceiversData.setCache(ReceiversCache, CachePolicy.CACHE_FIRST);

		}

		// ::::::::::::::::::::: CATEGORIES :::::::::::::::::::::

		public CategoriesModel[] GetCategories()
		{
			Print ("GetCategories...");
			var tcs = new TaskCompletionSource<CategoriesModel[]>();
			new Task (async () => {
				try {
					var categories = await CategoriesData.GetAsync();
					tcs.SetResult(categories);
					Print(categories.Length + " categorias");
				} catch(Exception e){
					tcs.SetResult(null);
					Print(e.Message);
				}
			}).Start();
			return tcs.Task.Result;
		}

		// ::::::::::::::::::::: PRODUCTS :::::::::::::::::::::

		public ProductsModel[] GetProducts(CategoriesModel category, TagsModel tagM)
		{
			Print ("GetProducts...");
			List<ProductsModel> result = new List<ProductsModel> ();
			var TCSProducts = new TaskCompletionSource<ProductsModel[]>();
			new Task (async () => {
				try {
					var products = await ProductsData.GetAsync();
					foreach(var product in products)
					{
						if(product.Category.resolved.Id == category.Id)
						{
							if(tagM == null) {
								result.Add (product);
							} else {
								foreach(var tag in product.Tags)
								{
									if(tag.resolved.Name == tagM.Name && !result.Exists(x => x.Id == product.Id))
									{
										result.Add (product);
									}
								}
							}
						}
					} 
					TCSProducts.SetResult(result.ToArray());
					Print (result.Count + " productos encontrados");
				} catch(Exception e) {
					TCSProducts.SetResult(null);
				}
			}).Start ();
			return TCSProducts.Task.Result;
		}		

		public ProductsModel[] GetBestProducts(CategoriesModel cat)
		{			
			Print ("GetBestProducts...");
			List<ProductsModel> result = new List<ProductsModel> ();
			var TCSProducts = new TaskCompletionSource<ProductsModel[]>();
			var query = from product in ProductsData				
				orderby product.DeliveredCount descending
				select product;	
			Task.Run (() => {
				int i = 0;
				foreach (var product in query) 
				{					
					if (i++ < 10) {
						if(cat == null)
							result.Add (product);
						else if(product.Category.resolved.Name == cat.Name)
							result.Add (product);						
					}
				}	
				TCSProducts.SetResult(result.ToArray<ProductsModel>());
				Print (result.Count + " mejores productos");
			});
			return TCSProducts.Task.Result;
		}

		public List<ProductsModel> GetProductsFromCommision(OrdersModel order, ReceiverDBModel receiver)
		{			
			Print ("GetProductsFromCommision...");
			var result = new List<ProductsModel> ();
			foreach(var commision in order.Commisions)
			{
				if (commision.resolved.Receiver.resolved.Id != null && commision.resolved.Receiver.resolved.Id == receiver.Id) {
					foreach(var product in commision.resolved.Products)
					{
						result.Add (product.resolved);
					}	
				}
			}
			Print (result.Count + " productos del pedido " + order.Id + " para " + receiver.Name);
			return result;
		}

		// ::::::::::::::::::::: TAGS :::::::::::::::::::::

		public TagsModel[] GetTags(CategoriesModel Category)
		{
			Print ("GetTags...");
			var result = new List<TagsModel> ();
			var TCSTags = new TaskCompletionSource<TagsModel[]>();
			new Task (async () => {						
				var products = await ProductsData.GetAsync ();
				var query = from product in products 
						where product.Category.resolved.Name == Category.Name 
					select product;
				foreach(var product in query.ToArray<ProductsModel>())
				{
					foreach(var tag in product.Tags)
					{
						if(!result.Exists(x => x.Name == tag.resolved.Name))	
							result.Add (tag.resolved);
					}
				}
				TCSTags.SetResult(result.ToArray());
				Print (result.Count + " etiquetas encontradas");
			}).Start ();
			return TCSTags.Task.Result;
		}			

		// ::::::::::::::::::::: ORDERS :::::::::::::::::::::

		public OrdersModel[] GetPendingOrCancelOrders() 
		{
			Print ("GetPendingOrCancelOrders...");
			List<OrdersModel> result = new List<OrdersModel> ();
			var tcs = new TaskCompletionSource<OrdersModel[]>();
			new Task (async () => {				
				var orders = await OrdersData.GetAsync();
				foreach(var order in orders) {
					if(IsPendingOrCancel(order))
						result.Add(order);
				}
				tcs.SetResult(result.ToArray<OrdersModel>());
				Print (result.Count + " ordenes pendientes o canceladas encontradas");
			}).Start ();
			return tcs.Task.Result;
		}

		public bool IsPendingOrCancel(OrdersModel order)
		{
			if (order.Status == 1 || order.Status == 2 || order.Status == 3)
				return true;
			return false;
		}

		public OrdersModel[] GetDeliveredOrders() 
		{
			Print ("GetDeliveredOrders...");
			var result = new List<OrdersModel> ();
			var TCSDeliveredOrders = new TaskCompletionSource<OrdersModel[]>();
			try {
				var query = from order in OrdersData
						where order.Status == 4
					select order;
				foreach(var order in query)
					result.Add (order);
				TCSDeliveredOrders.SetResult(result.ToArray<OrdersModel>());
				Print (result.Count + " ordenes entregadas encontradas");
			} catch(Exception e) {
				Print (e.Message);
				TCSDeliveredOrders.SetResult (null);
			}
			return TCSDeliveredOrders.Task.Result;
		}

		public bool CreatePendingOrder(OrdersModel order)
		{
			Print ("CreatePendingOrder");
			var tcs = new TaskCompletionSource<bool>();
			new Task (async () => {
				try {
					order.Status = 1;
					var orderUpdate = await OrdersData.SaveAsync(order);
					tcs.SetResult(true);
					Print("Orden " + order.Id + " guardada como pendiente");
				} catch (Exception e) {
					tcs.SetResult(false);
					Print (e.Message);
				}
			}).Start ();
			return tcs.Task.Result;
		}

		public OrdersModel CreateEmptyOrder(OrdersModel order)
		{
			Print ("CreateEmptyOrder...");
			var tcs = new TaskCompletionSource<OrdersModel>();
			new Task (async () => {
				var orderCreated = await OrdersData.SaveAsync(order);
				tcs.SetResult(orderCreated);
				Print ("Orden " + orderCreated.Id + " creada vacía");
			}).Start ();
			return tcs.Task.Result;
		}

		public async Task<bool> DeleteOrder(OrdersModel order)
		{
			try {					
				var deleted = await OrdersData.DeleteAsync(order.Id);
				Print ("Pedido " + order.Id + " eliminado");
				return true;					
			} catch(Exception e){
				Print (e.Message);
				return false;
			}
		}

		// ::::::::::::::::::::: SHOPPING CART :::::::::::::::::::::

		public OrdersModel GetShoppingCart()
		{			
			Print ("GetShoppingCart...");
			var tcs = new TaskCompletionSource<OrdersModel>();
			new Task (async () => {						
				var orders = await OrdersData.GetAsync();
				if (!orders.ToList<OrdersModel>().Exists(o => o.Status == 5)) {
					Print ("No se encontro ningún pedido en carrito");
					tcs.SetResult(null);	
				} else {
					foreach(var order in orders)
					{
						if (order.Status == 5)
						{
							tcs.SetResult(order);
							Print ("Orden " + order.Id + " en carrito");	
						}						
					}
				}
			}).Start ();				
			return tcs.Task.Result;
		}

		public async Task<OrdersModel> CreateCartOrder(OrdersModel order, CommisionsModel commision, AddressModel address)
		{
			Print ("CreateCartOrder...");
			try {
				var commisionRef = new KinveyReference<CommisionsModel> (CommisionsTableName, commision.Id);
				commisionRef.resolved = commision;
				var commisionsReferences = new List<KinveyReference<CommisionsModel>>();
				commisionsReferences.Add (commisionRef);

				var addressReference = new KinveyReference<AddressModel> (AddressesTableName, address.Id);
				addressReference.resolved = address;

				order.Commisions = commisionsReferences.ToArray<KinveyReference<CommisionsModel>>();
				order.Address = addressReference;
				order.Status = 5;

				var orderSaved = await OrdersData.SaveAsync(order);
				Print ("Orden " + orderSaved.Id + " guardada con " + orderSaved.Commisions.Length + " pedidos");
				return orderSaved;
			} catch(Exception e) {
				Print (e.Message);
				return null;
			}
		}

		public async Task<OrdersModel> AddProductToCartOrder(OrdersModel order, CommisionsModel commision)
		{
			Print ("AddProductToCartOrder...");
			try {
				var commisions = new List<KinveyReference<CommisionsModel>>();
				foreach(var commisionRef in order.Commisions)
				{
					if(commisionRef.resolved.Id == commision.Id) 
					{	
						commisionRef.resolved = commision;
					} 
					commisions.Add(commisionRef);
				}

				order.Commisions = commisions.ToArray<KinveyReference<CommisionsModel>>();

				var orderUpdate = await OrdersData.SaveAsync(order);
				Print ("Orden " + orderUpdate.Id + " editada con " + orderUpdate.Commisions.Length + " pedidos");
				return orderUpdate;
			} catch(Exception e) {
				Print (e.Message);
				return null;
			}
		}

		public async Task<OrdersModel> AddCommisionToCartOrder(OrdersModel order, CommisionsModel commision)
		{
			Print ("AddCommisionToCartOrder...");
			try {
				var commisionRef = new KinveyReference<CommisionsModel>(CommisionsTableName, commision.Id);
				commisionRef.resolved = commision;

				var commisions = new List<KinveyReference<CommisionsModel>>();
				foreach(var commiRef in order.Commisions)
				{
					commisions.Add(commiRef);
				}
				commisions.Add(commisionRef);

				order.Commisions = commisions.ToArray<KinveyReference<CommisionsModel>>();

				var orderUpdate = await OrdersData.SaveAsync(order);
				Print ("Orden " + orderUpdate.Id + " editada con " + orderUpdate.Commisions.Length + " pedidos");
				return orderUpdate;
			} catch(Exception e) {				
				Print (e.Message);
				return null;
			}
		}			

		// ::::::::::::::::::::: COMMISIONS :::::::::::::::::::::


		public async Task<CommisionsModel> UpdateCommision(CommisionsModel commision, ReceiverModel receiver, ProductsModel product)
		{
			Print ("UpdateCommision...");
			var productsReferences = new List<KinveyReference<ProductsModel>>();
			foreach(var productRef in commision.Products)
			{
				productsReferences.Add(productRef);
			}
			var newProductRef = new KinveyReference<ProductsModel> (ProductsTableName, product.Id);
			newProductRef.resolved = product;
			productsReferences.Add(newProductRef);
			commision.Products = productsReferences.ToArray<KinveyReference<ProductsModel>>();
			var commisionUpdate = await CommisionsData.SaveAsync(commision);
			return commisionUpdate;
		}

		public List<CommisionsModel> GetCommisionsFromOrder(OrdersModel order)
		{
			Print ("GetCommisionsFromOrder...");
			var result = new List<CommisionsModel> ();
			foreach(var commision in order.Commisions)
			{
				result.Add (commision.resolved);
			}				
			System.Diagnostics.Debug.WriteLine (result.Count + " pedidos en la orden");
			return result;
		}

		public CommisionsModel GetCommisionFromOrder(OrdersModel order, ReceiverModel receiver)
		{
			Print ("GetCommisionFromOrder...");
			CommisionsModel result = null;
			foreach(var commision in order.Commisions)
			{
				if (commision.resolved.Receiver.resolved.Id == receiver.Id)
					result = commision.resolved;
			}				
			return result;
		}

		public async Task<CommisionsModel> CreateCommision(ReceiverModel receiver, ProductsModel product)
		{
			Print ("CreateCommision...");
			try {
				var receiverRef = new KinveyReference<ReceiverModel> (ReceiversTableName, receiver.Id);
				receiverRef.resolved = receiver;

				var productRef = new KinveyReference<ProductsModel> (ProductsTableName, product.Id);
				productRef.resolved = product;

				var productsReferences = new List<KinveyReference<ProductsModel>>();
				productsReferences.Add(productRef);

				var commision = new CommisionsModel () {
					Receiver = receiverRef,
					Products = productsReferences.ToArray<KinveyReference<ProductsModel>> (),
				};

				var commisionCreated = await CommisionsData.SaveAsync(commision);
				System.Diagnostics.Debug.WriteLine ("Entrega " + commisionCreated.Id + " guardada");
				return commisionCreated;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public async Task<CommisionsModel> AddProductToCommision(CommisionsModel commision, ProductsModel product)
		{
			Print ("AddProductToCommision...");
			try {
				var productRef = new KinveyReference<ProductsModel> (ProductsTableName, product.Id);
				productRef.resolved = product;

				var productsReferences = new List<KinveyReference<ProductsModel>>();
				foreach(var prodRef in commision.Products)
				{
					productsReferences.Add(prodRef);
				}
				productsReferences.Add(productRef);

				commision.Products = productsReferences.ToArray<KinveyReference<ProductsModel>>();

				var commisionUpdated = await CommisionsData.SaveAsync(commision);
				System.Diagnostics.Debug.WriteLine ("Entrega " + commision.Id + " editada");
				return commisionUpdated;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public async Task<bool> DeleteCommision(CommisionsModel commision)
		{
			try {					
				var deleted = await CommisionsData.DeleteAsync (commision.Id);	
				Print ("Envío " + commision.Id + " eliminado");
				return true;
			} catch (Exception e) {
				Print (e.Message);
				return false;
			}
		}

		public async Task<CommisionsModel> DeleteProductCommision(CommisionsModel commision, ProductsModel product)
		{
			try {					
				var productsReferences = new List<KinveyReference<ProductsModel>>();
				foreach(var prodRef in commision.Products) 
				{
					if (prodRef.resolved.Id != product.Id) 
					{
						productsReferences.Add(prodRef);
					}
				}
				commision.Products = productsReferences.ToArray<KinveyReference<ProductsModel>>();
				var commisionUpdated = await CommisionsData.SaveAsync(commision);
				Print ("Producto " + product.Id + " eliminado del envío " + commision.Id);
				return commisionUpdated;					
			} catch (Exception e) {
				Print (e.Message);
				return null;
			}			

		}

		// ::::::::::::::::::::: ADDRESSES :::::::::::::::::::::

		public List<AddressModel> GetAddresses(ReceiverModel receiver)
		{
			Print ("GetAddresses...");
			var result = new List<AddressModel> ();
			if (receiver.Addresses != null) {
				foreach (var address in receiver.Addresses) {
					result.Add (address.resolved);
				}
				System.Diagnostics.Debug.WriteLine (result.Count + " direcciones");
			} else {
				result = null;
			}
			return result;
		}

		public List<AddressModel> GetAddresses()
		{
			Print ("GetAddresses...");
			var tcs = new TaskCompletionSource<List<AddressModel>>();
			new Task (async () => {
				var addresses = await AddressData.GetAsync();
				tcs.SetResult(addresses.ToList<AddressModel>());
				System.Diagnostics.Debug.WriteLine (addresses.Length + " direcciones");
			}).Start ();
			return tcs.Task.Result;
		}

		public async Task<AddressModel> CreateAddress(AddressModel address)
		{
			Print ("CreateAddress...");
			try {
				var addressCreated = await AddressData.SaveAsync (address);
				Print ("Dirección " + addressCreated.Id + " guardada");
				return addressCreated;
			} catch (Exception e){
				Print (e.Message);
				return null;
			}
		}

		public async Task<AddressModel> UpdateAddress(AddressModel address)
		{
			Print ("UpdateAddress...");	
			var addressUpdate = await AddressData.SaveAsync (address);
			System.Diagnostics.Debug.WriteLine ("Dirección " + address.Id + " modificada");
			return addressUpdate;
		}

		public bool DeleteAddress(AddressModel address)
		{
			Print ("DeleteAddress...");
			var tcs = new TaskCompletionSource<bool>();
			new Task (async () => {								
				try {
					var deleted = await AddressData.DeleteAsync(address.Id);
					tcs.SetResult(true);
					System.Diagnostics.Debug.WriteLine ("Dirección " + address.Id + " eliminada");
				} catch(Exception e) {
					tcs.SetResult(false);
					System.Diagnostics.Debug.WriteLine (e.Message);
				}
			}).Start ();
			return tcs.Task.Result;
		}

		public async Task<bool> ExistsAddresses()
		{
			Print ("ExistsAddresses...");
			var addresses = await AddressData.GetAsync ();
			if (addresses.Length > 0) {
				return true;
			} else {
				return false;
			}
		}

		// ::::::::::::::::::::: RECEIVERS :::::::::::::::::::::

		public List<ReceiverModel> GetReceivers()
		{
			Print ("GetReceivers...");
			var TCSReceivers = new TaskCompletionSource<List<ReceiverModel>>();
			new Task (async () => {
				try {
					var receivers = await ReceiversData.GetAsync();
					TCSReceivers.SetResult(receivers.ToList<ReceiverModel>());
					System.Diagnostics.Debug.WriteLine (receivers.Length + " destinatarios");
				} catch(Exception e){
					TCSReceivers.SetResult(null);
					System.Diagnostics.Debug.WriteLine (e.Message);
				}
			}).Start ();
			return TCSReceivers.Task.Result;
		}

		public List<ReceiverModel> GetReceiversFromOrder(OrdersModel order)
		{
			Print ("GetReceiversFromOrder...");
			var result = new List<ReceiverModel> ();
			foreach(var commision in order.Commisions)
			{
				var receiver = commision.resolved.Receiver.resolved;
				if (!result.Exists (x => x.Id == receiver.Id))
					result.Add (receiver);
			}				
			System.Diagnostics.Debug.WriteLine (result.Count + " destinatarios");
			return result;
		}

		public async Task<ReceiverModel> CreateReceiver(ReceiverModel receiver, AddressModel address)
		{
			Print ("CreateReceiver...");
			try {
				var addressRef = new KinveyReference<AddressModel> (AddressesTableName, address.Id);
				addressRef.resolved = address;
				var addresses = new List<KinveyReference<AddressModel>> ();
				if(receiver.Addresses != null) 
				{
					foreach (var addr in receiver.Addresses) 
					{
						addresses.Add (addr);
					}
				}
				addresses.Add (addressRef);
				receiver.Addresses = addresses.ToArray<KinveyReference<AddressModel>>();

				var receiverCreated = await ReceiversData.SaveAsync (receiver);
				System.Diagnostics.Debug.WriteLine ("Destinatario " + receiverCreated.Name + " guardado");
				return receiverCreated;
			} catch(Exception e) {				
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public ReceiverModel UpdateReceiver(ReceiverModel receiver)
		{
			Print ("UpdateReceiver...");
			var tcs = new TaskCompletionSource<ReceiverModel>();
			new Task (async () => {								
				var receiverUpdated = await ReceiversData.SaveAsync(receiver);
				tcs.SetResult(receiverUpdated);
				System.Diagnostics.Debug.WriteLine ("Destinatario " + receiverUpdated.Id + " modificado");
			}).Start ();
			return tcs.Task.Result;
		}

		public bool DeleteReceiver(ReceiverModel receiver)
		{
			Print ("DeleteReceiver...");
			var tcs = new TaskCompletionSource<bool>();
			new Task (async () => {				
				try {
					var deleted = await ReceiversData.DeleteAsync(receiver.Id);
					tcs.SetResult(true);
					System.Diagnostics.Debug.WriteLine ("Destinatario " + receiver.Id + " eliminado");
				} catch(Exception e) {
					tcs.SetResult(false);
					System.Diagnostics.Debug.WriteLine (e.Message);
				}
			}).Start ();
			return tcs.Task.Result;
		}

		// ::::::::::::::::::::: IMAGES :::::::::::::::::::::

		public string GetImageSource(FileMetaData md)
		{
			Print ("GetImageSource...");
			var tcs = new TaskCompletionSource<string>();
			App.client.File ().downloadMetadata (md.id, new KinveyDelegate<FileMetaData>{
				onSuccess = (meta) => {
					tcs.SetResult(meta.downloadURL);
				},
				onError = (obj) => {
					tcs.SetResult("");
				}
			});
			return tcs.Task.Result;
		}


		// ::::::::::::::::::::: OTHERS :::::::::::::::::::::

		public void Print(string msg)
		{
			System.Diagnostics.Debug.WriteLine (msg);
		}
	}
	*/
}

