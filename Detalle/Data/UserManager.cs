﻿using System;
using System.Threading.Tasks;
using KinveyXamarin;

namespace Detalle
{
	public class UserManager
	{
		public UserManager ()
		{
		}

		static public User GetCurrentUser()
		{
			TaskCompletionSource<User> tcs = new TaskCompletionSource<User>();
			new Task (async () => {		
				try{	
					var user = await App.client.User().RetrieveAsync();	
					System.Diagnostics.Debug.WriteLine ("Current user! \nUsername: " + user.UserName + "\nAuthToken: " + user.AuthToken);
					tcs.SetResult(user);
				} catch(Exception e) {
					System.Diagnostics.Debug.WriteLine (e.Message);
					tcs.SetResult(null);
				}
			}).Start ();
			return tcs.Task.Result;
		}

		// No real user
		static public User GetImplicitUser()
		{
			TaskCompletionSource<User> tcs = new TaskCompletionSource<User>();
			new Task (async () => {					
				var user = await App.client.User().LoginAsync();
				System.Diagnostics.Debug.WriteLine ("New implicit user! \nUsername: " + user.UserName + "\nAuthToken: " + user.AuthToken);
				tcs.SetResult(user);
			}).Start ();
			return tcs.Task.Result;
		}

		static public User SignUp(string username, string password){
			TaskCompletionSource<User> tcs = new TaskCompletionSource<User>();
			new Task (async () => {							
				var some = await App.client.User().DeleteAsync(App.ActiveUser.Id, true);
				var user = await App.client.User ().CreateAsync (username, password);
				//user.Add("real", "true");
				//var userUpdated = await App.client.User().UpdateAsync(user);
				System.Diagnostics.Debug.WriteLine ("User created! \nUsername: " + user.UserName + "\nAuthToken: " + user.AuthToken);
				tcs.SetResult(user);				
			}).Start ();
			return tcs.Task.Result;

		}

		static public void LoginWithFacebook(){
			new Task (async () => {
				User user = await App.client.User().LoginFacebookAsync ("408832435984255");
			}).Start();
		}
	}
}

