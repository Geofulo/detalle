﻿using System;

using Xamarin.Forms;
using KinveyXamarin;

namespace Detalle
{
	public class App : Application
	{
		static public int ScreenWidth;
		//static public UserModel CurrentUser; 
		static public Client client;
		static public User ActiveUser;
		static public DataManager DataMan;
		static public LocalDataManager LocalDataMan;
		static public bool IsRealUser = false;

		public App ()
		{
			if (DependencyService.Get<INetworkConnection> ().IsConnected) {
				System.Diagnostics.Debug.WriteLine ("Internet available!");
				//client = DependencyService.Get<IOfflineData> ().GetUser ();
				client = new Client.Builder ("kid_-1VWKZwPVg", "51f21aab312d456d9ed9724068d628df").build ();

				ActiveUser = UserManager.GetCurrentUser ();	
				if (ActiveUser == null) {
					ActiveUser = UserManager.GetImplicitUser ();
					ActiveUser = UserManager.GetCurrentUser ();	
				}			

				DataMan = new DataManager ();
				LocalDataMan = new LocalDataManager ();
				LocalDataMan.GetDataFromKinvey ();

				//CurrentUser = DependencyService.Get<ILogin> ().GetCurrentUser ();
				MainPage = new MainMasterDetailPage();	
			} else {
				System.Diagnostics.Debug.WriteLine ("No Internet!");
				MainPage = new ContentPage {
					Content = new Label {
						Text = "No tiene conexión a internet",
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
				};
			}
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

