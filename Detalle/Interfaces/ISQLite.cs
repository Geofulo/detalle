﻿using System;

namespace Detalle
{
	public interface ISQLite
	{
		SQLite.SQLiteConnection GetConnection();
	}
}

