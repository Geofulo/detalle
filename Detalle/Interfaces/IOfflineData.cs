﻿using System;
using KinveyXamarin;

namespace Detalle
{
	public interface IOfflineData
	{
		Client GetUser();
	}
}

