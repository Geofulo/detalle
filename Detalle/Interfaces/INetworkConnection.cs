﻿using System;

namespace Detalle
{
	public interface INetworkConnection
	{
		bool IsConnected { get; }
	}
}

