﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace Detalle
{
	public interface ILocation
	{
		void ManageLocation (Map map); 
	}
}

