﻿using System;

namespace Detalle
{
	public interface INotifications
	{
		void ShowMessageRouteInit ();
		void ShowMessageRouteFinished ();
	}
}

