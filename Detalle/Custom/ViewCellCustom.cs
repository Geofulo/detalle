﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ViewCellCustom : ViewCell
	{
		public static readonly BindableProperty IsCheckmarkAccessoryProperty = 
			BindableProperty.Create<ViewCellCustom,bool>(
				p=>p.IsCheckmarkAccessory, false);

		public bool IsCheckmarkAccessory {
			get { return (bool)GetValue (IsCheckmarkAccessoryProperty); }
			set { SetValue (IsCheckmarkAccessoryProperty, value); }
		}

		public static readonly BindableProperty IsDetailAccessoryProperty = 
			BindableProperty.Create<ViewCellCustom,bool>(
				p=>p.IsDetailAccessory, false);

		public bool IsDetailAccessory {
			get { return (bool)GetValue (IsDetailAccessoryProperty); }
			set { SetValue (IsDetailAccessoryProperty, value); }
		}

		public static readonly BindableProperty IsDisclosureAccessoryProperty = 
			BindableProperty.Create<ViewCellCustom,bool>(
				p=>p.IsDisclosureAccessory, false);

		public bool IsDisclosureAccessory {
			get { return (bool)GetValue (IsDisclosureAccessoryProperty); }
			set { SetValue (IsDisclosureAccessoryProperty, value); }
		}
	}
}

