﻿using System;
using Xamarin.Forms;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

namespace Detalle
{
	public class CarouselViewCustom : ScrollView
	{
		readonly StackLayout stack;
		int selectedIndex;
		private bool layingOutChildren;
		public DataTemplate ItemTemplate { get; set; }
		public IList ItemsSource {
			get { Debug.WriteLine("get ItemsSource"); return (IList) GetValue (ItemsSourceProperty); }
			set { Debug.WriteLine("set ItemsSource"); SetValue (ItemsSourceProperty, value); }
		}

		public CarouselViewCustom ()
		{
			Orientation = ScrollOrientation.Horizontal;
			stack = new StackLayout () {
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
			};
			Content = stack;


			Debug.WriteLine("CarouselView");
		}

		public IList<View> Children {
			get {
				Debug.WriteLine("get Children");
				return stack.Children;
			}
		}

		protected override void LayoutChildren (double x, double y, double width, double height) {
			base.LayoutChildren (x, y, width, height);
			if (layingOutChildren) return;

			layingOutChildren = true;
			foreach (var child in Children) child.WidthRequest = width;
			layingOutChildren = false;
			Debug.WriteLine("LayoutChildren");
		}

		public int SelectedIndex {
			get { Debug.WriteLine("get SelectedIndex"); return (int)GetValue (SelectedIndexProperty); }
			set { Debug.WriteLine("set SelectedIndex"); SetValue (SelectedIndexProperty, value); }
		}
		/*
		void UpdateSelectedItem () {
			selectedItemTimer.Stop ();
			selectedItemTimer.Start ();
		}*/

		public static readonly BindableProperty SelectedIndexProperty =
			BindableProperty.Create<CarouselViewCustom, int> (
				carousel => carousel.SelectedIndex, 0,
				BindingMode.TwoWay,
				propertyChanged: (bindable, oldValue, newValue) => {
					Debug.WriteLine("SelectedIndexProperty");
					//((CarouselView)bindable).UpdateSelectedItem ();
				}
			);

		public static readonly BindableProperty ItemsSourceProperty =
			BindableProperty.Create<CarouselViewCustom, IList> (
				view => view.ItemsSource, null,
				propertyChanging: (bindableObject, oldValue, newValue) => {
					((CarouselViewCustom)bindableObject).ItemsSourceChanging ();
					Debug.WriteLine("ItemsSourceProperty Changing");
				},
				propertyChanged: (bindableObject, oldValue, newValue) => {
					((CarouselViewCustom)bindableObject).ItemsSourceChanged ();
					Debug.WriteLine("ItemsSourceProperty Changed");
				}
			);				

		void ItemsSourceChanging () {
			if (ItemsSource == null) Debug.WriteLine("ItemsSource NULL"); return;
			selectedIndex = ItemsSource.IndexOf (SelectedItem);
			Debug.WriteLine("ItemsSourceChanging");
		}

		void ItemsSourceChanged ()
		{
			stack.Children.Clear ();
			foreach (var item in ItemsSource) {
				var view = (View)ItemTemplate.CreateContent ();
				var bindableObject = view as BindableObject;
				if (bindableObject != null)
					bindableObject.BindingContext = item;
				stack.Children.Add (view);
			}

			if (selectedIndex >= 0) SelectedIndex = selectedIndex;
			Debug.WriteLine("ItemsSourceChanged");
		}

		public static readonly BindableProperty SelectedItemProperty = 
			BindableProperty.Create<CarouselViewCustom, object> (
				view => view.SelectedItem, null,
				BindingMode.TwoWay,
				propertyChanged: (bindable, oldValue, newValue) => {
					//((CarouselViewCustom)bindable).UpdateSelectedIndex ();
					Debug.WriteLine("SelectedItemProperty");
				}
			);

		public Image SelectedItem {
			get { Debug.WriteLine("get SelectedItem"); return (Image) GetValue (SelectedItemProperty); }
			set { Debug.WriteLine("set SelectedItem"); SetValue (SelectedItemProperty, value); }
		}
		/*
		void UpdateSelectedIndex () {
			if (SelectedItem == BindingContext) return;
			SelectedIndex = Children
				.Select (c => c.BindingContext)
				.ToList ()
				.IndexOf (SelectedItem);
			Debug.WriteLine("UpdateSelectedIndex");
		}*/
	}
}

