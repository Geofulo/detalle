﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class EditorCustom : Editor
	{
		public static readonly BindableProperty BorderColorProperty = 
			BindableProperty.Create<EditorCustom, Color>(
				p=>p.BorderColor, Color.Black);

		public Color BorderColor {
			get { return (Color)GetValue (BorderColorProperty); }
			set { SetValue (BorderColorProperty, value); }
		}

		public static readonly BindableProperty BorderSizeProperty = 
			BindableProperty.Create<EditorCustom, float>(
				p=>p.BorderSize, 0);

		public float BorderSize {
			get { return (float)GetValue (BorderSizeProperty); }
			set { SetValue (BorderSizeProperty, value); }
		}

		public static readonly BindableProperty BorderRadiusProperty = 
			BindableProperty.Create<EditorCustom, float>(
				p=>p.BorderRadius, 0);

		public float BorderRadius {
			get { return (float)GetValue (BorderRadiusProperty); }
			set { SetValue (BorderRadiusProperty, value); }
		}

		public static readonly BindableProperty TextColorProperty = 
			BindableProperty.Create<EditorCustom, Color>(
				p=>p.TextColor, Color.Black);

		public Color TextColor {
			get { return (Color)GetValue (TextColorProperty); }
			set { SetValue (TextColorProperty, value); }
		}

		public static readonly BindableProperty PlaceholderProperty = 
			BindableProperty.Create<EditorCustom, string>(
				p=>p.Placeholder, "");

		public string Placeholder {
			get { return (string)GetValue (PlaceholderProperty); }
			set { SetValue (PlaceholderProperty, value); }
		}
	}
}

