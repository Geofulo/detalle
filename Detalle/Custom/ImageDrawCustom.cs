﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ImageDrawCustom : Image
	{
		public static readonly BindableProperty CurrentLineColorProperty = 
			BindableProperty.Create ((ImageDrawCustom w) => w.CurrentLineColor, Color.Default);

		public Color CurrentLineColor {
			get {
				return (Color)GetValue (CurrentLineColorProperty);
			}
			set {
				SetValue (CurrentLineColorProperty, value);
			}
		}
	}
}

