﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class StepperCustom : Stepper
	{
		public static readonly BindableProperty BorderColorProperty = 
			BindableProperty.Create<StepperCustom, Color>(
				p=>p.BorderColor, Color.Black);

		public Color BorderColor {
			get { return (Color)GetValue (BorderColorProperty); }
			set { SetValue (BorderColorProperty, value); }
		}
	}
}

