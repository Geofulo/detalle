﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ImageCustom : Image
	{
		public static readonly BindableProperty VectorProperty =
			BindableProperty.Create<ImageCustom, string>(
				p => p.Vector, 
				default(string));

		public static readonly BindableProperty ColourProperty =
			BindableProperty.Create<ImageCustom, Color?>(p => p.Colour, default(Color?));
		
		public string Vector
		{
			get { 
				var value = GetValue(VectorProperty);
				if (value == null) {
					return default(string);
				}
				return (string)value; 
			}
			set { SetValue(VectorProperty, value); }
		}

		public Color? Colour
		{
			get { return (Color?)GetValue(ColourProperty); }
			set { SetValue(ColourProperty, value); }
		}
	}
}

