﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class NavigationPageCustom : NavigationPage
	{
		public NavigationPageCustom(Page page) : base(page) {}

		public static readonly BindableProperty IsTranslucentProperty =
			BindableProperty.Create<NavigationPageCustom, Boolean>(
				p => p.IsTranslucent, false);

		public Boolean IsTranslucent {
			get { return (Boolean)GetValue(IsTranslucentProperty); }
			set { SetValue(IsTranslucentProperty, value); }
		}

		public static readonly BindableProperty IsCombinedProperty =
			BindableProperty.Create<NavigationPageCustom, Boolean>(
				p => p.IsCombined, false);

		public Boolean IsCombined {
			get { return (Boolean)GetValue(IsCombinedProperty); }
			set { SetValue(IsCombinedProperty, value); }
		}

		public static readonly BindableProperty TintColorProperty =
			BindableProperty.Create<NavigationPageCustom, Color>(
				p => p.TintColor, Color.Black);

		public Color TintColor {
			get { return (Color)GetValue(TintColorProperty); }
			set { SetValue(TintColorProperty, value); }
		}

	}
}

