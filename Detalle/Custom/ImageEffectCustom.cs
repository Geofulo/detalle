﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class ImageEffectCustom : Image {

		/*
		 * IS_BLUR_RIGHT	-> input: Boolean, default: false		 
		 * IS_DARK_BLUR		-> input: Boolean, default: false
		*/

		public static readonly BindableProperty BlurLightProperty =
			BindableProperty.Create<ImageEffectCustom, Boolean>(
				p => p.IsBlurLight, false);

		public Boolean IsBlurLight {
			get { return (Boolean)GetValue(BlurLightProperty); }
			set { SetValue(BlurLightProperty, value); }
		}

		public static readonly BindableProperty BlurLightMediumProperty =
			BindableProperty.Create<ImageEffectCustom, Boolean>(
				p => p.IsBlurLightMedium, false);

		public Boolean IsBlurLightMedium {
			get { return (Boolean)GetValue(BlurLightMediumProperty); }
			set { SetValue(BlurLightMediumProperty, value); }
		}

		public static readonly BindableProperty DarkBlurProperty =
			BindableProperty.Create<ImageEffectCustom, Boolean>(
				p => p.IsDarkBlur, false);

		public Boolean IsDarkBlur {
			get { return (Boolean)GetValue(DarkBlurProperty); }
			set { SetValue(DarkBlurProperty, value); }
		}

		public static readonly BindableProperty DarkBlurMediumProperty =
			BindableProperty.Create<ImageEffectCustom, Boolean>(
				p => p.IsDarkBlurMedium, false);

		public Boolean IsDarkBlurMedium {
			get { return (Boolean)GetValue(DarkBlurMediumProperty); }
			set { SetValue(DarkBlurMediumProperty, value); }
		}

		public static readonly BindableProperty IsCircleProperty =
			BindableProperty.Create<ImageEffectCustom, Boolean>(
				p => p.IsCircle, false);

		public Boolean IsCircle {
			get { return (Boolean)GetValue(IsCircleProperty); }
			set { SetValue(IsCircleProperty, value); }
		}

		public static readonly BindableProperty BorderSizeProperty =
			BindableProperty.Create<ImageEffectCustom, int>(
				p => p.BorderSize, 0);

		public int BorderSize {
			get { return (int)GetValue(BorderSizeProperty); }
			set { SetValue(BorderSizeProperty, value); }
		}

		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create<ImageEffectCustom, Color>(
				p => p.BorderColor, Color.White);

		public Color BorderColor {
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}

	}
}

