﻿using System;
using Xamarin.Forms;

namespace Detalle
{
	public class FrameCustom : Frame
	{
		public static readonly BindableProperty ShadowRadiusProperty = 
			BindableProperty.Create<FrameCustom, float>(
				p=>p.ShadowRadius, 0);

		public float ShadowRadius {
			get { return (float)GetValue (ShadowRadiusProperty); }
			set { SetValue (ShadowRadiusProperty, value); }
		}

		public static readonly BindableProperty ShadowOffsetHeightProperty = 
			BindableProperty.Create<FrameCustom, float>(
				p=>p.ShadowOffsetHeight, 0);

		public float ShadowOffsetHeight {
			get { return (float)GetValue (ShadowOffsetHeightProperty); }
			set { SetValue (ShadowOffsetHeightProperty, value); }
		}

		public static readonly BindableProperty ShadowOffsetWidthProperty = 
			BindableProperty.Create<FrameCustom, float>(
				p=>p.ShadowOffsetWidth, 0);

		public float ShadowOffsetWidth {
			get { return (float)GetValue (ShadowOffsetWidthProperty); }
			set { SetValue (ShadowOffsetWidthProperty, value); }
		}

		public static readonly BindableProperty ShadowOpacityProperty = 
			BindableProperty.Create<FrameCustom, float>(
				p=>p.ShadowOpacity, 1);

		public float ShadowOpacity {
			get { return (float)GetValue (ShadowOpacityProperty); }
			set { SetValue (ShadowOpacityProperty, value); }
		}

		public static readonly BindableProperty BorderRadiusProperty = 
			BindableProperty.Create<FrameCustom, float>(
				p=>p.BorderRadius, 1);

		public float BorderRadius {
			get { return (float)GetValue (BorderRadiusProperty); }
			set { SetValue (BorderRadiusProperty, value); }
		}


	}
}

