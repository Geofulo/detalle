﻿using System;
using Detalle;
using Xamarin.Forms.Maps;
using Android.Locations;
using System.Threading;
using Xamarin.Forms;
using Android.Content;

[assembly: Xamarin.Forms.Dependency (typeof (ILocation))]

namespace Detalle.Droid
{
	public class LocationDependency : ILocation
	{
		Map map;
		//LocationManager DealerLM;
		//LocationManager DestinationLM;
		Position DealerPosition = new Position (19.432108, -99.152056);	
		Position DestinationPosition = new Position (19.416311, -99.159900);
		Position AveragePosition;
		//Location DealerLocation; = new Location (19.432108, -99.152056);
		//Location DestinationLocation; = new Location (19.416311, -99.159900);
		Pin DealerPin;
		Pin DestinationPin;
		double AverageDistance;

		public LocationDependency () {}

		public void ManageLocation(Map map){
			this.map = map;
			ManageDestinationLocation ();
			ManageDealerLocation ();

		}

		void ManageDestinationLocation ()
		{			
			//DestinationLM = Forms.Context.ApplicationContext.GetSystemService (Context.LocationService) as LocationManager;
			/*
			DestinationLocation = new Location() {
				Latitude = 19.416311,
				Longitude = -99.159900
			};*/

			DestinationPin = new Pin () {
				Label = "Destino",
				Position = DestinationPosition,
				Type = PinType.Place,
			};
			map.Pins.Add (DestinationPin);

		}

		void ManageDealerLocation ()
		{
			//DealerLM = Forms.Context.ApplicationContext.GetSystemService (Context.LocationService) as LocationManager;
			/*
			DealerLocation = new Location() {
				Latitude = 19.432108,
				Longitude = -99.152056
			};*/

			DealerPin = new Pin(){
				Label = "Repartidor",
				Position = DealerPosition,				
			};
			map.Pins.Add (DealerPin);
			AveragePosition = new Position ((DealerPosition.Latitude + DestinationPosition.Latitude) / 2, 
				(DealerPosition.Longitude + DestinationPosition.Longitude) / 2);	
			//AverageDistance = Math.Abs(DealerPosition.Longitude - DestinationPosition.Longitude / DealerPosition.Latitude - DestinationPosition.Latitude);

			//AverageDistance = DealerLocation.DistanceTo (DestinationLocation);

			AverageDistance = Math.Acos (
				(Math.Sin (DealerPosition.Latitude) * Math.Sin (DestinationPosition.Latitude)) +
				(Math.Cos (DealerPosition.Latitude) * Math.Cos (DestinationPosition.Latitude))
				* Math.Cos (DestinationPosition.Longitude - DealerPosition.Longitude)) * 6378137;
			
			map.MoveToRegion (MapSpan.FromCenterAndRadius(AveragePosition, Distance.FromMeters(AverageDistance / 2)));

			Thread someThread = new Thread (new ThreadStart (delegate {
				SetDealerRoute();
			}));
			someThread.Start ();

		}

		public void SetDealerRoute() {
			DependencyService.Get<INotifications> ().ShowMessageRouteInit ();
			while (AverageDistance != 0) {		
				var newLatitude = 0.0;
				var newLongitude = 0.0;

				if (DestinationPosition.Latitude - DealerPosition.Latitude < 0) {
					newLatitude = DealerPosition.Latitude - 0.0005;
				} else 
					newLatitude = DestinationPosition.Latitude;
				if(DestinationPosition.Longitude - DealerPosition.Longitude < 0){
					newLongitude = DealerPosition.Longitude - 0.0005;
				} else 
					newLongitude = DestinationPosition.Longitude;

				//DealerLocation = new Location (newLatitude, newLongitude);					
				DealerPosition = new Position (newLatitude, newLongitude);

				AveragePosition = new Position ((DealerPosition.Latitude + DestinationPosition.Latitude) / 2, 
					(DealerPosition.Longitude + DestinationPosition.Longitude) / 2);
				AverageDistance = Math.Acos (
					(Math.Sin (DealerPosition.Latitude) * Math.Sin (DestinationPosition.Latitude)) +
					(Math.Cos (DealerPosition.Latitude) * Math.Cos (DestinationPosition.Latitude))
					* Math.Cos (DestinationPosition.Longitude - DealerPosition.Longitude)) * 6378137;
				Console.WriteLine (AverageDistance);

				Device.BeginInvokeOnMainThread (delegate {
					map.Pins.Add (new Pin(){
						Label = AverageDistance.ToString(),
						Position = DealerPosition,
					});					
					//DealerPin.Position = DealerPosition;
					map.MoveToRegion (MapSpan.FromCenterAndRadius(AveragePosition, Distance.FromMeters(AverageDistance)));
				});

				Thread.Sleep (2000);
			}			
			DependencyService.Get<INotifications> ().ShowMessageRouteFinished ();
		}
	}
}

