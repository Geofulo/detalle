﻿using System;
using Detalle;
using Android.Net;
using Android.Content;
using Android.Test.Mock;

[assembly: Xamarin.Forms.Dependency (typeof (INetworkConnection))]

namespace Detalle.Droid
{
	public class NetworkConnectionDependency : INetworkConnection
	{
		#region INetworkConnection implementation
		public bool IsConnected {
			get {
				//throw new NotImplementedException ();
				var connectivityManager = (ConnectivityManager) Android.App.Application.Context.GetSystemService(Context.ConnectivityService);
				var activeConnection = connectivityManager.ActiveNetworkInfo;
				if ((activeConnection != null) && activeConnection.IsConnected) {
					return true;
				} else {
					return false;
				}

				var mobileState = connectivityManager.GetNetworkInfo(ConnectivityType.Mobile).GetState();
				if (mobileState == NetworkInfo.State.Connected)
				{
					// We are connected via WiFi
				}
			}
		}
		#endregion
		
	}
}

