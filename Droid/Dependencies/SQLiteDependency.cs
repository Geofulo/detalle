﻿using System;
using Detalle;
using System.IO;

[assembly: Xamarin.Forms.Dependency (typeof (ISQLite))]

namespace Detalle.Droid
{
	public class SQLiteDependency : ISQLite
	{
		public SQLiteDependency () {}

		public SQLite.SQLiteConnection GetConnection ()
		{
			var sqliteFilename = "Detalles1.db3";
			string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); // Documents folder
			var path = Path.Combine(documentsPath, sqliteFilename);
			// Create the connection
			var conn = new SQLite.SQLiteConnection(path);
			// Return the database connection
			return conn;
		}
	}
}

