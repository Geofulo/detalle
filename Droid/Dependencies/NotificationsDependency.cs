﻿using System;
using Detalle;
using Android.App;
using Xamarin.Forms;
using Android.Content;
using Android.Media;
using Android.Support.V4.App;

[assembly: Xamarin.Forms.Dependency (typeof (INotifications))]

namespace Detalle.Droid
{
	public class NotificationsDependency : INotifications
	{
		public NotificationsDependency () {}

		public void ShowMessageRouteInit (){
			Notification.Builder builder = new Notification.Builder (Forms.Context)
				.SetContentTitle ("Regalo en camino")		
				.SetSmallIcon(Resource.Drawable.ic_media_route_on_mono_dark)
				.SetContentText ("Su regalo ya va en camino, da click aqui para ver la ruta")
				.SetDefaults (NotificationDefaults.Sound);
			builder.SetSound (RingtoneManager.GetDefaultUri (RingtoneType.Ringtone));
			NotificationManager notificationManager = Forms.Context.ApplicationContext.GetSystemService(Context.NotificationService) as NotificationManager;				
			notificationManager.Notify (0, builder.Build());
		}

		public void ShowMessageRouteFinished (){
			Notification.Builder builder = new Notification.Builder (Forms.Context)
				.SetContentTitle ("Regalo entregado")
				.SetSmallIcon(Resource.Drawable.ic_media_route_on_mono_dark)
				.SetContentText ("Su regalo llegó a su destino")
				.SetDefaults (NotificationDefaults.Sound);
			builder.SetSound (RingtoneManager.GetDefaultUri (RingtoneType.Ringtone));
			NotificationManager notificationManager = Forms.Context.ApplicationContext.GetSystemService(Context.NotificationService) as NotificationManager;				
			notificationManager.Notify (1, builder.Build());
		}
	}
}

