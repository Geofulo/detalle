﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;
using Detalle.Droid;

namespace Detalle.Droid
{
	[Activity (Label = "Detalle.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);

			DependencyService.Register<LocationDependency>();
			DependencyService.Register<NotificationsDependency>();
			DependencyService.Register<NetworkConnectionDependency>();
			DependencyService.Register<SQLiteDependency>();

			App.ScreenWidth = (int)Resources.DisplayMetrics.WidthPixels;

			LoadApplication (new App ());
		}
	}
}

